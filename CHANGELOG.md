# Niceerr Changelog

The release versions follow the usual semver MAJOR.MINOR.PATCH convention.

Current major version is 1 and the entire program can be considered stable. It's very unlikely to change unless there's a significant change.

### Today, tomorrow, forever and always

- War, because war never changes

### Dec 12th, 2024

Release 1.66.1

Backend changes:

- Usual dependency bump

Extra/Telegram userbot changes:

- Dependency bump
- Feat: Add support to enqueue server-side downloads
- Fix: Rare bug when internet connection is unstable that caused the script to kill itself



### Dec 5th, 2024

Release 1.66.0

Backend changes: 

- Usual dependency bump
- Feat: **BREAKING CHANGE**: ARL refresh service: remove `token_refresh_interval_sec` and get rid of one worker thread that always ran every once in a while, in favour of a brand new refresh system that only runs on demand 
- Feat: add `arl_refresh_max_age_sec` and `arl_refresher_inactivity_timeout_sec`
- Chore: change default server-side and streaming worker count to 6 and 3, respectively (this appears to better saturate most links)
- Feat: Info endpoint: remove `has_valid_arl` metric
- Feat: Add `prometheus_basic_auth_credentials` for the new prometheus endpoint
- Feat: Metrics endpoint will now live in the same server (instead of being spawned in a separate axum server)
- Feat: add sample prometheus config to extras folder

Frontend changes:

- Feat: info page: remove `has_valid_arl` metric; change `fallbackArlCacheSize` to `hotArlCacheSize`


### Dec 3rd, 2024

Release 1.65.0

Backend changes:

- Usual dependency bump
- Feat: Relay client: use "refresh token" feature as much as possible instead of doing plain old auth calls
- Feat: Expose rustc, LLVM compiler info and database statistics
- Feat: reduce idle db connection timeout
- Chore: make minimum rust version `1.83`

Frontend changes:

- Feat: Expose new stats info

### Nov 29th, 2024

Release 1.64.0

Backend changes:

- Usual dependency bump
- Feat: add prometheus endpoint (off by default)
- **BREAKING CHANGE:** removed `listen_port`, now listen port shall be specified in `listen_address`


### Nov 26th, 2024

Release 1.63.1

Backend changes:

- Usual dependency bump
- Fix: Fallback not working properly
- Fix: relayed downloads failing when network is unstable (don't make those errors "un-retriable")
- Chore: Optimize some relay client code


### Nov 20th, 2024

Release 1.63.0

Backend changes:

- Usual dependency bump
- Fix: Don't allow hidden folders patterns/strings in download paths when creating or updating users
- Fix: Remove redundant and duplicate `can_stream_at_format` checks in the relay endpoint
- Feat: Add relay endpoint only when the feature is enabled to reduce router bloat

Frontend changes:

- Feat: add toggle for full black theme
- Fix: make "positive" notification darker because it was almost unreadable (white text on pale green background)
- Fix: when the server has a valid ARL, don't show an orange warning icon


### Nov 15th, 2024

Release 1.62.0

Backend changes:

- Usual dependency bump
- Fix: Remove extra 'E" in startup ASCII art
- Feat: Always enable `advanced_arl_availability` by default and remove the feature flag to turn it on
- Feat: Remove `user_ids_immutable_passwords`
- Feat: Add `server_enable_relay` to allow a "server" Niceerr instance to relay stream links to another "client" Niceerr instance without an ARL
- Feat: Add `relay_url`: Allow any user, without a valid ARL, to use another "server" Niceerr instance (with a valid ARL) to stream stuff
- Feat: Add `relay_client_refresh_sec`: refresh the relay token every this many seconds
- Feat: Revamp DRM decoding logic to make it slightly more efficient
- Feat: Add new permission to allow per-user relay usage
- Feat: Use exponential backoff in more places to make the whole program more robust
- Feat: Use a dedicated and descriptive error message when no valid ARL exists
- Chore: change default jwt duration from 1h to 12h
- Chore: change pruner internval (`prune_service_interval_sec`) from 10m to 30m

Frontend:

- Feat: add permission changes for the relay stuff
- Feat: add relay-related stuff to the admin console tools
- Feat: Add spinner animation to "Trigger server ARL refresh" notification popup
- Fix: When a local download fails, remove "Cancel" button from notification popup (there's nothing to cancel)

### Nov 9th, 2024

Release 1.61.0

Backend changes: 

- Usual dependency bump
- Feat: On Unix-like systems, don't allow download paths that start with either "." or ".." as these are "hidden" folder names
- Feat: Remove "Create User" permission as it doesn't make any sense to allow regular users to create users with potentially more permissions
- Chore: Use sets instead of lists to store permission data
- Chore: Minor improvements to api call used to fetch all users

Frontend changes:

- Feat: Remove "Create User" permission and require superuser perm to create other users
- Fix: User management: don't allow sorting the table by permissions (backend doesn't support sorting by this column, so it doesn't make sense to allow that)

### Nov 2nd, 2024

Release 1.60.0

Backend changes:

- Usual dependency bump
- Optimize logic in repeated album detector

### Oct 21th, 2024

VERY BIG (but still minor :p) RELEASE!

Release 1.59.0

Backend changes:

- Usual dependency bump
- Feat: Implement fine grained permission-based access control and drop the old "simpler but wide" permission system
- Feat: Implement the permission system for almost every resource there is

Frontend changes:

- Fix: Use computed properties in more places to dynamically update the UI when the user token is updated
- Fix: Ongoing download listener: properly try to re-establish a connection when the server goes down for maintenance
- Feat: Ongoing download listener: remove a lot of redundant code by using js' bind methods
- Feat: Implement support for fine grained permission system
- Feat: All views and relevant parts of Niceerr will be hidden from view when the user doesn't have the permission to do so
- Feat: User management: edit popups will be hidden if the user doesn't have the permission to edit certain attributes

### Oct 17th, 2024

Release 1.58.0

Backend changes:

- Usual dependency bump
- Fix: Stream downloads getting stuck due to strong reference being kept unintentionally
- Chore: Increase robustness by panic'ing in places where errors aren't supposed to happen
- Fix: Add unique_key constraint to ARL token column to prevent duplicate tokens

Frontend changes:

- Feat: Admin console: Add bulk ARL parser

"Extra tools" changes:

- Fix: Remove firehawk ARL scrapper because the site got nuked

### Oct 11th, 2024

Release 1.57.0

Backend changes:

- Usual dependency bump that apparently fixes a weird Tokio memory leak
- Feat: Try to use the new and shiny rust `LazyLock` instead of `OnceLock` in more places
- Feat: Compile all regexes on startup (should insignificantly speed things up)
- Feat: Info endpoint: Add server startup time/uptime
- Feat: Simplify and harden auth token refresh logic

Frontend changes:

- Feat: Show server uptime/start date in Server Stats
- Feat: Simplify auth token refresh logic to match up the backend changes

### Sep 20th, 2024

Release: 1.56.1

Backend changes:

- Usual dependency bump
- Fix: Shutdown handler: forcefully disconnect 'ongoing download' event listeners attached as this literally prevented shutdowns
- Feat: Shutdown handler: Merge SIGTERM and CTRL-C handlers into one (less memory used, reduces code bloat)
- Feat: Stream downloader: Remove redundant code and optimize the functions used to perform live and from-storage streams so that they have less branches and instead have more focused/specific code.
- Feat: Config: premature optimization: if `cors_origins` isn't set, don't waste memory in a unused full blown Vec; waste only 1 'pointer' worth of memory only
- Feat: Dzlib: Remove redundant code and make it slightly more efficient


### Sep 14th, 2024

Release: 1.56.0

Backend changes:

- Fix: Duplicate album detector: no longer detect albums as duplicates if they are from different artists
- Fix: Duplicate song name logic: completely remove it because it actually messed up tags; replace it with far more robust logic
- Fix: Stream downloads: no longer cancel the download when it finishes successfully (prevents marking the download session as 'Stopped' even when it actually 
finished)
- Fix: properly replace name schema templates (e.g. $VERSION) when there's missing data, otherwise we'd end up with weird song names such as '1. SomeSong $VERSION.flac         '
- Feat: Artist/Album/Song name schemas: minor optimizations
- Feat: Get rid of dynamic dispatch and use static dispatch for the song streamer (should make things insignificantly faster) 
- Feat: Add `$VERSION` to available 'song name schema' placeholders, and enable it by default

### Sep 12th, 2024

Release: 1.55.0

Backend changes: 

- Usual dependency bump
- Feat: `user_ids_immutable_passwords`: prevent specific user IDs from changing their password
- Fix: Config: increase streaming timeout to account for potentially slow downloaders/network issues


### Sep 9th, 2024

Release: 1.54.1

Backend changes: 

- Remove `stale_semaphore_pruner_interval_sec` completely and use a more efficient alternative
- Remove tokio `sync` dependency
- SchedulerTracker: Remove semaphore pruner task as it no longer runs periodically

Frontend changes:

- Remove semaphore pruner from task list


### Sep 6th, 2024

This is quite a big release!

Release: 1.54.0

Backend changes:

- housekeeping: Usual dependency bump
- fix: Streamer: revert back to `DuplexStream` due to tokio bug not closing connections properly when the downstream client disconnects
- fix: Streamer: Now the server can cut off super slow consumers with extremely crappy connections. This prevents resource starvation.
- feat: Add a whole lot of endpoints to manage the server: Console
- feat: Add scheduler tracker to keep track of scheduled tasks
- feat: ARL CRUD: Return 423: Locked when someone tries to edit/update/delete ARLs and the server is performing a potentially long ARL operation
- feat: Console: Add feature to reorder ARL priorities
- feat: Console: Add feature to manually execute scheduled tasks
- feat: Console: Add feature to show scheduler stats

Frontend:

- feat/fix: login page: revamp error message and add some logging when things go wrong (requested by repowoman)
- housekeeping: Usual dependency bump (fixes Quasar memory leaks)
- feat: add a whole lot of components to access the Console feature
- fix: update dev dependencies vulnerabilities 
- feat: apply translucent blur to notifications
- fix: replace old $q.notification with niceerr's notification wrapper

### Sep 3rd, 2024

Release: 1.53.1

Backend changes:

- Bump dependencies
- Bugfix: Properly extract lyrics from API
- Bugfix: Burn `UNSYNCEDLYRICS` 


### Aug 30th, 2024

Release: 1.53.0

Backend changes:

- Bump dependencies
- Optimize streamed downloads by using uni-directional instead of bi-directional pipes 
- Add new knob: `http_accept_language` to set the language used to make requests
- ID3 tags: genres now should follow the 'accept_language'

### Aug 19th, 2024

Release: 1.52.0

Backend changes:

- Bump dependencies
- Big optimization: Improve asynchronous album fetching
- Add `album_fetcher_queue_size`: Now we'll keep a queue with ready-to-use albums
- Dzlib: Album covers are now fetched at their true maximum quality 
- Dzlib: Fix some ID3 tags not being properly saved

Release: 1.51.2

Backend changes:

- Bump dependencies (reqwest)
- Minor optimization: instead of downloading the album cover for each song, download it for the entire album instead
- Minor optimization: when checking if an album is a compilation, use numeric artist IDs for comparison instead of strings

Release: 1.51.1

Backend changes:

- Bump dependencies
- Fix an issue with albums being wrongfully tagged as "Various Artists" due to Deezer API returning wrong info


### Aug 14th, 2024

Release: 1.51.0

Frontend changes:

- Add a toggle for translucent blur
- Settings page: make everything simpler and more organized


### Aug 13th, 2024

Release: 1.50.1

Backend changes:

- Bump dependencies
- Fix: When downloading compilations, burn "Various Artists" into `albumartist` tag

### Aug 10th, 2024

Release: 1.50.0

Backend changes:

- Bump dependencies

Frontend changes:

- User settings: add a new toggle to toggle desktop/mobile behavior for some components
- User management: When updating users, properly validate any data before even sending it to the server
- UX: Add new component to properly show info tooltips, even on mobile screens
- Modularize more components


### Aug 2nd, 2024

Release: 1.49.0

Backend changes:

- Further optimize the code that delivers live updates for ongoing downloads: the server no longer wastes resources if the caller doesn't have any ongoing downloads for them
- Document new options added in version `1.48`
- Info endpoint: Add a count for SSE stream readers
- Deal with dz library log spam when an ARL is invalid

Frontend changes:

- (stab): Try to fix frozen ongoing downloads view when a live streaming download is going on by changing the request priority 
- Ongoing downloads: Fix a bug where an SSE connection wouldn't be terminated properly, causing client-side resource leaks
- Implement blur in (hopefully) all dialogs and tooltips
- Info page: Add detailed descriptions to all items
- Info page: Expose new info (SSE stream reader count)

### Aug 1st, 2024

Release: 1.48.0

Backend changes:

- Minor dependency bump
- Ongoing downloads: Add a new endpoint that makes use of Server Side Events: this should eliminate the need for continuous polling on both the client and the server
- Ongoing downloads: New downloads will show up immediately in the client thanks to the use of a dedicated waker

Frontend changes:

- Crank up the blur in even more places
- Remove old polling logic and use dedicated server-side events instead


### July 31st, 2024

Release: 1.47.1

Frontend changes:

- Crank up the blur


### July 28th, 2024

Release: 1.47.0

Backend changes:

- Minor dependency bump
- Remove old logic used to detect invalid strings and replace it with a more performant one
- Reduce memory usage even further by removing redundant structures
- Return proper JSON errors when we fail to deserialize json due to incorrect data (should result in better error messages)
- Expose download session for individual downloads. This allows us to present a little bit more data in the frontend.
- Fix bug where potentially invalid/empty downlodad paths might be saved

Frontend changes:

- UI/UX: Add blur in "main menu" and download dialogs
- Ongoing downloads: show more data sent by the backend (now you can see download type, ID, etc).
- Ongoing Downloads & Download History: Revamp user column


### July 25th, 2024

Release: 1.46.0

Backend changes:

- Remove zip files LZMA dependency (it wasn't being used)

Frontend changes:

- Fix typo: remove redundant "s" in minutesS
- Add unsupported platform warning when running on windows
- Add option to hide unsupported platform warnings

### July 23rd, 2024

Release: 1.45.0

Backend changes:

- Minor dependency bump
- Add runtime information to info endpoint

Frontend changes:

- Fix an issue where relative dates would show as "1 hour 1 seconds" instead of "1 hour 1 minute"
- Fix 2-country code to string not recognizing Montenegro
- Hide delete and download buttons for ongoing downloads
- Expose runtime info in about page

### July 19th, 2024

Release: 1.44.2

Backend changes:

- Minor dependency bump
- Fix an issue when downloading playlists containing unpublished/deleted songs taking too long to download due to backoff kicking in

Frontend changes:

- About page: add a warning when the backend and frontend versions don't match up

### July 17th, 2024

Release: 1.44.1

Backend changes:

- Reduce memory usage a bit by shrinking stack memory usage in spawned tasks
- Add backend version to info endpoint

Frontend: 

- Remove some unused, legacy code that used inexistent properties
- Downloads history page: add direct upstream link  to "DownloadKind/UpstreamID" column
- Expose backend version in about page

### July 16th, 2024

Release: 1.44.0

Backend changes:

- Bump dependencies
- Rename config parameter `max_download_workers` to `server_side_download_workers`
- Reduce memory usage by spawning download workers only when needed for both stream and server-side downloads
- Fix (streaming downloads): no longer spawn separate workers for each session (potentially reduces memory usage)
- Add new config option: `streaming_remote_timeout_sec` to prevent slow downloaders from taking up system resources
- Add active worker count to info endpoint

Frontend changes:

- Expose active worker count in about page      

### July 11th, 2024

Release: 1.43.2

Backend changes:

- Fix HTTP Client: use correct read_timeout in builder
- Webhook service: use system-wide HTTP client builder

Release: 1.43.1

Backend changes:

- tmp folder cleaner: use BFS instead of recursion to speed up the process and avoid possible stack overflows

### July 10th, 2024

Release: 1.43.0

Backend changes:

- Fix potential race condition when the tmp folder cleaner runs with active/ongoing downloads
- Apply some clippy pedantic lints
- Add tmp folder size to info endpoint
- Disallow '0' values for some config parameters (this simplifies niceerr a little bit)

Frontend changes:

- Expose tmp folder size in about page


### July 6th, 2024

Release: 1.42.0

Backend changes:

- Add "fallback ARL mechanism" to download songs when they're censored in a country
- Add fallback cache size to info endpoint
- Add toggle for fallback logic

Frontend changes:

- Expose fallback cache size in about page

### July 5th, 2024

Release: 1.41.0

Backend changes:

- Use the simplified, more robust backoff logic in more places. This should result in simpler and more robust behavior when there are network failures.
- Performance: remove   useless cloning in some places
- Remove code that can potentially panic in backoff logic
- Dependency updates

### July 3rd, 2024

Release: 1.40.1

Backend changes:

- Clean up the code a little bit
- Performance: use primitive integers instead of atomics for each song's byte counter

### July 2nd, 2024

Release: 1.40.0

Backend changes:

- Pull full song information to prevent issues with missing licensing data
- Handle extreme corner cases when the song API doesn't return licensing data even when the song can be streamed

### June 27th, 2024

Release: 1.39.0

Backend changes:

- Refactor sorting and filtering logic to make it more efficient
- Dependency updates
- When listing users, include total downloaded bytes per user
- Enforce max download path length to 256 chars when creating or updating users

Frontend changes:
- Check max download path length when creating users
- Add a toggle to hide user IDs in Previous Downloads
- Shorten potentially super-long strings in relevant places