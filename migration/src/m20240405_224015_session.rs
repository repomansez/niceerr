use niceerr_entity::{download_session, user};
use sea_orm_migration::prelude::*;
#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(DownloadSession::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(DownloadSession::Id)
                            .uuid()
                            .not_null()
                            .primary_key(),
                    )
                    .col(
                        ColumnDef::new(DownloadSession::DownloadId)
                            .string()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(DownloadSession::DownloadQuality)
                            .string()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(DownloadSession::DownloadKind)
                            .string()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(DownloadSession::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null()
                            .default(Expr::current_timestamp()),
                    )
                    .col(
                        ColumnDef::new(DownloadSession::DownloadedBy)
                            .uuid()
                            .not_null(),
                    )
                    .col(ColumnDef::new(DownloadSession::Outcome).string().not_null())
                    .foreign_key(
                        ForeignKey::create()
                            // create foreign key from this record...
                            .from(
                                download_session::Entity,
                                download_session::Column::DownloadedBy,
                            )
                            // ..and point it to upload_session
                            .to(user::Entity, user::Column::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .col(
                        ColumnDef::new(DownloadSession::IsPruned)
                            .boolean()
                            .default(false)
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(DownloadSession::IncludeCredited)
                            .boolean()
                            .default(false)
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(DownloadSession::AllowFallback)
                            .boolean()
                            .default(false)
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(DownloadSession::SkipLive)
                            .boolean()
                            .default(false)
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(DownloadSession::IsStream)
                            .boolean()
                            .default(false)
                            .not_null(),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(
                Table::drop()
                    .if_exists()
                    .table(DownloadSession::Table)
                    .to_owned(),
            )
            .await
    }
}

#[derive(DeriveIden)]
enum DownloadSession {
    Table,
    Id,
    DownloadId,
    DownloadQuality,
    DownloadKind,
    DownloadedBy,
    Outcome,
    CreatedAt,
    IsPruned,
    IsStream,
    IncludeCredited,
    AllowFallback,
    SkipLive,
}
