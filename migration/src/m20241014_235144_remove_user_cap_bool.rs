use sea_orm_migration::prelude::*;

use crate::m20220101_000001_create_user::User;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Replace the sample below with your own migration scripts

        manager
            .alter_table(
                Table::alter()
                    .table(User::Table)
                    .drop_column(User::ServerSideDownloadsEnabled)
                    .drop_column(User::StreamingDownloadsEnabled)
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        println!("WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING");
        println!(
            r#"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
The migration to upgrade the db schema used for user's download capabilities has been reverted.

If you did this, it means something went wrong with the database and you decided to rollback to
an earlier schema version. Columns have been restored but download permissions for users have been lost and
reset out of an abundance of caution.

You will need to grant permissions manually again for each user and roll back to an older Niceerr version because
the latest one requires the upgraded schema, which you just rolled back.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"#
        );
        manager
            .alter_table(
                Table::alter()
                    .table(User::Table)
                    .add_column_if_not_exists(
                        ColumnDef::new(User::ServerSideDownloadsEnabled)
                            .boolean()
                            .not_null()
                            .default(false),
                    )
                    .add_column_if_not_exists(
                        ColumnDef::new(User::StreamingDownloadsEnabled)
                            .boolean()
                            .not_null()
                            .default(false),
                    )
                    .to_owned(),
            )
            .await
    }
}
