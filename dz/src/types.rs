use crate::{
    playlist::IncompletePlaylist,
    util::{de_u64, de_usize},
};
use lofty::config::ParsingMode;
use serde::{Deserialize, Serialize};

pub(crate) static IV: &[u8; 8] = b"\x00\x01\x02\x03\x04\x05\x06\x07";
pub(crate) static KEY: &[u8; 16] = b"g4el58wc0zvf9na1";

pub(crate) type CbcBlowfish = cbc::Decryptor<blowfish::Blowfish>;

/// Artist biography
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq, Default)]
pub struct ArtistBiography {
    #[serde(rename(deserialize = "BIO"))]
    pub biography: String,
    #[serde(rename(deserialize = "RESUME"))]
    pub html_resume: String,
    #[serde(rename(deserialize = "SOURCE"))]
    pub source: String,
}

/// A lightweight piece of artist. This struct is only used when you query
/// an artist's information page.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq, Default)]
pub struct RelatedArtist {
    /// Artist's ID. You can use the client to get their info, their picture
    /// their albums, etc.
    #[serde(deserialize_with = "de_u64", rename(deserialize = "ART_ID"))]
    pub artist_id: u64,
    #[serde(rename(deserialize = "ART_NAME"))]
    pub name: String,
}

/// Artist's information page. This struct can't be used to pull
/// the artist's albums: it merely contains some useful information
/// such as their biography, fan count, picture, etc.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq, Default)]
pub struct ArtistInfo {
    #[serde(deserialize_with = "de_u64", rename(deserialize = "ART_ID"))]
    pub artist_id: u64,
    #[serde(rename(deserialize = "ART_NAME"))]
    pub artist_name: String,
    #[serde(rename(deserialize = "ART_PICTURE"))]
    pub(crate) artist_picture: String,
    #[serde(rename(deserialize = "NB_FAN"))]
    pub fan_count: usize,
    /// This artist's highlighted songs.
    #[serde(default)]
    pub highlights: Vec<IncompleteSong>,
    /// This artist's top songs.
    #[serde(default)]
    pub top_songs: Vec<IncompleteSong>,
    /// Related artists. This is a normie attribute and most underground
    /// bands have this set to None.
    #[serde(default)]
    pub related_artists: Vec<RelatedArtist>,
    /// Related playlists.
    #[serde(default)]
    pub related_playlists: Vec<IncompletePlaylist>,
    /// Artis't biography. Usually only available for normie artists:
    /// the real good underground stuff usually doesn't have this
    /// attribute.
    #[serde(default)]
    pub biography: Option<ArtistBiography>,
}

/// An artist's discography.
///
/// This struct is very lightweight: it only contains an array of Album IDs
/// and a boolean attribute, but that's it. To get the actual albums, you'll
/// have to call either `stream_albums()` (to get a stream of albums) or call this
/// struct's `ids()` method and then call the client's `get_album()` method
/// for each album ID.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq, Default)]
pub struct DiscographyIds {
    pub(crate) album_ids: Vec<u64>,
    pub(crate) include_credited: bool,
}

#[derive(Deserialize, Debug)]
pub(crate) struct AlbumId {
    #[serde(rename = "ALB_ID")]
    pub(crate) id: String,
}

/// Deeznuts quality choice.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq, Copy, Default)]
pub enum StreamFormat {
    /// Highest available quality
    #[default]
    Flac,
    /// 320kbps mp3
    Mp3_320,
    /// 128kbps mp3
    Mp3_128,
}

/// Additional attributes in this album.
///
/// Deezer API: `SUBTYPE`
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct AlbumAttributes {
    #[serde(rename(deserialize = "isCompilation"))]
    pub(crate) is_compilation: bool,
    #[serde(rename(deserialize = "isLive"))]
    pub(crate) is_live: bool,
}

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct Album {
    #[serde(deserialize_with = "de_u64", rename(deserialize = "ALB_ID"))]
    pub id: u64,
    #[serde(rename(deserialize = "ALB_TITLE"))]
    pub name: String,
    #[serde(deserialize_with = "de_u64", rename(deserialize = "ART_ID"))]
    pub artist_id: u64,
    #[serde(rename(deserialize = "ART_NAME"))]
    pub artist_name: String,
    #[serde(rename(deserialize = "ALB_PICTURE"))]
    pub album_picture: String,
    #[serde(rename(deserialize = "LABEL_NAME"))]
    pub label: Option<String>,
    #[serde(rename(deserialize = "DIGITAL_RELEASE_DATE"))]
    pub digital_release_date: Option<String>,
    #[serde(rename(deserialize = "ORIGINAL_RELEASE_DATE"))]
    pub original_release_date: Option<String>,
    #[serde(rename(deserialize = "PHYSICAL_RELEASE_DATE"))]
    pub physical_release_date: Option<String>,
    #[serde(rename(deserialize = "SUBTYPES"))]
    pub attributes: AlbumAttributes,
    #[serde(default)]
    pub songs: Vec<IncompleteSong>,
    #[serde(default)]
    pub genres: Vec<String>,
}

/// Convenience struct that contains an album and its cover art.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct AlbumWCover {
    pub album: Album,
    pub cover: Vec<u8>,
}

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct SongRights {
    /// Whether the caller can stream this song or not.
    #[serde(rename = "STREAM_SUB_AVAILABLE")]
    pub stream_sub_available: Option<bool>,
}

/// A piece of time-synchronized lyrics.
///
/// Note that, in certain cases, the following attributes will be set
/// to the default serde value:
///
/// - `line` (default: "")
/// - `duration_milliseconds` (default: 0)
/// - `timestamp`: (default: "")
/// - `milliseconds`: (default: 0)
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct SyncedLyricPiece {
    /// The duration of this lyrics piece, in milliseconds.
    #[serde(
        rename(deserialize = "milliseconds"),
        deserialize_with = "de_u64",
        default
    )]
    pub duration_milliseconds: u64,
    /// A line of the lyrics.
    pub line: String,
    /// String timestamp at which this lyrics piece should appear.
    /// The format is as follows: '[MINUTE:SECOND.MILLISECOND]'
    #[serde(default, rename(deserialize = "lrc_timestamp"))]
    pub timestamp: String,
    // #[serde(default, deserialize_with = "de_u64")]
    // Timestamp (in milliseconds) at which this lyrics piece
    // should appear.
    // pub milliseconds: u64,
}

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
/// This song's lyrics.
pub struct SongLyrics {
    #[serde(rename(deserialize = "LYRICS_COPYRIGHTS"))]
    /// This lyric's copyrihghts owner.
    pub copyright: String,
    #[serde(rename(deserialize = "LYRICS_ID"))]
    /// ID of this lyrics object.
    pub id: String,
    #[serde(rename(deserialize = "LYRICS_SYNC_JSON"))]
    /// An array of lyric pieces. Most normie songs contain
    /// time-synced lyrics: the real good underground stuff
    /// usually doesn't have this attribute set.
    pub synced_lyrics: Option<Vec<SyncedLyricPiece>>,

    /// The lyrics, nicely formatted for use in documents
    /// or UI interfaces.
    #[serde(rename(deserialize = "LYRICS_TEXT"))]
    pub text: String,
    /// Lyric writers.
    #[serde(rename(deserialize = "LYRICS_WRITERS"))]
    pub writers: String,
}

/// List of country codes where this song is available.
///
/// When checking for availability in a specific country, the
/// caller must make sure to check both lists. Some artists
/// don't allow their songs to be streamed with ads.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct SongAvailableCountries {
    #[serde(rename(deserialize = "STREAM_ADS"))]
    pub stream_ads: Vec<String>,
    #[serde(rename(deserialize = "STREAM_SUB_ONLY"))]
    pub stream_sub_only: Vec<String>,
}

/// An incomplete song with some missing stuff, such as lyrics,
/// streaming rights and so on.
///
/// Most methods in this library return incomplete data because it
/// doesn't make any sense to include too many details when querying
/// artist/album/playlist data. This is the way the deezer API works.
///
/// To get a full-blown, streamable song you must use the client's
/// song method.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct IncompleteSong {
    #[serde(rename(deserialize = "SNG_ID"), deserialize_with = "de_u64")]
    pub id: u64,
    #[serde(rename(deserialize = "PRODUCT_TRACK_ID"))]
    pub product_track_id: String,
    #[serde(rename(deserialize = "SNG_TITLE"))]
    pub title: String,
    #[serde(rename(deserialize = "VERSION"))]
    pub version: Option<String>,
    #[serde(rename(deserialize = "ART_ID"), deserialize_with = "de_u64")]
    pub artist_id: u64,
    #[serde(rename(deserialize = "ALB_ID"), deserialize_with = "de_u64")]
    pub album_id: u64,
    #[serde(rename(deserialize = "ART_NAME"))]
    pub artist_name: String,
    #[serde(rename(deserialize = "ALB_PICTURE"))]
    pub album_picture: String,
    #[serde(rename(deserialize = "ALB_TITLE"))]
    pub album_name: String,
    #[serde(
        rename(deserialize = "FILESIZE_MP3_320"),
        deserialize_with = "de_usize"
    )]
    pub filesize_mp3_320: usize,
    #[serde(
        rename(deserialize = "FILESIZE_MP3_128"),
        deserialize_with = "de_usize"
    )]
    pub filesize_mp3_128: usize,
    #[serde(rename(deserialize = "FILESIZE_FLAC"), deserialize_with = "de_usize")]
    pub filesize_flac: usize,
    #[serde(rename(deserialize = "DISK_NUMBER"), deserialize_with = "de_u64")]
    pub disk_number: u64,
    #[serde(rename(deserialize = "TRACK_NUMBER"), deserialize_with = "de_u64")]
    pub track_number: u64,
    #[serde(rename(deserialize = "TRACK_TOKEN"))]
    pub track_token: String,
    /// This field (md5) might not be present when using a free account
    #[serde(rename(deserialize = "MD5_ORIGIN"))]
    pub md5: Option<String>,
    #[serde(rename(deserialize = "ISRC"))]
    pub isrc: String,
    #[serde(rename(deserialize = "GAIN"))]
    pub gain: Option<String>,
    #[serde(rename(deserialize = "LYRICS_ID"), deserialize_with = "de_u64")]
    /// This song's lyrics ID. A lyrics_id of 0 means no lyrics.
    pub lyrics_id: u64,
}

/// A complete song, with all possible information. This struct is pretty much
/// the same as [`IncompleteSong`] but it has a few extra fields and
/// some extra methods to stream it.
///
/// This struct implements the Deref trait for [`IncompleteSong`].
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct Song {
    #[serde(flatten)]
    pub song: IncompleteSong,

    /// This song's lyrics.
    #[serde(rename(deserialize = "LYRICS"), default)]
    pub lyrics: Option<SongLyrics>,
    // Rights the caller has access to
    #[serde(rename(deserialize = "RIGHTS"))]
    pub rights: SongRights,

    #[serde(rename(deserialize = "AVAILABLE_COUNTRIES"))]
    pub available_countries: SongAvailableCountries,
}

/// Optional tagging options. This struct can be used to optionally
/// add more information to a song when using the tagging features.
#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct ExtraTagInfo {
    pub is_compilation: bool,
    pub parsing_mode: Option<ParsingMode>,
}
