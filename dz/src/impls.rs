use std::{
    collections::{HashMap, HashSet},
    fmt::Display,
    fs::File,
    io::{Cursor, Seek, Write},
    ops::{Deref, DerefMut},
};

use crate::{
    error::FallibleAPIResponse,
    stream::SongStream,
    types::{
        Album, AlbumWCover, ArtistInfo, DiscographyIds, ExtraTagInfo, IncompleteSong, Song,
        SongLyrics, StreamFormat, KEY,
    },
};
use bytes::Bytes;
use futures_util::{stream::StreamExt, Stream};
use lofty::{
    config::{ParseOptions, WriteOptions},
    file::{BoundTaggedFile, TaggedFileExt},
    picture::{Picture, PictureType},
    tag::{Accessor, ItemKey, ItemValue, Tag, TagItem},
};
use log::debug;
use serde_json::{json, Value};
use tokio::pin;

use crate::{
    client::DzClient,
    error::{DzError, DzResult},
    util::{is_response_error, try_extract},
};

impl StreamFormat {
    pub fn extension(&self) -> &'static str {
        match self {
            StreamFormat::Flac => "flac",
            StreamFormat::Mp3_320 => "mp3",
            StreamFormat::Mp3_128 => "mp3",
        }
    }
}

impl Display for StreamFormat {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self {
            StreamFormat::Flac => write!(f, "FLAC"),
            StreamFormat::Mp3_320 => write!(f, "MP3_320"),
            StreamFormat::Mp3_128 => write!(f, "MP3_128"),
        }
    }
}

impl ArtistInfo {
    pub(crate) async fn get(artist_id: u64, client: &DzClient) -> DzResult<ArtistInfo> {
        let payload = HashMap::from([
            ("art_id".into(), artist_id.into()),
            ("lang".into(), "us".into()),
        ]);
        let response = client
            .call_method_proxy("deezer.pageArtist", payload)
            .await?;
        let mut artist_info: ArtistInfo = try_extract(&response, "/results/DATA")?;
        let bio = &response["results"]["BIO"];
        if bio.is_object() {
            artist_info.biography = Some(serde_json::from_value(bio.to_owned()).map_err(|e| {
                DzError::UpstreamError(format!("Couldn't deserialize biography: {e:?}"))
            })?);
        }
        artist_info.highlights = IncompleteSong::try_from_array(
            &response["results"]["HIGHLIGHT"]["ITEM"]["SONGS"]["data"],
        )?;
        artist_info.top_songs =
            IncompleteSong::try_from_array(&response["results"]["TOP"]["data"])?;
        artist_info.related_artists = try_extract(&response, "/results/RELATED_ARTISTS/data")?;
        artist_info.related_playlists = try_extract(&response, "/results/RELATED_PLAYLIST/data")?;

        Ok(artist_info)
    }

    /// This song's cover URL in the absolute maximum quality.
    pub fn cover_url(&self) -> String {
        format!(
            "https://cdn-images.dzcdn.net/images/artist/{}/3000x0-000000-80-0-0.jpg",
            self.artist_picture
        )
    }

    /// This artist's cover image as a jpeg byte array.
    pub async fn cover_jpg(&self, client: &DzClient) -> DzResult<Vec<u8>> {
        Ok(client
            .client
            .get(self.cover_url())
            .send()
            .await?
            .bytes()
            .await?
            .to_vec())
    }
}

impl DiscographyIds {
    /// Album IDs in this discography. These IDs are guaranteed to be valid
    /// Deezer album IDs.
    pub fn ids(&self) -> &Vec<u64> {
        &self.album_ids
    }

    /// Whether this discography includes credited albums or not.
    pub fn include_credited(&self) -> bool {
        self.include_credited
    }
}

impl Album {
    /// Whether this is a live album or not.
    pub fn is_live(&self) -> bool {
        self.attributes.is_live
    }

    /// Whether this album is a compilation or not. This method goes through
    /// each song and verifies if there's more than 1 different artist.
    ///
    /// Note that you can also use [`Album::is_dz_compilation`], although that
    /// method might be slightly less accurate than this one.
    pub fn is_compilation(&self) -> bool {
        self.songs.len() > 1
            && self
                .songs
                .iter()
                .skip(1)
                .any(|s| s.artist_id != self.songs[0].artist_id)
    }

    /// Whether this album is a compilation or not (according to deezer).
    ///
    /// This attribute might be true even if the album isn't a compilation,
    /// because the data curators/label don't always do their due diligence.
    pub fn is_dz_compilation(&self) -> bool {
        self.attributes.is_compilation
    }

    pub(crate) fn from_value(value: &Value) -> DzResult<Self> {
        try_extract(value, "/results/DATA")
    }

    /// Get the total size (in bytes) of this album.
    pub fn byte_size(&self, stream_format: &StreamFormat) -> usize {
        self.songs.iter().map(|s| s.byte_size(stream_format)).sum()
    }

    /// Release date of this album.
    ///
    /// This will return the release date, if available, in the following order:
    ///
    /// - `ORIGINAL_RELEASE_DATE`
    /// - `PHYSICAL_RELEASE_DATE`
    /// - `DIGITAL_RELEASE_DATE`
    ///
    /// Note that the release date isn't guaranteed to be a valid date: this function
    /// simply passes through whatever the API sent to the client. Upstream dates
    /// usually follow this format: `YYYY-MM-DD`.
    pub fn release_date(&self) -> Option<String> {
        self.original_release_date
            .as_ref()
            .map(ToOwned::to_owned)
            .or_else(|| self.physical_release_date.as_ref().map(ToOwned::to_owned))
            .or_else(|| self.digital_release_date.as_ref().map(ToOwned::to_owned))
    }

    /// Release year of this album.
    ///
    /// This function will attempt to get the release date, and assuming the album has
    /// at least one valid release date (see `release_date`), will then attempt to parse the year out of it.
    ///
    /// Please see `release_date()`'s documentation for possible issues and limitations.
    pub fn release_year(&self) -> Option<u32> {
        self.release_date()
            .and_then(|d| d.get(0..4).map(|year| year.parse::<u32>()))
            .transpose()
            .unwrap_or_default()
    }

    /// This song's cover URL in the absolute maximum quality.
    pub fn cover_url(&self) -> String {
        format!(
            "https://cdns-images.dzcdn.net/images/cover/{}/3000x0-000000-100-0-0.jpg",
            self.album_picture
        )
    }

    /// Upgrade this basic album instance into one that also has its cover image.
    pub async fn into_album_w_cover(self, client: &DzClient) -> DzResult<AlbumWCover> {
        Ok(AlbumWCover {
            cover: self.cover_jpg(client).await?,
            album: self,
        })
    }

    /// This album's cover image as a jpeg byte array.
    pub async fn cover_jpg(&self, client: &DzClient) -> DzResult<Vec<u8>> {
        Ok(client
            .client
            .get(self.cover_url())
            .send()
            .await?
            .bytes()
            .await?
            .to_vec())
    }

    /// Number of disks in this album.
    pub fn disk_count(&self) -> u64 {
        self.songs.iter().map(|s| s.disk_number).max().unwrap_or(0)
    }
}

impl From<(Album, Vec<u8>)> for AlbumWCover {
    fn from(value: (Album, Vec<u8>)) -> Self {
        AlbumWCover {
            cover: value.1,
            album: value.0,
        }
    }
}

impl Deref for Song {
    type Target = IncompleteSong;

    fn deref(&self) -> &Self::Target {
        &self.song
    }
}

impl DerefMut for Song {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.song
    }
}

impl Deref for AlbumWCover {
    type Target = Album;

    fn deref(&self) -> &Self::Target {
        &self.album
    }
}

impl DerefMut for AlbumWCover {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.album
    }
}

impl Song {
    pub(crate) fn try_from_response(value: &FallibleAPIResponse) -> DzResult<Song> {
        let pointer = match value {
            FallibleAPIResponse::Fallback(_) => "/results/DATA/FALLBACK",
            FallibleAPIResponse::Normal(_) => "/results/DATA",
        };
        let mut song = try_extract::<Song>(value, pointer)?;

        if song.lyrics.is_none() {
            if let Ok(lyrics) = try_extract(value, "/results/LYRICS") {
                song.lyrics = lyrics;
            }
        }
        Ok(song)
    }

    /// Whether this song is unavailable everywhere. This usually means you can't stream
    /// it with whatever ARL you might have, because it's been unpublished.
    pub fn is_unavailable(&self) -> bool {
        self.available_countries.stream_ads.is_empty()
            && self.available_countries.stream_sub_only.is_empty()
    }

    /// Whether the passed country can stream this song.
    ///
    /// This must be an uppercase, ISO 3166 country code.
    pub fn country_can_stream<S: AsRef<str>>(&self, country: S) -> bool {
        let c = country.as_ref();
        self.available_countries.stream_ads.iter().any(|it| it == c)
            || self
                .available_countries
                .stream_sub_only
                .iter()
                .any(|it| it == c)
    }

    /// Get a set containing all the available countries where this song is available.
    ///
    /// This method is as efficient as it gets because it only returns a set containing
    /// references to the countries where this song is available.
    pub fn available_countries(&self) -> HashSet<&String> {
        let mut countries = HashSet::with_capacity(
            self.available_countries
                .stream_ads
                .len()
                .max(self.available_countries.stream_sub_only.len()),
        );
        countries.extend(self.available_countries.stream_ads.iter());
        countries.extend(self.available_countries.stream_sub_only.iter());
        countries
    }

    /// Does the same thing as [`Song::available_countries`] but in a slightly less
    /// efficient way, because it clones quite a lot of stuff.
    pub fn available_countries_owned(&self) -> HashSet<String> {
        self.available_countries()
            .into_iter()
            .map(|c| c.to_owned())
            .collect()
    }

    /// Get the encrypted stream URL for this song.
    ///
    /// You probably don't want to use this function, unless you
    /// want to reinvent the wheel.
    pub async fn stream_url(
        &self,
        client: &DzClient,
        stream_format: &StreamFormat,
    ) -> DzResult<String> {
        client.can_stream_at_format(self, stream_format)?;

        let license_token = &client.creds.license_token;
        let payload = json!({
            "license_token": license_token,
            "media": [{
                "type": "FULL",
                "formats": [{
                    "cipher": "BF_CBC_STRIPE",
                    "format": stream_format.to_string(),
                }]
            }],
            "track_tokens": [self.track_token]
        });
        debug!("url dl: payload: {payload:#?}");

        let response: Value = client
            .client
            .post("https://media.deezer.com/v1/get_url")
            .json(&payload)
            .send()
            .await?
            .error_for_status()?
            .json()
            .await?;

        debug!("dz: url dl response: {response:#?}");
        is_response_error(&response)?;

        let extracted = &response["data"][0]["media"][0]["sources"][0]["url"];
        if extracted.is_null() {
            return Err(DzError::UpstreamError(
                "Stream URL couldn't be extracted".into(),
            ));
        }
        let url = extracted.as_str().ok_or(DzError::UpstreamError(
            "Stream URL: expected str, got something else".into(),
        ))?;

        Ok(url.into())
    }

    /// Get the decrypted, ready to use stream for this song.
    ///
    /// See [`SongStream`]'s documentation for more info.
    pub async fn stream(
        &self,
        client: &DzClient,
        stream_format: &StreamFormat,
    ) -> DzResult<SongStream<impl Stream<Item = Result<Bytes, reqwest::Error>>>> {
        let stream = self.raw_encrypted_stream(client, stream_format).await?;
        Self::decrypt_reqwest_stream(stream, self.stream_key())
    }

    /// Decrypt any reqwest stream with the given key using deezer's digital restrictions
    /// management algorithm.
    pub fn decrypt_reqwest_stream(
        stream: impl Stream<Item = Result<Bytes, reqwest::Error>>,
        key: Vec<u8>,
    ) -> DzResult<SongStream<impl Stream<Item = Result<Bytes, reqwest::Error>>>> {
        SongStream::new(stream, key)
    }

    /// Get the raw encrypted stream for this song.
    ///
    /// You probably don't want to use this method, unless you want to reinvent the wheel.
    pub async fn raw_encrypted_stream(
        &self,
        client: &DzClient,
        stream_format: &StreamFormat,
    ) -> DzResult<impl Stream<Item = Result<Bytes, reqwest::Error>>> {
        let url = self.stream_url(client, stream_format).await?;
        let stream = client
            .client
            .get(url)
            .send()
            .await?
            .error_for_status()?
            .bytes_stream();

        Ok(stream)
    }

    /// Get the decrypted, ready-to-use contents of this song.
    ///
    /// This function may potentially cause out-of-memory behaviors if the song is
    /// big enough. You should always use `stream` over this function.
    pub async fn bytes(
        &self,
        client: &DzClient,
        stream_format: &StreamFormat,
    ) -> DzResult<Vec<u8>> {
        let stream = self.stream(client, stream_format).await?;
        let mut contents = Vec::with_capacity(self.byte_size(stream_format));
        pin!(stream);
        while let Some(chunk) = stream.next().await.transpose()? {
            contents.extend(chunk);
        }
        Ok(contents)
    }

    /// Apply this song's tags to a `File`
    ///
    /// This method consumes the passed `File`, and returns Ok(()) on success. Otherwise,
    /// a [`DzError::TaggingError`]! is returned.
    ///
    /// Arguments:
    ///
    /// * `file`: File to apply tags to. **IMPORTANT:** This file must've been opened in both **READ and WRITE** mode.
    /// * `album`: Album containing this song.
    /// * `picture`: Album cover (can be obtained using `cover_jpg`)
    /// * `extra_opts`: Extra tagging options
    pub async fn file_apply_tags(
        &self,
        mut file: File,
        album: &Album,
        picture: Vec<u8>,
        extra_opts: ExtraTagInfo,
    ) -> DzResult<()> {
        let this = self.clone();
        let release_date = album.release_date().as_ref().map(ToOwned::to_owned);
        let original_release_date = album.original_release_date.as_ref().map(ToOwned::to_owned);
        let label = album.label.as_ref().map(ToOwned::to_owned);
        let year = album.release_year();
        let genres = (!album.genres.is_empty()).then_some(album.genres.join(", "));
        let parsing_mode = extra_opts.parsing_mode.unwrap_or_default();

        // tagging task might block the async executor due to the use of
        // synchronous API (`File`), so we have to run it inside the threadpool.
        let tagger_task = tokio::task::spawn_blocking(move || {
            file.rewind()
                .map_err(|e| DzError::TaggingError(format!("rewind error: {e:?}")))?;
            let opts = ParseOptions::default().parsing_mode(parsing_mode);
            let mut file = BoundTaggedFile::read_from(file, opts)
                .map_err(|e| DzError::TaggingError(format!("tagger open error: {e:?}")))?;
            let tag = Tag::new(file.primary_tag_type());
            file.insert_tag(tag);

            let tag = file
                .primary_tag_mut()
                .ok_or_else(|| DzError::TaggingError("Can't insert primary tag".into()))?;
            let mut picture = Picture::from_reader(&mut Cursor::new(picture))
                .map_err(|e| DzError::TaggingError(e.to_string()))?;
            picture.set_pic_type(PictureType::CoverFront);
            tag.push_picture(picture);
            tag.set_artist(this.song.artist_name);

            if extra_opts.is_compilation {
                tag.insert_text(lofty::tag::ItemKey::AlbumArtist, "Various Artists".into());
            }

            tag.set_album(this.song.album_name);
            tag.set_title(this.song.title);
            tag.set_track(this.song.track_number as u32);
            tag.set_disk(this.song.disk_number as u32);
            tag.insert_text(lofty::tag::ItemKey::Isrc, this.song.isrc);

            if let Some(year) = year {
                // NOTE: Most ID3 parsers and music players consider "DATE" and the release year,
                // and don't even bother reading the actual "YEAR" tag. We try to overcome this
                // issue by setting RecordingDate (internally mapped by lofty to "DATE").
                tag.insert_text(lofty::tag::ItemKey::RecordingDate, year.to_string());
                tag.set_year(year);
            }
            if let Some(label) = label {
                tag.insert_text(lofty::tag::ItemKey::Label, label);
            }
            if let Some(gain) = this.song.gain {
                tag.insert_text(lofty::tag::ItemKey::ReplayGainTrackGain, gain);
            }
            if let Some(lyrics) = this.lyrics {
                // Some music players/tag editors can't read the default, standard `LYRICS`
                // attribute but they do recognize the UNSYNCEDLYRICS one.
                tag.insert_unchecked(TagItem::new(
                    ItemKey::Unknown("UNSYNCEDLYRICS".into()),
                    ItemValue::Text(lyrics.text.clone()),
                ));
                tag.insert_text(lofty::tag::ItemKey::Lyrics, lyrics.text);
            }
            if let Some(genre) = genres {
                tag.set_genre(genre);
            }
            if let Some(release_date) = release_date {
                tag.insert_text(lofty::tag::ItemKey::ReleaseDate, release_date);
            }
            if let Some(original_release_date) = original_release_date {
                tag.insert_text(
                    lofty::tag::ItemKey::OriginalReleaseDate,
                    original_release_date,
                );
            }
            file.save(WriteOptions::default())
                .map_err(|e| DzError::TaggingError(e.to_string()))?;
            let mut file = file.into_inner();
            file.flush()
                .map_err(|e| DzError::TaggingError(e.to_string()))?;
            Ok::<_, DzError>(())
        });
        tagger_task
            .await
            .map_err(|e| DzError::TaggingError(e.to_string()))??;

        Ok(())
    }
}

impl IncompleteSong {
    pub(crate) fn try_from_array(value: &Value) -> DzResult<Vec<IncompleteSong>> {
        let Some(array) = value.as_array() else {
            return Err(DzError::UpstreamError(
                "Couldn't deserialize song array".into(),
            ));
        };
        let mut ret = vec![];
        for item in array {
            ret.push(Self::try_from_object(item)?)
        }
        Ok(ret)
    }

    pub(crate) fn try_from_object(value: &Value) -> DzResult<IncompleteSong> {
        if value["FALLBACK"].is_object() {
            return try_extract(value, "/FALLBACK");
        }
        try_extract(value, "")
    }

    /// Whether upstream has a lyrics object available for this song.
    pub fn lyrics_available(&self) -> bool {
        self.lyrics_id != 0
    }

    /// Fetch the lyrics and populate this song's lyrics attribute,
    /// if they're available.
    /// [`DzError::NotFound`] is returned if this song doesn't have
    /// any lyrics.
    pub async fn fetch_lyrics(&self, client: &DzClient) -> DzResult<SongLyrics> {
        if !self.lyrics_available() {
            return Err(DzError::NotFound);
        }
        let payload = HashMap::from([("sng_id".into(), self.id.into())]);
        let response = client.call_method_proxy("song.getLyrics", payload).await?;
        try_extract(&response, "/results")
    }

    /// Whether this song is available at the passed [`StreamFormat`].
    #[inline]
    pub fn is_stream_format_available(&self, stream_format: &StreamFormat) -> bool {
        match stream_format {
            StreamFormat::Flac => self.filesize_flac > 0,
            StreamFormat::Mp3_320 => self.filesize_mp3_320 > 0,
            StreamFormat::Mp3_128 => self.filesize_mp3_128 > 0,
        }
    }

    /// Return the next lower-quality format given a [`StreamFormat`]. Currently, only "normal"
    /// formats are implemented: FLAC, decent MP3 (320) and crappy MP3 (128).
    ///
    /// This function will inevitably return None if MP3 128 is passed.
    #[inline]
    pub fn fallback_format(&self, stream_format: &StreamFormat) -> Option<StreamFormat> {
        match stream_format {
            StreamFormat::Flac => {
                if self.is_stream_format_available(&StreamFormat::Mp3_320) {
                    return Some(StreamFormat::Mp3_320);
                } else if self.is_stream_format_available(&StreamFormat::Mp3_128) {
                    return Some(StreamFormat::Mp3_128);
                }
            }
            StreamFormat::Mp3_320 => {
                if self.is_stream_format_available(&StreamFormat::Mp3_128) {
                    return Some(StreamFormat::Mp3_128);
                }
            }
            _ => {}
        };
        None
    }

    /// Get the encryption key for this song, just for the lulz.
    ///
    /// You don't want to use this function: [`Song::stream`] already provides
    /// a nice way to stream this song's contents.
    pub fn stream_key(&self) -> Vec<u8> {
        let md5 = format!("{:x}", md5::compute(self.id.to_string()));
        let hash = md5.as_bytes();

        assert_eq!(hash.len(), 32, "Hash length mismatch (md5 library error)");
        (0..16).fold(Vec::<u8>::with_capacity(16), |mut acc, i| {
            let byte = hash[i] ^ hash[i + 16] ^ KEY[i];
            acc.push(byte);
            acc
        })
    }

    /// This song's cover URL in the absolute maximum quality.
    pub fn cover_url(&self) -> String {
        format!(
            "https://cdns-images.dzcdn.net/images/cover/{}/3000x0-000000-100-0-0.jpg",
            self.album_picture
        )
    }

    /// This song's cover image as a jpeg byte array.
    pub async fn cover_jpg(&self, client: &DzClient) -> DzResult<Vec<u8>> {
        Ok(client
            .client
            .get(self.cover_url())
            .send()
            .await?
            .bytes()
            .await?
            .to_vec())
    }

    /// This song's size, in bytes.
    pub fn byte_size(&self, stream_format: &StreamFormat) -> usize {
        match stream_format {
            StreamFormat::Flac => self.filesize_flac,
            StreamFormat::Mp3_320 => self.filesize_mp3_320,
            StreamFormat::Mp3_128 => self.filesize_mp3_128,
        }
    }
}
