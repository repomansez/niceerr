use std::ops::Deref;

use log::error;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use thiserror::Error;

/// A type that represents a deezer API response.
///
/// This type is used internally to determine whether
/// any given type (e.g. an album) needs to be retrieved
/// again using the fallback ID.
///
/// This type implements the `Deref` trait so you can use this
/// just like a normal `Value`.
#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum FallibleAPIResponse {
    /// Normal response.
    Normal(Value),
    /// Fallback response.
    Fallback(Value),
}

impl FallibleAPIResponse {
    pub fn into_inner(self) -> Value {
        match self {
            Self::Normal(t) | Self::Fallback(t) => t,
        }
    }
}

impl Deref for FallibleAPIResponse {
    type Target = Value;

    fn deref(&self) -> &Self::Target {
        match self {
            Self::Fallback(t) | Self::Normal(t) => t,
        }
    }
}

/// Reason why a song might not be available for streaming.
#[derive(Error, Debug, PartialEq)]
pub enum UnavailableReason {
    #[error("contents not available in your country, try using an ARL from another country")]
    /// Geoblocked: the song is not available in the client's region.
    Geoblocked,
    /// Song isn't available anywhere in the globe
    #[error("contents are globally unavailable")]
    Unpublished,
    #[error("requested stream quality is unavailable, try a different quality instead")]
    InvalidStreamQuality,
}

#[derive(Error, Debug)]
pub enum DzError {
    /// Returned only if the API doesn't return
    /// something we were expecting, like a number instead
    /// of a string, or a missing attribute.
    #[error("Upstream error")]
    UpstreamError(String),
    /// Access to the contents require a premium
    /// subscription.
    #[error("Subscription required")]
    SubscriptionRequired,
    /// An error occurred while decrypting a song's stream.
    /// This should never happen, but if it ever happens then the
    /// CDN most likely corrupted some data.
    #[error("Decryption error")]
    DecryptionError,
    /// Contents couldn't be found upstream
    #[error("Not found")]
    NotFound,
    /// Contents are unavailable for streaming.
    #[error("Unavailable: {0}")]
    Unavailable(UnavailableReason),
    /// Tagging error (usually a lofty error, but
    /// can also happen if we were unable to rewind
    /// the file).
    #[error("Tagging error: {0}")]
    TaggingError(String),
    /// Reqwest error
    #[error("Reqwest error: {0}")]
    ReqwestError(#[from] reqwest::Error),
}

pub type DzResult<T> = Result<T, DzError>;

impl PartialEq for DzError {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::UpstreamError(l0), Self::UpstreamError(r0)) => l0 == r0,
            (Self::TaggingError(l0), Self::TaggingError(r0)) => l0 == r0,
            (Self::Unavailable(l0), Self::Unavailable(r0)) => l0 == r0,
            (Self::ReqwestError(l0), Self::ReqwestError(r0)) => {
                l0.status() == r0.status()
                    && l0.url() == r0.url()
                    && l0.to_string() == r0.to_string()
            }
            _ => core::mem::discriminant(self) == core::mem::discriminant(other),
        }
    }
}
