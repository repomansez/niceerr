use bytes::{Bytes, BytesMut};
use cbc::cipher::{block_padding::NoPadding, BlockDecryptMut, KeyIvInit};
use futures_util::Stream;
use log::error;
use std::task::Poll;

use crate::{
    error::{DzError, DzResult},
    types::{CbcBlowfish, IV},
};

const CHUNK_SIZE: usize = 2048;
/// Default buffer size used by reqwest
const BUFFER_CAPACITY: usize = 16384;
type DzStream = Poll<Option<DzResult<Bytes>>>;

struct SongStreamInner {
    buff: BytesMut,
    processed: BytesMut,
    chunk_index: usize,
    cipher: CbcBlowfish,
}

impl SongStreamInner {
    fn new(key: Vec<u8>) -> DzResult<Self> {
        let cipher = CbcBlowfish::new_from_slices(&key, IV.as_slice())
            .map_err(|_| DzError::DecryptionError)?;

        Ok(Self {
            cipher,
            buff: BytesMut::with_capacity(BUFFER_CAPACITY),
            processed: BytesMut::with_capacity(BUFFER_CAPACITY),
            chunk_index: 0,
        })
    }

    #[inline]
    fn poll_handle_last_chunk(&mut self) -> DzStream {
        if self.buff.is_empty() {
            return Poll::Ready(None);
        }

        if self.buff.len() == CHUNK_SIZE && self.chunk_index % 3 == 0 {
            let cipher = self.cipher.clone();
            Self::decrypt_chunk(&mut self.buff, cipher)?;
        }

        let bytes = self.buff.split().freeze();
        Poll::Ready(Some(Ok(bytes)))
    }

    fn handle_chunk(&mut self, chunk: Bytes, cx: &mut std::task::Context<'_>) -> DzStream {
        self.buff.extend_from_slice(&chunk);
        drop(chunk);
        self.processed.clear();

        while self.buff.len() >= CHUNK_SIZE {
            let mut current_chunk = self.buff.split_to(CHUNK_SIZE);

            if self.chunk_index % 3 == 0 {
                let cipher = self.cipher.clone();
                Self::decrypt_chunk(&mut current_chunk, cipher)?;
            }

            self.processed.extend_from_slice(&current_chunk);
            self.chunk_index += 1;
        }

        if self.processed.is_empty() {
            cx.waker().wake_by_ref();
            Poll::Pending
        } else {
            Poll::Ready(Some(Ok(self.processed.split().freeze())))
        }
    }

    #[inline(always)]
    fn decrypt_chunk(chunk: &mut [u8], cipher: CbcBlowfish) -> DzResult<()> {
        cipher
            .decrypt_padded_mut::<NoPadding>(chunk)
            .map(|_| ())
            .map_err(|e| {
                error!("Cannot decrypt block: {e:?}");
                DzError::DecryptionError
            })
    }
}

/// A stream of decrypted song data.
///
/// This type implements future's `Stream` trait. All you need to do
/// is basically call `next()` and do whatever you want with the
/// returned bytes until it returns None.
#[pin_project::pin_project]
pub struct SongStream<S: Stream<Item = Result<Bytes, reqwest::Error>>> {
    #[pin]
    stream: S,
    inner: SongStreamInner,
}

impl<S> SongStream<S>
where
    S: Stream<Item = Result<Bytes, reqwest::Error>>,
{
    pub(crate) fn new(stream: S, key: Vec<u8>) -> DzResult<Self> {
        Ok(Self {
            stream,
            inner: SongStreamInner::new(key)?,
        })
    }
}

impl<S> Stream for SongStream<S>
where
    S: Stream<Item = Result<Bytes, reqwest::Error>>,
{
    type Item = DzResult<Bytes>;

    fn poll_next(self: std::pin::Pin<&mut Self>, cx: &mut std::task::Context<'_>) -> DzStream {
        let this = self.project();

        match this.stream.poll_next(cx) {
            Poll::Ready(Some(Ok(bytes))) => this.inner.handle_chunk(bytes, cx),
            Poll::Ready(None) => this.inner.poll_handle_last_chunk(),
            Poll::Pending => Poll::Pending,
            Poll::Ready(Some(Err(e))) => Poll::Ready(Some(Err(e.into()))),
        }
    }
}
