FROM alpine:latest as builder

# Install frontend deps
RUN apk add --no-cache \
    npm \
    && npm install -g @quasar/cli

WORKDIR /app

COPY . .

# Build UI
WORKDIR /app/ui
RUN npm install && \
    npx quasar build

FROM alpine:latest

RUN apk add --no-cache nginx && mkdir -p /var/www/frontend /var/log/nginx /etc/niceerr /usr/local/bin/

# This step is performed by the gitlab CI/CD
COPY --from=builder /app/niceerr /usr/local/bin/niceerr

COPY --from=builder /app/docker /

COPY --from=builder /app/ui/dist/spa /var/www/frontend

RUN chmod +x /start.sh

ENTRYPOINT ["/start.sh"]

EXPOSE 80/tcp

VOLUME ["/etc/niceerr/.env", "/downloads"]