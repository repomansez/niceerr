# niceerr (ui)

Music discovery server

## Important note

When developing locally, you might need to change the API server URL.
You can do so by setting the API_URL to your development server:

```bash
export API_URL=http://localhost:8000/api
```

The rust server spawns a listener on 0.0.0.0:8000 by default, so executing the above command
should be more than enough.

## Install the dependencies

```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

### Lint the files

```bash
yarn lint
# or
npm run lint
```

### Format the files

```bash
yarn format
# or
npm run format
```

### Build the app for production

```bash
quasar build
```

### Customize the configuration

See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).
