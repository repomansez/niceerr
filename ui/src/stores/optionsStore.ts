import { defineStore } from 'pinia';
import { StreamFormat } from 'src/core/download';
import useCredsStore from './userCreds';
import { mergeObjects } from 'src/core/util';

type PreferredInterface = 'desktop' | 'mobile' | 'auto';

interface Options {
  v: number;
  enableWildcardSearch: boolean;
  useIsoDates: boolean;
  // Download options
  allowFallback: boolean;
  skipLive: boolean;
  includeCredited: boolean;
  ignoreErrors: boolean;
  streamDefault: boolean;
  streamFormat: StreamFormat;
  // Download history view options
  downloadViewShowUserId: boolean;
  hideUnsupportedPlatformWarnings: boolean;
  interface: PreferredInterface;
  interfaceBlur: boolean;
  interfaceFullBlack: boolean;
}

const DEFAULTS = {
  v: 3,
  enableWildcardSearch: true,
  useIsoDates: false,
  allowFallback: true,
  skipLive: false,
  includeCredited: false,
  ignoreErrors: false,
  streamDefault: false,
  streamFormat: 'Flac' as StreamFormat,
  downloadViewShowUserId: false,
  hideUnsupportedPlatformWarnings: false,
  interface: 'auto' as PreferredInterface,
  interfaceBlur: true,
  interfaceFullBlack: false,
};

const useOptionsStore = defineStore('globalOptions', {
  state: () => ({
    options: null as Options | null,
  }),

  getters: {
    /// Get the default background style
    defaultBg(state): string {
      if (!state.options) {
        console.warn('Options not loaded');
      }

      if (state.options?.interfaceBlur === false) {
        return 'bg-black';
      }
      return 'blur-background';
    },

    fullBlack(state): string {
      if (!state.options) {
        console.warn('Options not loaded');
      }

      if (state.options?.interfaceFullBlack === true) {
        return 'bg-black';
      }
      return '';
    },
  },

  actions: {
    set(v: Options) {
      const credStore = useCredsStore();
      // If stream or server-side downloads are disabled for the caller,
      // modify the default options. This will cause the main download
      // page to switch the download button accordingly.
      if (credStore.canStreamOnly) {
        v.streamDefault = true;
      } else if (credStore.canServerSideOnly) {
        v.streamDefault = false;
      }
      this.options = v;
    },

    getOrSetDefault(): Options {
      const current = this.$state.options;
      if (current) {
        // Merge any missing options in the user's outdated prefs
        if (current.v !== DEFAULTS.v) {
          console.info(
            `UI upgrade: triggering merge (curr=${current.v}, new=${DEFAULTS.v})`
          );
          const mergedState = mergeObjects(
            JSON.parse(JSON.stringify(this.$state)),
            DEFAULTS
          ) as Options;
          mergedState.v = DEFAULTS.v;
          this.set(mergedState as Options);
        }

        const credStore = useCredsStore();
        if (credStore.canStreamOnly) {
          current.streamDefault = true;
        } else if (credStore.canServerSideOnly) {
          current.streamDefault = false;
        }
        return current;
      }

      this.set(DEFAULTS);
      return DEFAULTS;
    },
  },

  persist: true,
});

export default useOptionsStore;
export type { Options };
