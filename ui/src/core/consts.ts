const FRONTEND_VERSION = '1.66.1';

// Run the refresh token function in the main thread every this many
// milliseconds. Set by default to 60s (1 minute)
const RUN_REFRESH_TASK_INTERVAL_MSEC = 60 * 1000;

// When running the refresh token function, refresh the token IF and ONLY IF
// this many minutes have elapsed since we last updated it.
// Set by default to 5 minutes.
const REFRESH_TOKEN_EVERY_MIN = 60 * 3;

// Only used for confirm-type buttons. These buttons allow the user
// to confirm they're really sure of taking certain actions. After that, the button
// will just go back to normal.
const CONFIRM_BUTTON_TIMEOUT_MSEC = 5000;
const CONFIRM_BUTTON_TICKET_INTERVAL_MSEC = 200;

// Default number of rows to pull when querying info, and rows-per-page presets.
const TABLE_DEFAULT_ROWS = 50;
const TABLE_ROWS_PER_PAGE_ARRAY = [TABLE_DEFAULT_ROWS, 100, 300, 666];

// Strings longer than this limit will be shortened as "Some long stri..."
const MAX_STRING_LENGTH_OVERFLOW = 25;

// After not receiving a ping or data from the server, reset any existing
// SSE connections after this many mseconds.
// This timeout should almost never be used, because the server sends the
// keep-alive interval it'll be using.
const SSE_RESET_CONNECTION_TIMEOUT_MSEC = 20_000;

// When the server returns the keep-alive interval it'll be using, add
// this extra time to it. This is a safety buffer in case there are network
// issues before we abruptly restart the SSE stream.
const SSE_EXTRA_TIMEOUT_MSEC = 10000;

const CHECKBOX_COLOR = 'grey-8';
const RADIO_COLOR = 'grey-6';
const MUTED_CHIP_COLOR = 'grey-6';
const WARNING_COLOR = 'orange-10';

export {
  RUN_REFRESH_TASK_INTERVAL_MSEC,
  REFRESH_TOKEN_EVERY_MIN,
  CONFIRM_BUTTON_TICKET_INTERVAL_MSEC,
  CONFIRM_BUTTON_TIMEOUT_MSEC,
  TABLE_DEFAULT_ROWS,
  FRONTEND_VERSION,
  TABLE_ROWS_PER_PAGE_ARRAY,
  MAX_STRING_LENGTH_OVERFLOW,
  SSE_RESET_CONNECTION_TIMEOUT_MSEC,
  SSE_EXTRA_TIMEOUT_MSEC,
  CHECKBOX_COLOR,
  RADIO_COLOR,
  MUTED_CHIP_COLOR,
  WARNING_COLOR,
};
