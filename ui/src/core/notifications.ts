import { QNotifyUpdateOptions } from 'quasar';
import { Notify } from 'quasar';
import useOptionsStore from 'src/stores/optionsStore';

type NOTIFICATION_TYPE = 'normal' | 'positive' | 'negative' | 'warning';

function notifColorClass(kind: NOTIFICATION_TYPE): string | undefined {
  const optionsStore = useOptionsStore();
  const interfaceBlur = optionsStore.options?.interfaceBlur;
  if (optionsStore.fullBlack || kind == 'normal') {
    return interfaceBlur
      ? 'notif-black-blur-background'
      : 'notif-black-background';
  }
  if (kind == 'positive') {
    return interfaceBlur ? 'notif-positive-blur-background' : 'bg-positive';
  } else if (kind == 'negative') {
    return interfaceBlur ? 'notif-negative-blur-background' : 'bg-negative';
  } else if (kind == 'warning') {
    return interfaceBlur ? 'notif-warning-blur-background' : 'bg-warning';
  }

  return undefined;
}

class QNotification {
  private inner: (props?: QNotifyUpdateOptions) => void;

  constructor(
    title: string,
    caption: string,
    icon: string | undefined = undefined,
    type: string | undefined = undefined,
    group = false,
    timeout = 0
  ) {
    this.inner = Notify.create({
      type: type,
      group: group,
      progress: true,
      message: title,
      caption: caption,
      icon: icon,
      timeout: timeout,
      classes: notifColorClass('normal'),
    });
  }

  public addAction(label: string, color: string, callback: () => void) {
    this.inner({
      actions: [
        { label: label, color: color, outline: true, handler: callback },
      ],
    });
  }

  public dismiss() {
    this.inner();
  }

  public updateMessage(title: string, caption: string) {
    this.inner({ message: title, caption: caption });
  }

  public enableSpinner() {
    this.inner({ spinner: true });
  }

  public disableSpinner() {
    this.inner({ spinner: false });
  }

  public finishWarning() {
    this.inner({
      type: 'warning',
      progress: true,
      spinner: false,
      classes: notifColorClass('warning'),
      timeout: 7000,
    });
  }

  public finishPositive() {
    this.inner({
      icon: 'check',
      progress: true,
      spinner: false,
      classes: notifColorClass('positive'),
      timeout: 7000,
    });
  }

  public finishNegative() {
    this.inner({
      actions: [],
    });
    this.inner({
      icon: 'error',
      progress: true,
      spinner: false,
      timeout: 7000,
      classes: notifColorClass('negative'),
    });
  }
}

export default QNotification;
