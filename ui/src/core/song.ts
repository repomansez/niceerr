import { useApi } from 'src/boot/axios';
import { PaginatedResponse, Pagination } from './pagination';

interface SongEntry{
  id: string;
  sessionId: string;
  itemId: string;
  totalBytes: number;
  downloadedBytes: number;
  downloadPath: string;
  errorMessage: string;
}

async function fetchSongs(pagination: Pagination): Promise<PaginatedResponse<SongEntry>>{
  const response = await useApi().get(`download/history/songs${pagination.asQueryParams()}`);
  return new PaginatedResponse(response);
}

export type { SongEntry };
export { fetchSongs };
