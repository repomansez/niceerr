import { useApi } from 'src/boot/axios';
import useCredsStore from 'src/stores/userCreds';
import { User } from './user';
import useOptionsStore from 'src/stores/optionsStore';

type StreamFormat = 'Mp3_128' | 'Mp3_320' | 'Flac';

interface DownloadRequest {
  link: string;
  streamFormat: StreamFormat;
  includeCredited: boolean;
  allowFallback: boolean;
  skipLive: boolean;
}

interface DownloadResponse {
  id: string;
  dlBytesSoFar: number;
  totalBytes: number;
  irrecoverableErrors: number;
  downloadSession: SingleDownloadSession;
}

interface SingleDownloadSession {
  id: string;
  downloadId: string; // Upstream content ID
  downloadQuality: string;
  downloadKind: string;
  isStream: boolean;
  includeCredited: boolean;
  allowFallback: boolean;
  skipLive: boolean;
}

/// The nice download view interface.
interface OngoingDownload {
  username: string;
  userId: string;
  dlBytesSoFar: number;
  totalBytes: number;
  irrecoverableErrors: number;
  downloadSession: SingleDownloadSession;
}

interface OngoingUserDlEntry {
  user: User;
  ongoingDownloads: DownloadResponse[];
}

class StreamDownloader {
  private link: string;

  private callback: (progress: number) => void;
  private controller: AbortController | null = null;

  constructor(link: string, callback: (progress: number) => void) {
    this.link = link;
    this.callback = callback;
  }

  async start(): Promise<void> {
    const optionsStore = useOptionsStore();
    const options = optionsStore.getOrSetDefault();
    // const response = await useApi().post('/download/link', {
    // });

    this.controller = new AbortController();

    const response = await useApi().post(
      '/download/link',
      {
        link: this.link,
        streamFormat: options.streamFormat,
        includeCredited: options.includeCredited,
        allowFallback: options.allowFallback,
        skipLive: options.skipLive,
        stream: true,
      },
      {
        responseType: 'blob',
        adapter: 'fetch',
        signal: this.controller.signal,
        onDownloadProgress: (pEvent: { loaded: number }) =>
          this.callback(pEvent.loaded),
        fetchOptions: {
          priority: 'low',
        },
      }
    );

    if (response.status !== 200) {
      throw new Error(`Error downloading zip: ${response.status}`);
    }

    const blob = new Blob([response.data], { type: 'application/zip' });
    const link = document.createElement('a');
    link.href = URL.createObjectURL(blob);
    link.download = `streamed-${options.streamFormat}.zip`;
    link.click();
    URL.revokeObjectURL(link.href);

    this.controller = null;
  }

  cancelDownload(): void {
    if (this.controller) {
      this.controller.abort();
      this.controller = null;
    }
  }
}

class DownloadRequest implements DownloadRequest {
  constructor(link: string) {
    this.link = link;
  }

  public static fromLink(link: string): DownloadRequest {
    return new DownloadRequest(link);
  }

  public async send(): Promise<DownloadResponse> {
    const credStore = useCredsStore();
    if (!credStore.isStoredUserValid) {
      throw new Error('Your session has expired. Please refresh this page.');
    }

    const optionsStore = useOptionsStore();
    const options = optionsStore.getOrSetDefault();
    const response = await useApi().post<DownloadResponse>('/download/link', {
      link: this.link,
      streamFormat: options.streamFormat,
      includeCredited: options.includeCredited,
      allowFallback: options.allowFallback,
      skipLive: options.skipLive,
    });

    return response.data;
  }
}

async function cancelDownload(userId: string, downloadId: string) {
  await useApi().delete(`/download/${userId}/${downloadId}`);
}

export { StreamDownloader, DownloadRequest, cancelDownload };
export type {
  DownloadResponse,
  OngoingDownload,
  StreamFormat,
  OngoingUserDlEntry,
};
