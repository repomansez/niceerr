import { useApi } from 'src/boot/axios';

interface Info {
  requestQueueSize: number;
  requestQueueCapacity: number;
  updateDlQueueSize: number;
  updateDlQueueCapacity: number;
  songToDbQueueSize: number;
  songToDbQueueCapacity: number;
  dbPoolConnections: number;
  dbIdleConnections: number;
  serverSideDownloadWorkers: number;
  maxGrantsPerUser: number;
  limitServiceUserCount: number;
  limitServiceGrantedPermits: number;
  songStats: SongStats;
  rssMemoryUsageBytes: number;
  streamingDownloadWorkers: number;
  streamingDownloadQueueSize: number;
  streamDownloadBufferByteSize: number;
  hotArlCacheSize: number;
  activeWorkerCount: number;
  tmpFolderSize?: number;
  serverVersion: string;
  aliveTaskCount: number;
  threadCount: number;
  sseOngoingDownloadSubscribers: number;
  startedAt: string;
  relayConfigured: boolean;
  hasValidRelayToken: boolean;
  rustVersion: string;
}

interface SongStats {
  downloadedBytes: number;
  totalBytes: number;
  totalCount: number;
  errorCount: number;
  sessionCount: number;
}

async function fetchServerInfo(): Promise<Info> {
  const url = '/info';
  const response = await useApi().get(url);
  return response.data;
}

export type { SongStats, Info };
export { fetchServerInfo };
