import { useApi } from 'src/boot/axios';
import { parseJwt } from './util';
import { PaginatedResponse, Pagination } from './pagination';
import { REFRESH_TOKEN_EVERY_MIN } from './consts';

import { z } from 'zod';

export enum Permission {
  ReadUser = 'readUser',
  UpdateUserBasicInfo = 'updateUserBasicInfo',
  UpdateUserExtendedInfo = 'updateUserExtendedInfo',
  ReadOngoingDownload = 'readOngoingDownload',
  StopOngoingDownload = 'stopOngoingDownload',
  ReadDownloadSession = 'readDownloadSession',
  PerformServerSideDownload = 'performServerSideDownload',
  PerformStreamDownload = 'performStreamDownload',
  PruneDownloadSession = 'pruneDownloadSession',
  DownloadServerSideDownloadedContent = 'downloadServerSideDownloadedContent',
  DeleteUser = 'deleteUser',
  CreateArl = 'createArl',
  ReadArl = 'readArl',
  UpdateArl = 'updateArl',
  DeleteArl = 'deleteArl',
  TriggerArlRefresh = 'triggerArlRefresh',
  ReadServerStats = 'readServerStats',
  AdminConsoleAccess = 'adminConsoleAccess',
  RelayAccess = 'relayAccess',
}

const PERMISSION_DATA: PermissionPiece[] = [
  {
    name: Permission.ReadUser,
    desc: 'User: read',
    supportsScope: true,
    detail: 'Allow this user to read their own user data',
    detailScopeAll: "Allow this user to read their own and others' user data",
  },
  {
    name: Permission.UpdateUserBasicInfo,
    desc: 'User: update basic info',
    supportsScope: true,
    detail:
      'Allow this user to update their own basic own info such as username and password',
    detailScopeAll:
      "Allow this user to update their own and others' basic own info such as username and password",
  },
  {
    name: Permission.UpdateUserExtendedInfo,
    desc: 'User: update extended info',
    supportsScope: true,
    detail:
      'Allow this user to update their own extended info (download path, persistence periods, quotas and enabled status).',
    detailScopeAll:
      "Allow this user to update their own and others' extended info (download path, persistence periods, quotas and enabled status).",
  },
  {
    name: Permission.DeleteUser,
    desc: 'User: delete',
    supportsScope: true,
    detail: 'Allow this user to delete itself',
    detailScopeAll: 'Allow this user to delete itself and other users',
  },
  {
    name: Permission.ReadOngoingDownload,
    desc: 'Ongoing downloads: read',
    supportsScope: true,
    detail: 'Allow this user to read their own ongoing downloads',
    detailScopeAll:
      "Allow this user to read their own and others' ongoing downloads",
  },
  {
    name: Permission.StopOngoingDownload,
    desc: 'Ongoing downloads: stop',
    supportsScope: true,
    detail: 'Allow this user to stop their own ongoing downloads',
    detailScopeAll:
      "Allow this user to stop their own and others' ongoing downloads",
  },
  {
    name: Permission.ReadDownloadSession,
    desc: 'Past downloads: read',
    supportsScope: true,
    detail: 'Allow this user to read their own download history',
    detailScopeAll:
      "Allow this user to read their own and others' download history",
  },
  {
    name: Permission.PerformServerSideDownload,
    desc: 'Perform server-side downloads',
    supportsScope: false,
    detail:
      'Allow this user to perform server-side downloads (uses server storage)',
    detailScopeAll: '',
  },
  {
    name: Permission.PerformStreamDownload,
    desc: 'Perform stream downloads',
    supportsScope: false,
    detail: 'Allow this user to perform streamed/direct downloads',
    detailScopeAll: '',
  },
  {
    name: Permission.PruneDownloadSession,
    desc: 'Prune past server-side downloads',
    supportsScope: true,
    detail:
      'Allow this user to prune contents of their own server-side downloads',
    detailScopeAll:
      "Allow this user to prune contents of their own and others' past server-side downloads",
  },
  {
    name: Permission.DownloadServerSideDownloadedContent,
    desc: 'Download past server-side downloads',
    supportsScope: true,
    detail:
      'Allow this user to download contents of past server-side downloads',
    detailScopeAll:
      "Allow this user to download contents of others' past server-side downloads",
  },
  {
    name: Permission.CreateArl,
    desc: 'ARLs: create',
    supportsScope: false,
    detail: 'Allow this user to create ARLs',
    detailScopeAll: '',
  },
  {
    name: Permission.ReadArl,
    desc: 'ARLs: read',
    supportsScope: true,
    detail: 'Allow this user to read their own submitted ARLs',
    detailScopeAll:
      "Allow this user to read their own and others' submitted ARLs",
  },
  {
    name: Permission.UpdateArl,
    desc: 'ARLs: update',
    supportsScope: true,
    detail: 'Allow this user to update their own ARLs',
    detailScopeAll: "Allow this user to update their own and others' ARLs",
  },
  {
    name: Permission.DeleteArl,
    desc: 'ARLs: delete',
    supportsScope: true,
    detail: 'Allow this user to delete their own ARLs',
    detailScopeAll: "Allow this user to delete their own and others' ARLs",
  },
  {
    name: Permission.TriggerArlRefresh,
    desc: 'Trigger server ARL refresh',
    supportsScope: false,
    detail: 'Allow this user to trigger a server-side ARL refresh',
    detailScopeAll: '',
  },
  {
    name: Permission.ReadServerStats,
    desc: 'Server stats: read',
    supportsScope: false,
    detail: 'Allow this user to read server statistics',
    detailScopeAll: '',
  },
  {
    name: Permission.AdminConsoleAccess,
    desc: 'Access server maintenance tools',
    supportsScope: false,
    detail: 'Allow this user to access server maintenance tools',
    detailScopeAll: '',
  },
  {
    name: Permission.RelayAccess,
    desc: 'Relay access',
    supportsScope: false,
    detail: 'Allow user to use this Niceerr server to remotely download songs',
    detailScopeAll: '',
  },
];

interface ServerPermission {
  name: Permission;
  scope: 'all' | 'caller';
}

interface PermissionPiece {
  name: Permission;
  desc: string;
  supportsScope: boolean;
  detail: string;
  detailScopeAll: string;
}

// Update PermissionSchema to use the enum values
const PermissionSchema = z.object({
  name: z.nativeEnum(Permission), // Use nativeEnum with Permission enum
  scope: z.enum(['caller', 'all']),
});

// Define the User schema
const UserSchema = z.object({
  id: z.string().uuid(),
  username: z.string(),
  createdAt: z.string().datetime(),
  level: z.number().nonnegative(),
  superuser: z.boolean(),
  enabled: z.boolean(),
  downloadPath: z.string().nullable(),
  maxBandwidthBytesPerDay: z.number().nonnegative(),
  downloadSessionPersistMinutes: z.number().nonnegative(),
  downloadedBytes: z.number().default(0),
  permissions: z.array(PermissionSchema),
});

// User class implementation
class User {
  id: string;
  username: string;
  createdAt: string;
  level: number;
  superuser: boolean;
  enabled: boolean;
  downloadPath: string | null;
  maxBandwidthBytesPerDay: number;
  downloadSessionPersistMinutes: number;
  downloadedBytes: number;
  permissions: { name: Permission; scope: 'all' | 'caller' }[];

  constructor(data: z.infer<typeof UserSchema>) {
    const validatedData = UserSchema.parse(data); // Validate input data with Zod
    this.id = validatedData.id;
    this.username = validatedData.username;
    this.createdAt = validatedData.createdAt;
    this.level = validatedData.level;
    this.superuser = validatedData.superuser;
    this.enabled = validatedData.enabled;
    this.downloadPath = validatedData.downloadPath;
    this.maxBandwidthBytesPerDay = validatedData.maxBandwidthBytesPerDay;
    this.downloadSessionPersistMinutes =
      validatedData.downloadSessionPersistMinutes;
    this.downloadedBytes = validatedData.downloadedBytes;
    this.permissions = validatedData.permissions;
  }

  isSuperuser(): boolean {
    return this.superuser;
  }

  /**
   * Whether this user has permission over other user for any given permission.
   *
   * This can be determined by looking at the scope of the permission.
   *
   * @param permission
   * @param otherUserId
   * @returns
   */
  hasPermissionOverUser(permission: Permission, otherUser: User): boolean {
    if (this.level == 0) {
      return true;
    }

    if (this.id != otherUser.id && this.level >= otherUser.level) {
      return false;
    }

    if (this.superuser) {
      return true;
    }

    const perm = this.permissions.find((p) => p.name === permission);
    if (perm === undefined) {
      return false;
    }

    if (this.id === otherUser.id || perm.scope === 'all') {
      return true;
    }
    return false;
  }

  /**
   * TODO: Find a way to get the level as this might not always indicate whether
   * we have permission over someone else's id
   * @param permission
   * @param otherUserId
   * @returns
   */
  hasPermissionOverUserId(
    permission: Permission,
    otherUserId: string
  ): boolean {
    if (this.superuser) {
      return true;
    }

    const perm = this.permissions.find((p) => p.name === permission);
    if (perm === undefined) {
      return false;
    }
    if (perm.scope === 'all' || this.id === otherUserId) {
      return true;
    }
    return false;
  }

  hasPermission(permission: Permission): boolean {
    return (
      this.superuser || this.permissions.some((p) => p.name === permission)
    );
  }
}

/**
 *  Operation type: what are we trying to do with this field update?
 *
 * Set: requires value to be set
 * Unset: no value required
 * Unchanged: no value required (doesn't even need to be used)
 */
interface FieldUpdate {
  type: 'Set' | 'Unset' | 'Unchanged';
  value?: string | number | boolean | null;
}

/**
 * List of fields that must be updated using the "field update
 * mechanism".
 *
 * This mechanism is needed because the server allows us to NULL-ify some
 * fields. In the case of user, it only applies to `downloadPath`.
 *
 * The value at the left indicates the field name, and the value at the right
 * the "empty" value that indicates this field should be unset.
 *
 * Note that we can't just simply send something like "downloadPath": null because
 * the server uses null values to indicate that a field shouldn't be updated.
 */
const NEED_ADVANCED_FIELD_UPDATE: Record<string, string> = {
  downloadPath: '',
};

interface UserUpdate {
  username?: string | null;
  password?: string | null;
  enabled?: boolean | null;
  superuser?: boolean | null;
  downloadPath?: string | null;
  streamingDownloadsEnabled?: boolean | null;
  serverSideDownloadsEnabled?: boolean | null;
  maxBandwidthBytesPerDay?: number | null;
  downloadSessionPersistMinutes?: number | null;
}

interface PruneResponse {
  success: number;
  error: number;
  sessionsPruned: number;
}

class Credentials {
  public expiresAt: number;

  constructor(
    public jwt: string,
    public user: User,
    public lastUpdatedAt: number
  ) {
    this.expiresAt = parseJwt(jwt);
  }

  private static async fromJwt(): Promise<Credentials> {
    const response = await useApi().post('/user/refresh');
    const { token, user } = response.data;
    useApi().defaults.headers.common['Authorization'] = `Bearer ${token}`;
    const now = new Date().getTime() / 1000;
    return new Credentials(token, user, now);
  }

  public static async fromUserPass(
    username: string,
    password: string
  ): Promise<Credentials> {
    const response = await useApi().post('/user/login', { username, password });
    const { token, user } = response.data;
    useApi().defaults.headers.common['Authorization'] = `Bearer ${token}`;
    const now = new Date().getTime() / 1000;
    return new Credentials(token, user, now);
  }

  public needsRefresh(): boolean {
    const now = new Date().getTime();
    const elapsed = now / 1000 - this.lastUpdatedAt;
    return elapsed > REFRESH_TOKEN_EVERY_MIN;
  }

  public async refresh(): Promise<Credentials> {
    return await Credentials.fromJwt();
  }
}

async function fetchUsers(
  pagination: Pagination
): Promise<PaginatedResponse<User>> {
  const url = `/user${pagination.asQueryParams()}`;
  return new PaginatedResponse(await useApi().get(url));
}

async function deleteUser(userId: string): Promise<void> {
  await useApi().delete(`/user/${userId}`);
}

async function createUser(user: UserUpdate): Promise<void> {
  await useApi().post('/user', user);
}

async function rawUserUpdate(
  userId: string,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  update: Record<string, any>[]
): Promise<void> {
  await useApi().put(`/user/${userId}`, update);
}

async function updateUser(userId: string, user: UserUpdate): Promise<void> {
  const payload: Record<string, FieldUpdate> = {};

  // TODO: Put this logic in utils
  /**
   * Verify whether this update needs to make use of the "Field Update"
   * logic.
   */
  for (const key in NEED_ADVANCED_FIELD_UPDATE) {
    const emptyValue = NEED_ADVANCED_FIELD_UPDATE[key];
    if (Object.prototype.hasOwnProperty.call(user, key)) {
      const updateValue = user[key as keyof UserUpdate];

      if (updateValue === emptyValue) {
        payload[key] = {
          type: 'Unset',
        };
      } else {
        payload[key] = {
          type: 'Set',
          value: updateValue,
        };
      }
    }
  }

  const updatePayload = Object.keys(payload).length === 0 ? user : payload;
  await useApi().put(`/user/${userId}`, updatePayload);
}

async function pruneUserDownloads(
  userId: string,
  allSessions: boolean,
  forcePrune: boolean
): Promise<PruneResponse> {
  const response = await useApi().delete<PruneResponse>(
    `/user/prune/${userId}?allSessions=${allSessions}&forcePrune=${forcePrune}`
  );
  return response.data;
}

async function pruneUserDownloadSession(
  userId: string,
  downloadSessionId: string,
  forcePrune: boolean
): Promise<PruneResponse> {
  const response = await useApi().delete<PruneResponse>(
    `/user/prune/${userId}/${downloadSessionId}?&forcePrune=${forcePrune}`
  );
  return response.data;
}

export {
  Credentials,
  createUser,
  fetchUsers,
  rawUserUpdate,
  deleteUser,
  updateUser,
  pruneUserDownloads,
  pruneUserDownloadSession,
  User,
  PERMISSION_DATA,
};
export type { UserUpdate, PruneResponse, ServerPermission, PermissionPiece };
