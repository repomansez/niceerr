import { boot } from 'quasar/wrappers';
import axios, { AxiosInstance } from 'axios';
import useCredsStore from 'src/stores/userCreds';

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $axios: AxiosInstance;
    $api: AxiosInstance;
  }
}

const api = axios.create({ baseURL: process.env.API_URL });

export default boot(({ app }) => {
  // for use inside Vue files (Options API) through this.$axios and this.$api
  app.config.globalProperties.$axios = axios;
  // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
  //       so you won't necessarily have to import axios in each vue file

  app.config.globalProperties.$api = api;
  // ^ ^ ^ this will allow you to use this.$api (for Vue Options API form)
  //       so you can easily perform requests against your app's API
});

function useApi() {
  const userCreds = useCredsStore();
  if (userCreds.isStoredUserValid) {
    const token = userCreds.getJwt ?? null;
    api.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  }
  return api;
}

export { useApi };
