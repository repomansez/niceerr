use futures::future::try_join;
use log::{debug, info, warn, Level};
use sea_orm::sea_query::Alias;
use sea_orm::{
    sea_query::{extension::postgres::PgExpr, Expr, IntoLikeExpr},
    ColumnTrait, Condition, EntityTrait, PaginatorTrait, QueryOrder, Select,
};
use sea_orm::{FromQueryResult, QueryFilter, QueryTrait, SelectModel, Selector};
use serde::Serialize;

use crate::error::{EntityError, EntityResult};
use crate::pagination::PaginationParams;

#[allow(async_fn_in_trait)]
pub trait Countable {
    type Entity: EntityTrait;

    async fn count() -> EntityResult<u64>
    where
        <Self::Entity as sea_orm::EntityTrait>::Model: Sync,
    {
        let db = niceerr_db::DBConfig::get_connection();
        let select = <Self::Entity as EntityTrait>::find();
        let count = <Select<Self::Entity> as PaginatorTrait<_>>::count(select, db).await?;
        Ok(count)
    }
}

#[allow(async_fn_in_trait)]
pub trait SelectIntoPaginated {
    type Entity: EntityTrait;
    type Model: Serialize;

    async fn into_paginated(
        self,
        params: &PaginationParams,
    ) -> EntityResult<(Vec<Self::Model>, u64, u64)>;
}

pub async fn selector_into_paginated<M: FromQueryResult + Send + Sync>(
    selector: Selector<SelectModel<M>>,
    params: &PaginationParams,
) -> EntityResult<(Vec<M>, u64, u64)> {
    if params.page_size == 0 || params.page_size > 1000 {
        info!("pagination params are invalid: {params:?}");
        return Err(EntityError::InvalidData);
    }
    let db = niceerr_db::DBConfig::get_connection();
    let paginator = selector.paginate(db, params.page_size);
    let num_items_pages = paginator.num_items_and_pages();
    let items_fut = paginator.fetch_page(params.page);
    let (num_items_pages, items) = try_join(num_items_pages, items_fut).await?;
    let (num_items, pages) = (
        num_items_pages.number_of_items,
        num_items_pages.number_of_pages,
    );
    Ok((items, num_items, pages))
}

impl<T> SelectIntoPaginated for Select<T>
where
    T: EntityTrait,
    T::Model: Serialize + Sync,
{
    type Entity = T;
    type Model = T::Model;

    /// Converts a select statement into a paginated result.
    ///
    /// The result is a tuple of 3 items:
    /// - A vec, containing the items
    /// - The count of items in this query
    /// - The number of available pages
    async fn into_paginated(
        self,
        params: &PaginationParams,
    ) -> EntityResult<(Vec<Self::Model>, u64, u64)> {
        if params.page_size == 0 || params.page_size > 1000 {
            info!("pagination params are invalid: {params:?}");
            return Err(EntityError::InvalidData);
        }
        let db = niceerr_db::DBConfig::get_connection();
        let paginator = <Select<T> as PaginatorTrait<_>>::paginate(self, db, params.page_size);
        let num_items_pages = paginator.num_items_and_pages();
        let items_fut = paginator.fetch_page(params.page);
        let (num_items_pages, items) = try_join(num_items_pages, items_fut).await?;
        let (num_items, pages) = (
            num_items_pages.number_of_items,
            num_items_pages.number_of_pages,
        );
        Ok((items, num_items, pages))
    }
}

#[derive(Debug, Clone, Copy)]
pub enum FilterOperations {
    ILike,
    Eq,
}

/// Trait that allows other models to easily implement filtering and sorting.
///
/// The purpose of this is to allow users to quickly find data, by potentially
/// matching the same value against multiple columns.
pub trait SortFilterParams
where
    Self: Sized,
{
    type Entity: EntityTrait;
    type Column: ColumnTrait;

    fn sortable_columns() -> &'static [Self::Column];

    /// Set of filterable columns. These columns must have clearly defined search operators.
    fn filterable_columns() -> &'static [Self::Column];

    /// Get the value that we'll use to search in multiple columns.
    fn get_search_value(self) -> Vec<impl IntoLikeExpr + Clone>;

    /// Use this column to sort the results.
    fn get_sort_value(&self) -> Option<&String>;

    /// Default sorting column.
    fn default_sort_column() -> Self::Column;

    /// Whether this result should be sorted in descending order.
    fn sort_by_desc(&self) -> bool;
}

pub trait SortableFilterable: SortFilterParams {
    // Try to match an user-provided sorting column against the list of sortable columns.
    // If a match is found, return the corresponding column.
    fn match_sort_column(&self) -> Option<Self::Column> {
        let sort_column = self.get_sort_value()?;
        for column in Self::sortable_columns() {
            let col_name = format!("{column:?}").to_lowercase();
            if sort_column.to_lowercase() == col_name {
                return Some(*column);
            }
        }
        warn!("sort column '{sort_column}' is not valid");
        None
    }

    /// Get the ready-to-use query with all the filters and sorting column applied.
    fn query(self) -> Select<Self::Entity> {
        let mut query = <Self::Entity as EntityTrait>::find();
        query = self.apply_sort_and_filters(query);
        // Don't build this query if we're not running in debug mode
        if log::max_level() == Level::Debug {
            debug!(
                "Database query: {}",
                query.build(sea_orm::DatabaseBackend::Postgres)
            );
        }
        query
    }

    fn apply_sort_and_filters(self, mut select: Select<Self::Entity>) -> Select<Self::Entity> {
        let sort_column = self
            .match_sort_column()
            .unwrap_or_else(Self::default_sort_column);
        select = match self.sort_by_desc() {
            true => select.order_by_desc(sort_column),
            false => select.order_by_asc(sort_column),
        };
        select = self.apply_filters(select);
        select
    }

    fn apply_filters(self, mut select: Select<Self::Entity>) -> Select<Self::Entity> {
        let search_values = self.get_search_value();
        if search_values.is_empty() {
            return select;
        }
        let mut master_condition = Condition::all();
        for rhs in search_values {
            let mut condition = Condition::any();
            for column in Self::filterable_columns() {
                // This forces the ORM to use the full "table"."column" to disambiguate
                // the column name when using JOIN queries.
                let lhs = Expr::col(column.as_column_ref()).cast_as(Alias::new("text"));
                condition = condition.add(lhs.ilike(rhs.clone()));
            }
            if !condition.is_empty() {
                master_condition = master_condition.add(condition);
            }
        }
        if !master_condition.is_empty() {
            select = select.filter(master_condition);
        }

        select
    }
}

impl<T> SortableFilterable for T where T: SortFilterParams {}
