use std::collections::HashSet;
use std::ops::{Deref, DerefMut};

use sea_orm::DeriveEntityModel;
use sea_orm::{entity::prelude::*, TryGetable};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

/// Permission scopes:
///
/// - Caller: grant this permission only to objects or items available to the caller
/// - All: grant this permission to all objects available to the caller
#[derive(
    EnumIter,
    DeriveActiveEnum,
    Eq,
    Hash,
    PartialEq,
    Deserialize,
    Serialize,
    Debug,
    Clone,
    DeriveDisplay,
)]
#[sea_orm(rs_type = "String", db_type = "String(StringLen::None)")]
#[serde(rename_all = "camelCase")]
pub enum PermissionScope {
    #[sea_orm(string_value = "CALLER")]
    Caller,
    #[sea_orm(string_value = "ALL")]
    All,
}

#[derive(
    EnumIter,
    DeriveActiveEnum,
    Eq,
    PartialEq,
    Deserialize,
    Serialize,
    Hash,
    Debug,
    Clone,
    DeriveDisplay,
    PartialOrd,
    Ord,
    Copy,
)]
#[sea_orm(rs_type = "String", db_type = "String(StringLen::None)")]
#[serde(rename_all = "camelCase")]
pub enum Permission {
    // User-related permissions
    #[sea_orm(string_value = "READ_USER")]
    ReadUser,
    // Basic info: username and password
    #[sea_orm(string_value = "UPDATE_USER_BASIC_INFO")]
    UpdateUserBasicInfo,
    /// Change properties such as user level, enabled status, stream/server side downloads, download
    /// persistence, quotas, download path, etc.
    #[sea_orm(string_value = "UPDATE_USER_EXTENDED_INFO")]
    UpdateUserExtendedInfo,
    #[sea_orm(string_value = "READ_ONGOING_DOWNLOAD")]
    ReadOngoingDownload,
    #[sea_orm(string_value = "STOP_ONGOING_DOWNLOAD")]
    StopOngoingDownload,
    #[sea_orm(string_value = "READ_DOWNLOAD_SESSION")]
    ReadDownloadSession,
    #[sea_orm(string_value = "PERFORM_SERVER_SIDE_DOWNLOAD")]
    PerformServerSideDownload,
    #[sea_orm(string_value = "PERFORM_STREAM_DOWNLOAD")]
    PerformStreamDownload,
    #[sea_orm(string_value = "PRUNE_DOWNLOAD_SESSION")]
    PruneDownloadSession,
    #[sea_orm(string_value = "DOWNLOAD_SERVER_SIDE_DOWNLOADED_CONTENT")]
    DownloadServerSideDownloadedContent,
    #[sea_orm(string_value = "DELETE_USER")]
    DeleteUser,
    // Arl permissions
    #[sea_orm(string_value = "CREATE_ARL")]
    CreateArl,
    #[sea_orm(string_value = "READ_ARL")]
    ReadArl,
    #[sea_orm(string_value = "UPDATE_ARL")]
    UpdateArl,
    #[sea_orm(string_value = "DELETE_ARL")]
    DeleteArl,
    #[sea_orm(string_value = "TRIGGER_ARL_REFRESH")]
    TriggerArlRefresh,
    #[sea_orm(string_value = "READ_SERVER_STATS")]
    ReadServerStats,
    #[sea_orm(string_value = "ADMIN_CONSOLE_ACCESS")]
    AdminConsoleAccess,
    #[sea_orm(string_value = "RELAY_ACCESS")]
    RelayAccess,
}

#[derive(Debug, Clone, Serialize, Deserialize, Hash, DeriveEntityModel, PartialEq, Eq)]
#[sea_orm(table_name = "assigned_permission")]
#[serde(rename_all = "camelCase")]
pub struct Model {
    // Don't allow the user to change ID
    #[sea_orm(primary_key)]
    #[serde(skip)]
    pub user_id: Uuid,
    #[sea_orm(primary_key)]
    pub name: Permission,
    pub scope: PermissionScope,
}

/// Newtype wrapper because we can't implement TryGetable on types
/// such as Vec<Model>.
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, Default)]
#[serde(rename_all = "camelCase")]
pub struct VecPermissionsWrapper(pub HashSet<Model>);

impl Deref for VecPermissionsWrapper {
    type Target = HashSet<Model>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for VecPermissionsWrapper {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

/// Dummy impls because we can't use the proc macro FromQueryResult
/// for some weird reason. We still want to return an empty vec anyway
impl TryGetable for VecPermissionsWrapper {
    fn try_get(_res: &QueryResult, _pre: &str, _col: &str) -> Result<Self, sea_orm::TryGetError> {
        Ok(VecPermissionsWrapper(HashSet::new()))
    }

    fn try_get_by<I: sea_orm::ColIdx>(
        _res: &QueryResult,
        _index: I,
    ) -> Result<Self, sea_orm::TryGetError> {
        Ok(VecPermissionsWrapper(HashSet::new()))
    }

    fn try_get_by_index(_res: &QueryResult, _index: usize) -> Result<Self, sea_orm::TryGetError> {
        Ok(VecPermissionsWrapper(HashSet::new()))
    }
}

impl ActiveModelBehavior for ActiveModel {}

#[derive(Copy, Clone, Debug, EnumIter)]
pub enum Relation {
    User,
}

impl RelationTrait for Relation {
    fn def(&self) -> RelationDef {
        match self {
            Self::User => Entity::belongs_to(super::user::Entity)
                .from(Column::UserId)
                .to(super::user::Column::Id)
                .into(),
        }
    }
}

impl Related<super::user::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::User.def()
    }
}
