use std::hash::Hash;

use chrono::{DateTime, Utc};
use sea_orm::DeriveEntityModel;
use sea_orm::{entity::prelude::*, FromQueryResult};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::assigned_permission::VecPermissionsWrapper;
use crate::ty::{FieldUpdate, NonBlankString, NonEmptyString, PathSanitizedString};

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct SearchFilterUser {
    #[serde(default)]
    pub search: Vec<String>,
    pub sort_by: Option<String>,
    pub desc: Option<bool>,
}

#[derive(Debug, Clone, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct UpdatableUser {
    pub username: Option<NonBlankString>,
    pub password: Option<NonEmptyString>,
    pub level: Option<u32>,
    pub superuser: Option<bool>,
    pub enabled: Option<bool>,
    #[serde(default)]
    pub download_path: FieldUpdate<PathSanitizedString>,
    pub max_bandwidth_bytes_per_day: Option<u64>,
    pub download_session_persist_minutes: Option<u64>,
    pub permissions: Option<VecPermissionsWrapper>,
}

#[derive(Debug, Clone, Serialize, Deserialize, DeriveEntityModel, Eq)]
#[sea_orm(table_name = "user")]
#[serde(rename_all = "camelCase")]
pub struct Model {
    // Don't allow the user to change ID
    #[sea_orm(primary_key)]
    #[serde(skip_deserializing, default = "Uuid::new_v4")]
    pub id: Uuid,
    pub username: NonBlankString,
    // Don't send back this to the user
    #[serde(skip_serializing)]
    pub password: NonEmptyString,
    #[serde(skip_deserializing, default = "Utc::now")]
    pub created_at: DateTime<Utc>,
    #[serde(skip_deserializing, default)]
    pub level: i32,
    #[serde(default = "default_superuser")]
    pub superuser: bool,
    #[serde(default = "default_enabled")]
    pub enabled: bool,
    #[serde(default)]
    pub download_path: Option<PathSanitizedString>,
    #[serde(default)]
    pub max_bandwidth_bytes_per_day: i64,
    #[serde(default)]
    pub download_session_persist_minutes: i64,
    #[sea_orm(ignore)]
    #[serde(default)]
    pub permissions: VecPermissionsWrapper,
}

#[derive(Debug, Clone, Serialize, Deserialize, FromQueryResult)]
#[serde(rename_all = "camelCase")]
pub struct UserWExtendedInfo {
    pub id: Uuid,
    pub username: String,
    pub created_at: DateTime<Utc>,
    #[serde(skip_deserializing, default)]
    pub level: i32,
    pub superuser: bool,
    pub enabled: bool,
    #[serde(default)]
    pub download_path: Option<String>,
    #[serde(default)]
    pub max_bandwidth_bytes_per_day: i64,
    #[serde(default)]
    pub download_session_persist_minutes: i64,
    pub downloaded_bytes: i64,
    #[sea_orm(ignore)]
    pub permissions: VecPermissionsWrapper,
}

impl PartialEq for Model {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl Hash for Model {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}

fn default_superuser() -> bool {
    false
}

fn default_enabled() -> bool {
    true
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(has_many = "super::arl::Entity")]
    Arl,
    #[sea_orm(has_many = "super::assigned_permission::Entity")]
    AssignedPermission,
}

// `Related` trait has to be implemented by hand
impl Related<super::arl::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::Arl.def()
    }
}

impl Related<super::assigned_permission::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::AssignedPermission.def()
    }
}

impl ActiveModelBehavior for ActiveModel {}
