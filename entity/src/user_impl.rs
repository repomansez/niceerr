use std::{sync::LazyLock, time::Duration};

use chrono::{Datelike, Local, TimeZone, Utc};
use futures::{Stream, TryStreamExt};
use log::{info, warn};
use niceerr_config::Config;
use niceerr_db::DBConfig;
use sea_orm::{
    sea_query::Expr, ActiveModelTrait, ActiveValue::NotSet, ColumnTrait, Condition, EntityTrait,
    FromQueryResult, IntoActiveModel, ModelTrait, QueryFilter, QuerySelect, QueryTrait, Select,
    SelectColumns, Set,
};
use tokio::task::spawn_blocking;
use uuid::Uuid;

use crate::{
    assigned_permission::{self, Permission, PermissionScope, VecPermissionsWrapper},
    download_session::{
        Column as DownloadSessionColumn, Entity as DownloadSessionEntity, Model as DownloadSession,
        OutcomeKind,
    },
    error::{EntityError, EntityResult},
    pagination::PaginationParams,
    song::{Column as SongColumn, Entity as SongEntity},
    traits::{selector_into_paginated, Countable, SortFilterParams, SortableFilterable},
    ty::NonEmptyString,
    user::{
        self, Column as UserColumn, Entity as UserEntity, Model as User, SearchFilterUser,
        UpdatableUser, UserWExtendedInfo,
    },
};
use argon2::{
    password_hash::{rand_core::OsRng, SaltString},
    Argon2, PasswordHash, PasswordHasher, PasswordVerifier,
};

static ARGON: LazyLock<Argon2<'static>> = LazyLock::new(Argon2::default);
pub type UserWithPermission = (user::Model, Vec<assigned_permission::Model>);

impl User {
    /// Verify whether this user can be created with the passed data. This usually checks
    /// the caller's permissions.
    pub fn create_verify_data(&self, caller: &User) -> EntityResult<()> {
        if caller.is_god() || caller.superuser {
            return Ok(());
        }

        if self.superuser {
            info!("non-superuser attempted to create a superuser");
            return Err(EntityError::InsufficientPermissions);
        }

        if !self.permissions.is_empty() {
            info!("attempted to create a user with modified permissions");
            return Err(EntityError::InsufficientPermissions);
        }

        if !caller.has_permission(&Permission::UpdateUserExtendedInfo)
            && (!self.enabled
                || self.download_path.is_some()
                || self.max_bandwidth_bytes_per_day != 0
                || self.download_session_persist_minutes != 0)
        {
            info!("attempted to create an user with modified extended attributes");
            return Err(EntityError::InsufficientPermissions);
        }

        Ok(())
    }

    // Create user instance.
    pub async fn create(mut self, caller: Option<&User>) -> EntityResult<Self> {
        if let Some(caller) = caller {
            self.create_verify_data(caller)?;
        }

        self.encode_password().await?;
        let db = niceerr_db::DBConfig::get_connection();
        let permissions = std::mem::take(&mut self.permissions.0);
        let model = self.into_active_model().insert(db).await?;

        if !permissions.is_empty() {
            let permissions = permissions
                .into_iter()
                .map(|it| it.into_active_model())
                .collect();
            return model.set_permissions(false, permissions).await;
        }
        info!("user: created user with ID '{}'", model.id);
        Ok(model)
    }

    async fn set_permissions(
        &self,
        delete_all_first: bool,
        mut perms: Vec<assigned_permission::ActiveModel>,
    ) -> EntityResult<Self> {
        if delete_all_first {
            let deleted = assigned_permission::Model::delete_all_permissions_for(self).await?;
            info!("deleted all permissions ({deleted}) for user {}", self.id);
        }
        for perm in &mut perms {
            perm.user_id = Set(self.id);
        }
        assigned_permission::Model::assign_multiple_perms(perms).await?;
        Ok(Self::find_by_id(self.id)
            .await?
            .expect("Can't find user after giving it permissions"))
    }

    async fn encode_string_to_password(password: &str) -> EntityResult<NonEmptyString> {
        let argon = &ARGON;
        let cloned = password.to_owned();
        let hashed = spawn_blocking(move || {
            let salt = SaltString::generate(&mut OsRng);
            let result = argon
                .hash_password(cloned.as_bytes(), &salt)
                .map_err(|e| EntityError::Unhandled(e.to_string()))?
                .to_string();
            Ok::<_, EntityError>(result)
        })
        .await??;
        Ok(NonEmptyString::new_unchecked(hashed))
    }

    // Encode this user's password with argon2
    // TODO: Print error backtrace with a macro
    pub async fn encode_password(&mut self) -> EntityResult<()> {
        self.password = Self::encode_string_to_password(&self.password).await?;
        Ok(())
    }

    pub async fn verify_password(&self, password: &str) -> EntityResult<()> {
        let argon = &ARGON;
        let real_password = self.password.to_owned();
        let user_password = password.to_owned();
        let span = tracing::Span::current();
        spawn_blocking(move || {
            let _guard = span.enter();
            let parsed_hash = PasswordHash::new(&real_password).map_err(|e| {
                warn!("auth: cannot reconstruct hash from db: {e:?}");
                EntityError::PasswordMismatch
            })?;
            argon
                .verify_password(user_password.as_bytes(), &parsed_hash)
                .map_err(|e| {
                    info!("auth: cannot verify password: {}", e);
                    EntityError::PasswordMismatch
                })
        })
        .await??;
        Ok(())
    }

    pub async fn find_by_username(username: &str) -> EntityResult<Option<Self>> {
        let db = niceerr_db::DBConfig::get_connection();
        Ok(UserEntity::find()
            .filter(Condition::all().add(UserColumn::Username.eq(username)))
            .find_with_related(assigned_permission::Entity)
            .all(db)
            .await?
            .into_iter()
            .nth(0)
            .map(|(mut user, perms)| {
                user.permissions = VecPermissionsWrapper(perms.into_iter().collect());
                user
            }))
    }

    // Delete proxy method
    pub async fn delete_p(self) -> EntityResult<()> {
        let db = niceerr_db::DBConfig::get_connection();
        info!("user: deleting user: '{}' ({})", self.id, self.username);
        self.delete(db).await?;
        Ok(())
    }

    /// Find users within the scope of the caller.
    pub async fn find_scoped(&self, id: Uuid) -> EntityResult<Option<Self>> {
        let db = niceerr_db::DBConfig::get_connection();
        let select = self.visible_users_select(&Permission::ReadUser)?;
        Ok(select.filter(UserColumn::Id.eq(id)).one(db).await?)
    }

    pub async fn find_by_id(id: Uuid) -> EntityResult<Option<user::Model>> {
        let db = niceerr_db::DBConfig::get_connection();
        Ok(UserEntity::find_by_id(id)
            .find_with_related(assigned_permission::Entity)
            .all(db)
            .await?
            .into_iter()
            .nth(0)
            .map(|(mut user, perms)| {
                user.permissions = VecPermissionsWrapper(perms.into_iter().collect());
                user
            }))
    }

    /// The users this user can see. Depending on the passed permission and scope,
    /// the caller will be either able to see only one user (scope: self) or all users
    /// with a "lower" level than theirs.
    pub fn visible_users_select(&self, perm: &Permission) -> EntityResult<Select<UserEntity>> {
        let mut select = UserEntity::find();
        if !self.is_god() {
            // Make sure the caller ALWAYS has this permission. If they don't have it, but we reached this
            // place, it means somewhere up the call stack someone forgot
            if !self.has_permission(perm) {
                info!("missing required perm (read visible user data): {perm:?}");
                return Err(EntityError::InsufficientPermissions);
            }
            let mut condition = Condition::any();
            condition = condition.add(UserColumn::Id.eq(self.id));
            if self.has_permission_scoped(perm, &PermissionScope::All) {
                condition = condition.add(UserColumn::Level.gt(self.level));
            }
            select = select.filter(condition);
        }
        Ok(select)
    }

    pub async fn list_paginated(
        caller: &User,
        params: &PaginationParams,
        filter: SearchFilterUser,
    ) -> EntityResult<(Vec<UserWExtendedInfo>, u64, u64)> {
        let mut select = caller.visible_users_select(&Permission::ReadUser)?;
        select = filter.apply_sort_and_filters(select);
        select = select
            // This will make postgres return an empty
            .column_as(
                Expr::expr(Expr::col(SongColumn::DownloadedBytes.as_column_ref()).sum()).if_null(0),
                "downloaded_bytes",
            )
            // Do a left join on all the download sessions that belong to each specific user.
            .join_rev(
                sea_orm::JoinType::LeftJoin,
                DownloadSessionEntity::belongs_to(UserEntity)
                    .from(DownloadSessionColumn::DownloadedBy)
                    .to(UserColumn::Id)
                    .into(),
            )
            // Once we have all the download sessions for this user, get all the songs they've downloaded
            // (we'll use this to calculate the total downloaded bytes)
            .join_rev(
                sea_orm::JoinType::LeftJoin,
                SongEntity::belongs_to(DownloadSessionEntity)
                    .from(SongColumn::SessionId)
                    .to(DownloadSessionColumn::Id)
                    .into(),
            )
            .group_by(UserColumn::Id);
        let selector = select.into_model::<UserWExtendedInfo>();
        let mut paginated = selector_into_paginated(selector, params).await?;

        // Perform another database trip to fetch the permissions for each user, that we've just paginated and their
        // permissions into the returned results.
        let user_ids = paginated.0.iter().map(|user| user.id).collect::<Vec<_>>();
        let mut assigned_permissions = assigned_permission::Entity::find()
            .filter(assigned_permission::Column::UserId.is_in(user_ids))
            .all(DBConfig::get_connection())
            .await?;

        for user in &mut paginated.0 {
            let mut permission_indexes = vec![];
            for (idx, model) in assigned_permissions.iter().enumerate() {
                if model.user_id == user.id {
                    permission_indexes.push(idx);
                }
            }
            // Start from end to start to prevent shifting indexes
            permission_indexes.sort_unstable_by(|a, b| b.cmp(a));
            for index in permission_indexes {
                user.permissions
                    .0
                    .insert(assigned_permissions.swap_remove(index));
            }
        }
        Ok(paginated)
    }

    pub fn find_permission(&self, perm: &Permission) -> Option<&assigned_permission::Model> {
        self.permissions.0.iter().find(|p| p.name == *perm)
    }

    pub fn has_permission_scoped(&self, perm: &Permission, scope: &PermissionScope) -> bool {
        self.superuser
            || self
                .find_permission(perm)
                .is_some_and(|p| &p.scope == scope)
    }

    pub fn has_permission(&self, perm: &Permission) -> bool {
        self.superuser || self.find_permission(perm).is_some()
    }

    pub fn verify_has_permission(&self, perm: &Permission) -> EntityResult<()> {
        if !self.has_permission(perm) {
            info!("user lacks permission {perm:?}");
            return Err(EntityError::InsufficientPermissions);
        }
        Ok(())
    }

    pub fn has_authority_over(&self, other: &User, perm: &Permission) -> bool {
        // Super-superuser is always right, and other superusers can edit any users below their own level/hierarchy
        if self.is_god() {
            return true;
        }
        // No one, not even superusers, can edit higher-ranked superusers or users
        if self.id != other.id && self.level >= other.level {
            info!(
                "Caller tried to operate on user with same or higher level {} ('{}')",
                other.id, other.username
            );
            return false;
        }

        // This superuser has a higher level over whatever other user, so this is right.
        if self.superuser {
            return true;
        }
        match self.find_permission(perm) {
            Some(caller_perm)
                if self.id == other.id || caller_perm.scope == PermissionScope::All =>
            {
                true
            }
            Some(_) => {
                info!("Caller has insufficient scope to operate on another user ({perm:?})");
                false
            }
            None => {
                info!("Caller is missing perm {perm:?}");
                false
            }
        }
    }

    pub fn verify_has_authority_over(&self, other: &User, perm: &Permission) -> EntityResult<()> {
        if !self.has_authority_over(other, perm) {
            return Err(EntityError::InsufficientPermissions);
        }
        Ok(())
    }

    /// Whether this user is a "god user"
    pub fn is_god(&self) -> bool {
        self.level == 0
    }

    pub async fn update(mut self, caller: &User, mut other: UpdatableUser) -> EntityResult<Self> {
        if !caller.is_god() {
            // Run checks on all fields to determine whether the caller can update the field
            let can_change_basic =
                caller.has_authority_over(&self, &Permission::UpdateUserBasicInfo);
            let can_change_ext =
                caller.has_authority_over(&self, &Permission::UpdateUserExtendedInfo);
            other.username.verify(can_change_basic)?;
            other.password.verify(can_change_basic)?;
            other.level.verify(caller.superuser)?;
            other.superuser.verify(caller.superuser)?;
            other.permissions.verify(caller.superuser)?;
            other.enabled.verify(can_change_ext)?;
            if other.download_path.is_change() && !can_change_ext {
                return Err(EntityError::InsufficientPermissions);
            }
            other.max_bandwidth_bytes_per_day.verify(can_change_ext)?;
            other
                .download_session_persist_minutes
                .verify(can_change_ext)?;
        }

        if other
            .level
            .as_ref()
            .is_some_and(|level| *level <= caller.level as u32)
        {
            warn!(
                "Prevented privilege escalation (level {} -> {})",
                self.level,
                other.level.unwrap()
            );
            return Err(EntityError::InvalidData);
        }

        // Ensure permissions aren't being assigned to an existing superuser, unless this
        // update request also demotes this superuser
        if self.superuser
            && other.superuser.unwrap_or(true)
            && other.permissions.as_ref().is_some_and(|p| !p.is_empty())
        {
            info!("Can't assign permissions to superuser");
            return Err(EntityError::InvalidData);
        }

        let mut permissions = std::mem::take(&mut self.permissions);

        // User has been promoted to superuser. Superusers must not have any permissions
        // assigned to them.
        if !self.superuser && other.superuser.unwrap_or(false) {
            other.permissions = None;
            info!(
                "User {} promoted to superuser. Permissions will be removed",
                self.id
            );
            permissions.clear();
            assigned_permission::Model::delete_all_permissions_for(&self).await?;
        }

        // Change this user's permissions
        if let Some(other_perms) = other.permissions {
            permissions = other_perms.clone();
            let deleted = assigned_permission::Model::delete_all_permissions_for(&self).await?;
            info!(
                "changing permissions for user {}: deleted {}",
                self.id, deleted
            );
            // Actually convert it to db model
            let perms = other_perms
                .0
                .into_iter()
                .map(|mut it| {
                    it.user_id = self.id;
                    it.into_active_model()
                })
                .collect();
            assigned_permission::Model::assign_multiple_perms(perms).await?;
        }

        let mut active_model = self.into_active_model();
        active_model.username = other.username.map_or(NotSet, Set);
        active_model.password = match other.password {
            Some(password) => Set(Self::encode_string_to_password(&password).await?),
            None => NotSet,
        };

        active_model.download_path = match other.download_path {
            crate::ty::FieldUpdate::Unchanged => NotSet,
            crate::ty::FieldUpdate::Set(value) => Set(Some(value)),
            crate::ty::FieldUpdate::Unset => Set(None),
        };
        active_model.superuser = other.superuser.map_or(NotSet, Set);
        active_model.level = other.level.map_or(NotSet, |v| Set(v as i32));
        active_model.enabled = other.enabled.map_or(NotSet, Set);
        active_model.max_bandwidth_bytes_per_day = other
            .max_bandwidth_bytes_per_day
            .map_or(NotSet, |v| Set(v as i64));
        active_model.download_session_persist_minutes = other
            .download_session_persist_minutes
            .map_or(NotSet, |v| Set(v as i64));
        let db = niceerr_db::DBConfig::get_connection();
        let mut model = active_model.update(db).await?;

        model.permissions = permissions;
        Ok(model)
    }

    /// The number of bytes this user has downloaded today.
    pub async fn bytes_downloaded(&self) -> EntityResult<usize> {
        let tz = Config::get().timezone;
        let now = Local::now();
        let local_start = tz
            .with_ymd_and_hms(now.year(), now.month(), now.day(), 0, 0, 0)
            .unwrap();
        let local_end = tz
            .with_ymd_and_hms(now.year(), now.month(), now.day(), 23, 59, 59)
            .unwrap();
        let start = local_start.with_timezone(&Utc);
        let end = local_end.with_timezone(&Utc);

        #[derive(FromQueryResult, Debug)]
        struct SumResult {
            sum: i64,
        }
        let db = niceerr_db::DBConfig::get_connection();
        let download_sessions = DownloadSessionEntity::find()
            .filter(DownloadSessionColumn::DownloadedBy.eq(self.id))
            .filter(DownloadSessionColumn::CreatedAt.gte(start))
            .filter(DownloadSessionColumn::CreatedAt.lte(end))
            .select_only()
            .column(DownloadSessionColumn::Id)
            .into_query();
        let downloaded_bytes = SongEntity::find()
            .filter(SongColumn::SessionId.in_subquery(download_sessions))
            .select_only()
            .select_column_as(SongColumn::DownloadedBytes.sum(), "sum")
            .into_model::<SumResult>()
            .one(db)
            .await
            .unwrap_or(Some(SumResult { sum: 0 }))
            .map_or(0, |bytes| bytes.sum);
        Ok(downloaded_bytes as usize)
    }

    /// Whether this user would be over its daily max byte download quota
    /// by downloading `other` bytes.
    pub async fn would_exceed_download_quota(&self, other: usize) -> EntityResult<bool> {
        // 0 means no quota
        if self.max_bandwidth_bytes_per_day == 0 {
            return Ok(false);
        }
        let bytes_downloaded = self.bytes_downloaded().await?;
        if bytes_downloaded + other > self.max_bandwidth_bytes_per_day as usize {
            return Ok(true);
        }
        Ok(false)
    }

    // Get users with downloads that can be potentially pruned.
    pub async fn with_pruneable_dl_sessions() -> EntityResult<impl Stream<Item = EntityResult<User>>>
    {
        let db = niceerr_db::DBConfig::get_connection();
        let users = UserEntity::find()
            .filter(UserColumn::DownloadSessionPersistMinutes.gt(0))
            .stream(db)
            .await?
            .map_err(EntityError::from);
        Ok(users)
    }

    /// Get this user's pruneable download sessions.
    /// Arguments:
    ///
    /// - `id`: The ID of the session to get
    /// - `force_prune`: Whether to include pruned sessions
    /// - `all_sessions`: Whether to include all sessions, or only sessions that are
    ///    supposed to be pruned
    pub async fn pruneable_download_sessions(
        &self,
        id: Option<Uuid>,
        force_prune: bool,
        all_sessions: bool,
    ) -> EntityResult<impl Stream<Item = EntityResult<DownloadSession>>> {
        let db = niceerr_db::DBConfig::get_connection();
        let secs = self.download_session_persist_minutes as u64 * 60;
        let mut sessions = DownloadSessionEntity::find()
            .filter(DownloadSessionColumn::DownloadedBy.eq(self.id))
            .filter(DownloadSessionColumn::Outcome.ne(OutcomeKind::InProgress))
            .filter(DownloadSessionColumn::IsStream.eq(false));

        if let Some(id) = id {
            sessions = sessions.filter(DownloadSessionColumn::Id.eq(id));
        }

        if !force_prune {
            sessions = sessions.filter(DownloadSessionColumn::IsPruned.eq(false));
        }
        if !all_sessions {
            sessions = sessions.filter(
                DownloadSessionColumn::CreatedAt.lt(Utc::now() - Duration::from_secs(secs)),
            );
        }

        Ok(sessions.stream(db).await?.map_err(EntityError::from))
    }
}

impl Countable for User {
    type Entity = UserEntity;
}

impl SortFilterParams for SearchFilterUser {
    type Entity = UserEntity;
    type Column = UserColumn;

    fn sortable_columns() -> &'static [Self::Column] {
        &[
            UserColumn::Id,
            UserColumn::Username,
            UserColumn::CreatedAt,
            UserColumn::DownloadPath,
            UserColumn::Level,
            UserColumn::Enabled,
            UserColumn::Superuser,
            UserColumn::DownloadSessionPersistMinutes,
            UserColumn::MaxBandwidthBytesPerDay,
        ]
    }

    fn filterable_columns() -> &'static [Self::Column] {
        &[
            UserColumn::Id,
            UserColumn::Username,
            UserColumn::CreatedAt,
            UserColumn::DownloadPath,
        ]
    }

    fn get_search_value(self) -> Vec<impl sea_orm::sea_query::IntoLikeExpr + Clone> {
        self.search
    }

    fn get_sort_value(&self) -> Option<&String> {
        self.sort_by.as_ref()
    }

    fn default_sort_column() -> Self::Column {
        UserColumn::CreatedAt
    }

    fn sort_by_desc(&self) -> bool {
        self.desc.unwrap_or(false)
    }
}

trait SomeToEntityError {
    fn verify(&self, cond: bool) -> EntityResult<()>;
}

impl<T> SomeToEntityError for Option<T> {
    fn verify(&self, cond: bool) -> EntityResult<()> {
        if !cond && self.is_some() {
            return Err(EntityError::InsufficientPermissions);
        }
        Ok(())
    }
}
