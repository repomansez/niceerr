use std::backtrace::Backtrace;

use log::{error, info};
use sea_orm::DbErr;
use thiserror::Error;
use tokio::task::JoinError;

#[derive(Error, Debug)]
pub enum EntityError {
    #[error("Invalid data")]
    InvalidData,
    #[error("Item not found")]
    NotFound,
    #[error("Unique key violation")]
    UniqueViolation,
    #[error("Unhandled error: {0}")]
    Unhandled(String),
    #[error("Password mismatch")]
    PasswordMismatch,
    #[error("Insufficient permissions")]
    InsufficientPermissions,
}

impl From<JoinError> for EntityError {
    fn from(e: JoinError) -> Self {
        // show error backtrace
        let backtrace = Backtrace::force_capture();
        error!("Fatal join error: {e}: {backtrace:?}");
        EntityError::Unhandled(e.to_string())
    }
}

impl From<DbErr> for EntityError {
    fn from(err: DbErr) -> Self {
        match err {
            DbErr::Query(sea_orm::RuntimeErr::SqlxError(err)) => {
                if err
                    .to_string()
                    .contains("duplicate key value violates unique constraint")
                {
                    info!("Unique key violation: {err:?}");
                    return EntityError::UniqueViolation;
                }
                let backtrace = Backtrace::force_capture();
                error!("Unhandled query error: {err}: {backtrace:#?}");
                EntityError::Unhandled(err.to_string())
            }
            err => {
                let backtrace = Backtrace::force_capture();
                error!("Unhandled database error: {err}: {backtrace:#?}");
                EntityError::Unhandled(err.to_string())
            }
        }
    }
}

pub type EntityResult<T> = Result<T, EntityError>;
