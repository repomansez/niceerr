use std::path::Path;

use sea_orm::{
    sea_query::{Expr, Func},
    ActiveModelTrait, ColumnTrait, EntityTrait, FromQueryResult, IntoActiveModel, QueryFilter,
    QuerySelect, QueryTrait, SelectColumns,
};
use serde::Serialize;
use uuid::Uuid;

use crate::{
    assigned_permission::Permission,
    download_session::{Column as DownloadSessionColumn, Entity as DownloadSessionEntity},
    error::EntityResult,
    pagination::PaginationParams,
    song::{Column as SongColumn, Entity as SongEntity, Model as Song, SearchFilterSong},
    traits::{SelectIntoPaginated, SortFilterParams, SortableFilterable},
    user::{Column as UserColumn, Model as User},
};

#[derive(FromQueryResult, Debug, Serialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct SongStats {
    pub downloaded_bytes: i64,
    pub total_bytes: i64,
    pub total_count: i64,
    pub error_count: i64,
    pub session_count: i64,
}

impl Song {
    pub fn new_unsaved(session_id: &Uuid, item_id: i64) -> Song {
        Self {
            id: Uuid::new_v4(),
            session_id: *session_id,
            item_id,
            total_bytes: 0,
            download_path: String::new(),
            downloaded_bytes: 0,
            error_message: None,
        }
    }

    pub fn is_completed(&self) -> bool {
        self.total_bytes > 0 && self.downloaded_bytes == self.total_bytes
    }

    pub async fn all_time_stats() -> EntityResult<SongStats> {
        let db = niceerr_db::DBConfig::get_connection();
        let with_error = Expr::expr(Func::char_length(Expr::col(SongColumn::ErrorMessage))).gt(0);
        let with_error_count = Expr::expr(with_error).count();

        let downloaded_bytes = Expr::expr(Expr::col(SongColumn::DownloadedBytes).sum()).if_null(0);
        let total_bytes = Expr::expr(Expr::col(SongColumn::TotalBytes).sum()).if_null(0);
        let stats = SongEntity::find()
            .select_only()
            .select_column_as(downloaded_bytes, "downloaded_bytes")
            .select_column_as(total_bytes, "total_bytes")
            .select_column_as(SongColumn::Id.count(), "total_count")
            .select_column_as(with_error_count, "error_count")
            .select_column_as(
                Expr::col(SongColumn::SessionId).count_distinct(),
                "session_count",
            )
            .into_model::<SongStats>()
            .one(db)
            .await?
            .unwrap_or_default();
        Ok(stats)
    }

    pub async fn list_paginated(
        caller: &User,
        params: &PaginationParams,
        filter: SearchFilterSong,
    ) -> EntityResult<(Vec<Self>, u64, u64)> {
        let mut query = filter.query();
        if !caller.is_god() {
            let visible_users = caller
                .visible_users_select(&Permission::ReadDownloadSession)?
                .select_only()
                .column(UserColumn::Id)
                .into_query();
            let visible_dl_sessions = DownloadSessionEntity::find()
                .filter(DownloadSessionColumn::DownloadedBy.in_subquery(visible_users))
                .select_only()
                .column(DownloadSessionColumn::Id)
                .into_query();
            query = query.filter(SongColumn::SessionId.in_subquery(visible_dl_sessions));
        }
        query.into_paginated(params).await
    }

    pub async fn save(self) -> EntityResult<Song> {
        let db = niceerr_db::DBConfig::get_connection();
        let saved = self.into_active_model().insert(db).await?;
        Ok(saved)
    }

    pub async fn prune(&self) -> std::io::Result<()> {
        let path = Path::new(&self.download_path);
        tokio::fs::remove_file(path).await
    }
}

impl SortFilterParams for SearchFilterSong {
    type Entity = SongEntity;
    type Column = SongColumn;

    fn sortable_columns() -> &'static [Self::Column] {
        &[
            SongColumn::DownloadPath,
            SongColumn::Id,
            SongColumn::TotalBytes,
            SongColumn::DownloadedBytes,
            SongColumn::ItemId,
            SongColumn::SessionId,
            SongColumn::ErrorMessage,
        ]
    }

    fn filterable_columns() -> &'static [Self::Column] {
        &[
            SongColumn::SessionId,
            SongColumn::Id,
            SongColumn::ItemId,
            SongColumn::DownloadPath,
            SongColumn::ErrorMessage,
        ]
    }

    fn get_search_value(self) -> Vec<impl sea_orm::sea_query::IntoLikeExpr + Clone> {
        self.search
    }

    fn get_sort_value(&self) -> Option<&String> {
        self.sort_by.as_ref()
    }

    fn default_sort_column() -> Self::Column {
        SongColumn::ErrorMessage
    }

    fn sort_by_desc(&self) -> bool {
        self.desc.unwrap_or(false)
    }
}
