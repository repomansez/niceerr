use log::{info, warn};
use niceerr_config::Config;
use sea_orm::{ConnectOptions, Database, DatabaseConnection};
use std::{sync::OnceLock, time::Duration};

pub static CONNECTION: OnceLock<DatabaseConnection> = OnceLock::new();

pub struct DBConfig;

impl DBConfig {
    pub async fn init_db_connection() -> anyhow::Result<()> {
        if CONNECTION.get().is_some() {
            warn!("init_db_connection() called twice!");
            return Ok(());
        }
        let config = Config::get();
        let mut opt = ConnectOptions::new(&config.database_url);
        // configure thread pool
        opt.max_connections(config.db_max_connections)
            .min_connections(config.db_min_connections)
            .idle_timeout(Duration::from_secs(config.db_idle_timeout_sec))
            .connect_timeout(Duration::from_secs(config.db_connect_timeout_sec))
            .acquire_timeout(Duration::from_secs(config.db_acquire_timeout_sec));

        info!("Starting up database pool...");
        let conn = Database::connect(opt).await?;
        CONNECTION
            .set(conn)
            .expect("Cannot set database connection");
        info!("Successfully initialized pool: {:?}", CONNECTION);
        Ok(())
    }

    /// Get a reference to the database connection.
    pub fn get_connection() -> &'static DatabaseConnection {
        CONNECTION
            .get()
            .expect("Database connection not initialized")
    }
}
