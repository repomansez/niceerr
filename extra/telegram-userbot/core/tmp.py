import io
import os
import zipfile
from typing import Optional
import aiofiles
import random
from core.const import TMP_STORAGE_PATH


class AsyncTempFile:

    def __init__(self):
        self.file = None
        self.filename = None
        self.filepath = None

    async def __aenter__(self):
        # Generate a random 32-bit integer filename
        self.filename = f"{random.randint(0, 0xFFFFFFFF):08x}.zip"
        if tmp_path := TMP_STORAGE_PATH:
            self.filename = f"{TMP_STORAGE_PATH}/{self.filename}"
        # Create the temporary file in the current working directory
        self.filepath = os.path.abspath(self.filename)

        # Open the file in read and write mode
        self.file = await aiofiles.open(self.filepath, mode="w+b")
        return self

    async def __aexit__(self, exc_type, exc_value, traceback):
        await self.file.close()
        os.remove(self.filepath)

    async def write(self, data: bytes):
        await self.file.write(data)

    async def close(self):
        await self.file.close()

    def path(self):
        return self.filepath

    def read_single_file_zip(self) -> Optional[io.BytesIO]:
        """
        Reads a zip file and checks if it contains exactly one file. If so,
        returns a BytesIO buffer with its contents.
        If the zip file contains more than one file, returns None.

        Returns:
        BytesIO: buffer with its contents or None if the zip file
        contains more than one file.
        """
        with zipfile.ZipFile(self.filepath, "r") as zip_ref:
            # Get the list of file names in the zip
            file_names = zip_ref.namelist()

            # Check if the zip contains exactly one file
            if len(file_names) != 1:
                return None

            # Extract the single file
            file_name = file_names[0]
            base_name = os.path.basename(file_name)

            with zip_ref.open(file_name) as file:
                file_content = file.read()

            # Create a BytesIO buffer with the file contents
            file_buffer = io.BytesIO(file_content)
            file_buffer.name = base_name

            return file_buffer
