"""
Constants used all throughout this bot.
"""

import os
from dotenv import load_dotenv


load_dotenv()

TRIGGER_KEYWORD: str = None
API_URL: str = None
TELEGRAM_API_ID: int = None
TELEGRAM_API_HASH: str = None
TELEGRAM_PHONE_NUMBER: str = None
SESSION_STORAGE_PATH: str = None
DATABASE_PATH: str = None
TMP_STORAGE_PATH: str = None
HELP_NICE_TEXT: str = None

try:
    API_URL = os.environ["API_URL"]
    if API_URL.endswith("/"):
        raise Exception(
            f"API_URL ('{API_URL}') must start with http(s):// and must not end with '/'."
        )

    TRIGGER_KEYWORD = os.environ.get("TRIGGER_KEYWORD", ":dzbot").strip()
    if len(TRIGGER_KEYWORD) == 0:
        raise Exception("TRIGGER_KEYWORD must not be empty")
    # Credential cache
    SESSION_STORAGE_PATH = os.environ.get("SESSION_STORAGE_PATH", "telegram").strip()
    # Database
    DATABASE_PATH = os.environ.get("DATABASE_PATH", "users.db")
    if not DATABASE_PATH.strip().endswith(".db"):
        raise Exception("DATABASE_PATH must end with .db")

    TELEGRAM_API_ID = int(os.environ["TELEGRAM_API_ID"])
    TELEGRAM_API_HASH = os.environ["TELEGRAM_API_HASH"].strip()
    TELEGRAM_PHONE_NUMBER = os.environ["TELEGRAM_PHONE_NUMBER"].strip()
    TMP_STORAGE_PATH = os.environ.get("TMP_STORAGE_PATH", None)
    if tmp_path := TMP_STORAGE_PATH:
        if tmp_path.endswith("/"):
            raise Exception("TMP_STORAGE_PATH must not end with '/'")

    HELP_NICE_TEXT = os.environ.get(
        "HELP_NICE_TEXT",
        "promise me that you will have courage to rebaptize your badness as the best in you",
    ).strip()

    print(f"[NOTICE] Telegram session storage: {SESSION_STORAGE_PATH}")
    print("[NOTICE] Loaded all env vars successfully.")
except Exception as e:
    print(f"Couldn't read env variable: {e}")
    exit(-1)

# Trigger keywords
TRIGGER_SELF_KEYWORD = f"{TRIGGER_KEYWORD}i"

# Commands
CMD_LOGIN = "login"
CMD_LOGOUT = "logout"
CMD_GET_STREAM = "get"
CMD_SERVER_ADD = "sadd"
CMD_HELP = "help"


HELP_TEXT = f"""Available commands:

- `{TRIGGER_KEYWORD} {CMD_LOGIN}` <`USER`> <`PASSWORD`>: Authenticate and save your credentials 
- `{TRIGGER_KEYWORD} {CMD_LOGOUT}`: Log out and remove your saved credentials
- `{TRIGGER_KEYWORD} {CMD_GET_STREAM}` <`URL`>: Download <`URL`> in a zip, directly using telegram
- `{TRIGGER_KEYWORD} {CMD_SERVER_ADD}` <`URL`>: Start server-side download
- `{TRIGGER_KEYWORD} {CMD_HELP}`: Show this help

{HELP_NICE_TEXT}
"""

INVALID_CMD_TXT = f"""Invalid command.

Use `{TRIGGER_KEYWORD} {CMD_HELP}` if you need help.
"""
