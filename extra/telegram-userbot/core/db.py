from peewee import SqliteDatabase, Model, CharField, IntegerField
import httpx
from core.const import API_URL, DATABASE_PATH

db = SqliteDatabase(DATABASE_PATH)


# This bot's user model.
class User(Model):
    username = CharField(unique=True)
    telegram_id = IntegerField(unique=True)
    password = CharField()

    class Meta:
        database = db

    async def authenticate(self) -> str:
        async with httpx.AsyncClient() as client:
            response = await client.post(
                API_URL + "/api/user/login",
                json={"username": self.username, "password": self.password},
            )
            json = response.json()
            if response.status_code == 200:
                return json["token"]
            else:
                raise Exception(f"Couldn't authenticate against API: {json}")


# Initialize sqlite db
db.connect()
db.create_tables([User])
