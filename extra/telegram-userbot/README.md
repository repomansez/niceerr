# Telegram userbot for niceerr

This folder contains a Telegram userbot that you can use to consume Niceerr using telegram.

# **IMPORTANT**

This service requires you to generate your telegram API keys. These API keys grant complete access to your Telegram
account, so please be careful when handling them.

## Features

- It's small and cool
- Lightning fast
- Leverages Niceerr's auth system
- Automatically cleans up temporary files with dark magic
- 1:1 Telegram user to Niceerr user assignments 
- You can customize the trigger keyword

## Pre-Setup

You'll need to generate your Telegram API key/hash. You can do so here: https://my.telegram.org/apps

**Make sure you save your keys in a safe place, and don't ever share them with anyone.**

## Configuration options

This is a quite simple bot, and it doesn't need anything (other than the API keys) to work.
However, you can customize it if you wish to do so.

|      Env variable       |       Type        | Required? | Details                                                                                                                                                              |
|:-----------------------:|:-----------------:|:---------:|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|        `API_URL`        | String/No default |  **Yes**  | Niceerr instance URL. Must not end with a trailing slash (`/`).                                                                                                      |
|    `TELEGRAM_API_ID`    |  int/No default   |  **Yes**  | Telegram API ID                                                                                                                                                      |
|   `TELEGRAM_API_HASH`   | String/No default |  **Yes**  | Telegram API Hash                                                                                                                                                    |
| `TELEGRAM_PHONE_NUMBER` | String/No default |  **Yes**  | Telegram Phone Number (Used for MFA authentication)                                                                                                                  |
|    `TRIGGER_KEYWORD`    |  String/`:dzbot`  |    No     | Keyword to listen to on all chats. **Make sure you use a special, uncommon word!**                                                                                   |
| `SESSION_STORAGE_PATH`  | String/No default |    No     | Telegram Session storage cache (defaults to `$PWD/telegram.session` if unset                                                                                         |
|   `TMP_STORAGE_PATH`    |    String/None    |    No     | Path used to store temporary downloads (these are immediately cleaned up). By default, uses the current working directory. Must not end with a trailing slash (`/`). |
|     `DATABASE_PATH`     | String/`user.db`  |    No     | SQLite database path (defaults to `$PWD/user.db` if unset)                                                                                                           |
|    `HELP_NICE_TEXT`     |      String       |    No     | Show this nice text in the help command.                                                                                                                             |


You can save all these variables in a `.env` file and the python script will automatically pick them up.

## Setup

This repository contains a simple python program: you can install its dependencies with pip and python.

```bash
cd <THIS FOLDER>
# Create virtual env
python -m venv .venv
# Activate virtual env
source .venv/bin/activate
# Install dependencies
pip install -r requirements.txt
# Run the bot
# READ CONFIGURATION OPTIONS FIRST!
python main.py 
```

To run this service, you simply need to run `python main.py` while you're in this folder.

When configured correctly, you should see the following:
```text
[NOTICE] Telegram session storage: telegram
[NOTICE] Loaded all env vars successfully.
[NOTICE] Ready to serve requests.
```
If you didn't change the trigger keyword, you can use `:dzbot help` for more information.

## This program wants to snatch my phone number?!?!

When you run this service for the first time, it's going to ask your phone number. This is the way
the underlying library (Telethon) works, and it's also required for accounts with MFA enabled.

This userbot doesn't send your phone number anywhere and it's only used to consume the Telegram API.