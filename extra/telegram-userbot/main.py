import time

from telethon import TelegramClient, events
import httpx
from peewee import IntegrityError
from core import const
from core.db import User
from core.tmp import AsyncTempFile

client = TelegramClient(
    const.SESSION_STORAGE_PATH, const.TELEGRAM_API_ID, const.TELEGRAM_API_HASH
)


def bytes_to_human_readable(size_in_bytes: int) -> str:
    # Define the units and the corresponding thresholds
    units = ["B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"]
    threshold = 1024.0

    # Initialize the index for units
    unit_index = 0

    # Keep dividing the size until it is less than the threshold
    while size_in_bytes >= threshold and unit_index < len(units) - 1:
        size_in_bytes /= threshold
        unit_index += 1

    # Return the formatted string with two decimal places
    return f"{size_in_bytes:.2f} {units[unit_index]}"


# Authenticate this user against the bot.
async def login(username: str, password: str, telegram_id: int):
    try:
        user = User(username=username, password=password, telegram_id=telegram_id)
        await user.authenticate()
        user.save()
        return "Auth success. You may now use this bot's features."
    except IntegrityError:
        return "Error: Username or Telegram ID already exists"
    except Exception as e:
        return f"Error: {e}"


def logout(telegram_id: int):
    query = User.delete().where(User.telegram_id == telegram_id)
    if query.execute():
        return "Successfully logged out."
    else:
        return "No user linked to your telegram account"

async def server_add(url: str, event: events.NewMessage.Event):
    telegram_id = event.sender_id
    try:
        user = User.get(User.telegram_id == telegram_id)
        token = await user.authenticate()
        message = await event.reply("Auth success, sending download request...")
        api_url = f"{const.API_URL}/api/download/link"
        json = {"link": url, "stream": False, "allowFallback": True}
        headers = {"Authorization": f"Bearer {token}"}
        http_client = httpx.AsyncClient()
        response = await http_client.post(api_url, json=json, headers=headers)
        json_response = response.json()
        if response.status_code != 200:
            await event.client.edit_message(
                message, f"API Error ({response.status_code}):\n```{json_response}```"
            )
            return
        else:
            download_id = json_response["downloadSession"]["id"]
            await event.client.edit_message(
                message, f"Ok: `{download_id}`"
            )
    except Exception as e:
        print(f"[ERROR] {e} {e.__class__}")
        await event.reply(f"Error: {e}")


# Download an album/playlist/whatever and send it
# to the user, whenever possible.
# Any temporary files are automatically cleaned up whenever
# this function exists.
async def get_stream(url: str, event: events.NewMessage.Event):
    telegram_id = event.sender_id
    try:
        user = User.get(User.telegram_id == telegram_id)
        token = await user.authenticate()
        message = await event.reply("Auth success, sending download request...")
        api_url = f"{const.API_URL}/api/download/link"
        json = {"link": url, "stream": True, "allowFallback": True}
        headers = {"Authorization": f"Bearer {token}"}
        http_client = httpx.AsyncClient()
        async with http_client.stream(
            "POST", api_url, json=json, timeout=500, headers=headers
        ) as response:
            if response.status_code != 200:
                await response.aread()
                await event.client.edit_message(
                    message, f"API Error: {response.json()}"
                )
                return
            await event.client.edit_message(
                message, f"Got 200 response, starting stream..."
            )

            last_update_sent = time.time()
            async with AsyncTempFile() as file:
                written = 0
                async for chunk in response.aiter_bytes():
                    written += len(chunk)
                    await file.write(chunk)
                    time_diff = time.time() - last_update_sent
                    if time_diff > 1:
                        last_update_sent = time.time()
                        written_human = bytes_to_human_readable(written)
                        await event.client.edit_message(
                            message, f"Downloaded {written_human} so far"
                        )

                await file.close()
                send_payload = file.path()
                file_name = file.path()
                # If this is a single file, extract it and read into a BytesIO
                # buffer.
                if single_song := file.read_single_file_zip():
                    send_payload = single_song
                    file_name = send_payload.name

                async def progress_callback(progress: float, total_bytes: int):
                    nonlocal last_update_sent
                    time_diff = time.time() - last_update_sent
                    if time_diff > 1:
                        last_update_sent = time.time()
                        progress_percentage = (progress / (total_bytes | 1)) * 100
                        progress_human = bytes_to_human_readable(progress)
                        total_bytes_human = bytes_to_human_readable(total_bytes)
                        await event.client.edit_message(
                            message,
                            f"Uploaded {progress_human}/{total_bytes_human} ({progress_percentage:.2f}%) so far",
                        )

                uploaded_file = await event.client.upload_file(
                    send_payload,
                    part_size_kb=512,
                    progress_callback=progress_callback,
                    file_name=file_name,
                )
                chat = await event.get_input_chat()
                await event.client.send_file(
                    chat,
                    uploaded_file,
                    reply_to=event.message,
                )
                # This might fail if the user deleted the original message, but it's no big
                # deal.
                try:
                    await event.client.delete_messages(event.chat, message, revoke=True)
                except Exception as e:
                    print(f"[WARN] Couldn't delete original message: {e}")
    except User.DoesNotExist:
        await event.reply("You're not authenticated against this bot.")
    except Exception as e:
        print(f"[ERROR] {e} {e.__class__}")
        await event.reply(f"Error: {e}")


def get_help():
    return const.HELP_TEXT


# We need another handler to deal with self-messages.
@client.on(
    events.NewMessage(outgoing=True, pattern=f"{const.TRIGGER_SELF_KEYWORD} (.+)")
)
async def handle_outgoing(event):
    await handler(event)


@client.on(events.NewMessage(pattern=f"{const.TRIGGER_KEYWORD} (.+)"))
async def handle_incoming(event):
    await handler(event)


# Handle incoming telegram events.
async def handler(event):
    command = event.pattern_match.group(1)
    print(f"[NOTICE] Command: '{command}' by user ID {event.sender_id}")
    args = command.split()

    if args[0] == const.CMD_LOGIN and len(args) == 3:
        response = await login(args[1], args[2], event.sender_id)
        await event.reply(response)

    elif args[0] == const.CMD_HELP:
        await event.reply(const.HELP_TEXT)

    elif args[0] == const.CMD_LOGOUT:
        response = logout(event.sender_id)
        await event.reply(response)

    elif args[0] == const.CMD_GET_STREAM and len(args) == 2:
        await get_stream(args[1], event)

    elif args[0] == const.CMD_SERVER_ADD and len(args) == 2:
        await server_add(args[1], event)

    else:
        await event.reply(const.INVALID_CMD_TXT)


async def main():
    await client.start(phone=const.TELEGRAM_PHONE_NUMBER)
    print("[NOTICE] Ready to serve requests.")


while True:
    try:
        with client:
            client.loop.run_until_complete(main())
            client.run_until_disconnected()
    except KeyboardInterrupt:
        print("[NOTICE] Caught ctrl+c. Shutting down.")
        break
    except Exception as e:
        print(f"[ERROR] {e}. Restarting event loop.")