# Navidrome refresh library webhook

This folder contains a quick, one-off, dirty webhook receiver that calls Navidrome's library update 
whenever Niceerr sends it a notification event.

The webhook ain't dumb: it only calls the update endpoint when there haven't been any new events
for a specific amount of time. This prevents calling the update endpoint way too often.

## Configuration

This is a rather simple program. It can read environment variables from a `.env`, just like niceerr.

| Variable | Description | 
|:-:|:-:|
| `listen_address` | Listening address, default: `127.0.0.1` |
| `listen_port` | Listening port, default: `8100` |
| `min_inactive_sec` | Wait this many seconds until things get calm (i.e. songs are no longer being downloaded) before calling the update endpoint. This effectively means your library will ONLY get updated after waiting this period. Default: `60` seconds. |
| `username` | Navidrome username |
| `url` | Navidrome URL, must be in the form `http://host.com/` |
| `username` | Navidrome username |
| `password` | Navidrome password |

## Coompile & run

This is just a regular rust program, you can run it with 

```bash
cargo run --release
```

### Notes

The webhook will only trigger a library refresh if, and only if all the following conditions are met:

- a song was downloaded completely
- a song was downloaded without any errors
- a song has a non-zero byte size

You don't have to take care of any of these conditions: niceerr sends all that data as part of the webhook event. 

