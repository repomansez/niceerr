use std::{
    error::Error,
    sync::{Arc, Mutex, OnceLock},
    time::{Duration, Instant},
};

use axum::{response::IntoResponse, routing::post, Json, Router};
use reqwest::StatusCode;
use serde::Deserialize;
use serde_inline_default::serde_inline_default;
use serde_json::Value;
use tokio::time::interval;
use url::Url;

#[derive(Debug)]
struct WatchServiceInner {
    last_activity: Instant,
    change_count: u64,
}

#[derive(Debug)]
struct WatchService {
    inner: Arc<Mutex<WatchServiceInner>>,
    min_inactive_sec: Duration,
    conf: Conf,
}

static SERVICE: OnceLock<WatchService> = OnceLock::new();

#[serde_inline_default]
#[derive(Debug, Deserialize)]
struct Conf {
    #[serde_inline_default(String::from("127.0.0.1"))]
    listen_address: String,
    #[serde_inline_default(8100)]
    listen_port: u16,
    #[serde_inline_default(60)]
    min_inactive_sec: u16,
    url: String,
    username: String,
    password: String,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct SongPayload {
    pub id: String,
    pub total_bytes: i32,
    pub downloaded_bytes: i32,
    pub error_message: Option<String>,
}

impl WatchService {
    fn init(conf: Conf) {
        let min_inactive_sec = Duration::from_secs(conf.min_inactive_sec.into());
        let svc = WatchService {
            conf,
            min_inactive_sec,
            inner: Arc::new(Mutex::new(WatchServiceInner {
                last_activity: Instant::now(),
                change_count: 0,
            })),
        };
        SERVICE.set(svc).expect("Cannot set service");
        tokio::spawn(Self::ticker());
    }

    fn update(&self) {
        let mut this = self.inner.lock().expect("cannot unlock mutex");
        this.change_count += 1;
        this.last_activity = Instant::now();
    }

    fn reset(&self) {
        let mut this = self.inner.lock().expect("cannot unlock mutex");
        this.change_count = 0;
        this.last_activity = Instant::now();
    }

    fn should_trigger(&self) -> bool {
        let this = self.inner.lock().expect("cannot unlock mutex");
        if this.change_count == 0 {
            return false;
        }
        this.last_activity.elapsed() > self.min_inactive_sec
    }

    async fn trigger_update() -> anyhow::Result<()> {
        // TODO: Don't calculate the password hash every time we enter this func
        let svc = SERVICE.get().expect("service isnt started");
        let password = md5::compute(format!("{}d513b8", svc.conf.password));
        let encoded = format!("{password:x}");
        let url = format!(
            "{}rest/startScan?u={}&t={}&s={}&f=json&v=1.8.0&c=NavidromeUI&fullScan=false",
            svc.conf.url, svc.conf.username, encoded, "d513b8"
        );
        let response = reqwest::get(&url)
            .await?
            .error_for_status()?
            .json::<Value>()
            .await?;
        println!("[ok] TRIGGERED! response: {:?}", response);
        Ok(())
    }

    async fn ticker() {
        let svc = SERVICE.get().expect("service isnt started");
        let mut sleeper = interval(svc.min_inactive_sec);
        loop {
            sleeper.tick().await;
            if svc.should_trigger() {
                println!("[ok] {:?}", Self::trigger_update().await);
                svc.reset();
            }
        }
    }
}

async fn trigger(Json(song): Json<SongPayload>) -> impl IntoResponse {
    let svc = SERVICE.get().expect("service isn't started");
    println!(
        "[ok] got song: id={}, error={:?}",
        song.id, song.error_message
    );
    if song.total_bytes > 0
        && song.downloaded_bytes == song.total_bytes
        && song.error_message.is_none()
    {
        println!("[ok] triggering timer for song {song:?}");
        svc.update();
    } else {
        println!("[warn] event: not triggering timer for song {song:?}");
    }
    StatusCode::OK
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // build our application with a single route
    dotenvy::dotenv().ok();
    let mut conf = envy::from_env::<Conf>()?;
    // Make sure we have a valid URL
    conf.url = Url::parse(&conf.url).expect("Cannot parse url").to_string();
    println!("[ok] parsed config: {conf:#?}");
    let app = Router::new().route("/", post(trigger));
    let address = format!("{}:{}", conf.listen_address, conf.listen_port);
    WatchService::init(conf);

    println!("[ok] spinning up webhook server at {address}...");
    let listener = tokio::net::TcpListener::bind(&address)
        .await
        .expect("Cannot attach to provided address");
    axum::serve(listener, app)
        .await
        .expect("Cannot start server");
    Ok(())
}
