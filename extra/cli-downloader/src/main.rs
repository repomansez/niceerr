use anyhow::{anyhow, bail, Context};
use clap::Parser;
use futures_util::StreamExt;
use log::{debug, error, info};
use rand::Rng;
use serde::Deserialize;
use serde_json::{json, Value};
use std::{
    env::current_dir,
    fmt::Display,
    fs::create_dir_all,
    io::BufReader,
    path::{Component, Path, PathBuf},
    time::{Duration, Instant},
};
use tokio::{
    fs::File,
    io::{AsyncSeekExt, AsyncWriteExt, BufWriter},
};
use url::Url;

#[derive(Debug, clap::ValueEnum, Clone, Default)]
enum Quality {
    #[default]
    Flac,
    Mp3_320,
    Mp3_128,
}

impl Display for Quality {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Quality::Flac => write!(f, "Flac"),
            Quality::Mp3_320 => write!(f, "Mp3_320"),
            Quality::Mp3_128 => write!(f, "Mp3_128"),
        }
    }
}

#[derive(Debug, Deserialize)]
struct Config {
    niceerr_instance_url: Url,
    username: String,
    password: String,
}

/// Simple CLI download tool for niceerr servers.
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// Deezer download link. This can be an artist, album, playlist or song.
    link: Url,
    /// Whether to disallow niceerr from falling back to crappier formats should the desired
    /// format be unavailable (default: true)
    #[arg(short, long, default_value = "false")]
    no_fallback: bool,
    /// When downloading discographies, include albums the artist is credited with
    /// (default: false)
    #[arg(short, long, default_value = "false")]
    include_credited: bool,
    /// Skip live content (may cause some content to not be downloaded at all).
    /// (default: false)
    #[arg(short, long, default_value = "false")]
    skip_live: bool,
    /// Download format
    #[arg(short, long, default_value = "flac")]
    quality: Quality,
    /// Config file path (Defaults to `<USER DIRECTORY>/.config/niceerr-cli/config.json` if not set)
    #[arg(short, long)]
    config_file: Option<String>,
    /// Optional download path (Defaults to the current working directory if not set)
    #[arg(short, long)]
    path: Option<String>,
}

async fn get_config(path: &Option<String>) -> anyhow::Result<Config> {
    let config_path = match path {
        Some(path) => PathBuf::from(path),
        None => homedir::get_my_home()
            .context("Cannot get user home dir")?
            .ok_or(anyhow::anyhow!("Cannot get user home dir"))?
            .join(".config")
            .join("niceerr-cli")
            .join("config.json"),
    };
    info!("Config path: {config_path:?}");
    let exists = tokio::fs::try_exists(&config_path).await?;
    if !exists {
        if path.is_none() {
            println!("Creating new file");
            let parent = config_path
                .parent()
                .expect("Cannot extract parent path for config");
            tokio::fs::create_dir_all(parent).await?;
            let example = "{\n  \"niceerr_instance_url\": \"https://your-niceerr-instance.com\",\n  \"username\": \"username\",\n  \"password\": \"password\"\n}";
            let mut file = tokio::fs::File::create(&config_path).await?;
            file.write_all(example.as_bytes()).await?;
        }
        bail!("Settings file doesn't exist. I've created a sample for you at {config_path:?}, but you'll need to modify it.");
    }
    let config = tokio::fs::read_to_string(&config_path).await?;
    let config: Config = serde_json::from_str(&config)?;
    Ok(config)
}

async fn get_token(client: &reqwest::Client, config: &Config) -> anyhow::Result<String> {
    let res = client
        .post(format!("{}api/user/login", config.niceerr_instance_url))
        .json(&json!({
            "username": config.username,
            "password": config.password
        }))
        .send()
        .await?
        .error_for_status()
        .context("Got invalid response: maybe you used the wrong credentials?")?
        .json::<Value>()
        .await?;
    let token = res["token"]
        .as_str()
        .ok_or(anyhow!("Couldn't extract token: malformed response"))?;
    Ok(token.to_string())
}

async fn write_to_file(
    response: reqwest::Response,
    file: &mut BufWriter<File>,
) -> anyhow::Result<usize> {
    let mut stream = response.bytes_stream();
    let mut bytes_written = 0;
    let mut last_progress_shown = Instant::now();
    while let Some(bytes) = stream
        .next()
        .await
        .transpose()
        .context("Couldn't read bytes from upstream server")?
    {
        bytes_written += bytes.len();
        if Instant::now().duration_since(last_progress_shown) > Duration::from_secs(1) {
            info!("Received {} bytes so far", bytes_written);
            last_progress_shown = Instant::now();
        }

        file.write_all(&bytes)
            .await
            .context("Couldn't write all bytes to upstream socket")?;
    }
    info!("Finished streaming: received {bytes_written} bytes");
    Ok::<_, anyhow::Error>(bytes_written)
}

fn unzip(file: std::fs::File, root_path: &Path) -> anyhow::Result<()> {
    let bufread = BufReader::new(file);
    let mut zip = zip::ZipArchive::new(bufread)?;
    info!("Unzipping {} file(s), please wait", zip.len());
    for idx in 0..zip.len() {
        let mut source = zip.by_index(idx)?;
        let path = PathBuf::from(&source.name());
        let normalized: PathBuf = path
            .components()
            .filter_map(|c| match c {
                // make sure this path isn't trying to do anything stupid
                // this should never happen, but we do it just in case
                Component::Normal(s) => Some(Component::Normal(s)),
                _ => None,
            })
            .collect();
        let full_path = root_path.join(&normalized);
        let folder_path = full_path
            .parent()
            .ok_or(anyhow!("Can't extract parent folder"))?;
        create_dir_all(folder_path).context("Cannot create sub-directory")?;
        let mut dest = std::io::BufWriter::new(
            std::fs::File::create(&full_path)
                .context(format!("Can't create file at {full_path:?}"))?,
        );
        if let Err(e) = std::io::copy(&mut source, &mut dest).context("Cannot copy file") {
            error!("Cannot copy file: {e:?}");
        };
    }
    Ok(())
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "cli_downloader=info");
    }
    env_logger::init();
    let args = Args::parse();
    let config = get_config(&args.config_file).await?;
    let payload = json!({
        "link": args.link,
        "includeCredited": args.include_credited,
        "skipLive": args.skip_live,
        "streamFormat": args.quality.to_string(),
        "allowFallback": !args.no_fallback,
        "stream": true
    });
    debug!("Payload: {payload:?}");
    let client = reqwest::Client::new();
    info!("Authenticating against {}", config.niceerr_instance_url);
    let token = get_token(&client, &config).await?;
    info!("Authenticated, sending stream request for {}.", args.link);
    let initial_response = client
        .post(format!("{}api/download/link", config.niceerr_instance_url))
        .header("Authorization", format!("Bearer {token}"))
        .json(&payload)
        .send()
        .await
        .context("Couldn't send initial request")?;
    if initial_response.error_for_status_ref().is_err() {
        let response_text = initial_response.text().await?;
        bail!("Got invalid response (maybe your link is invalid): {response_text}");
    }
    info!("Got successful response, starting stream now");

    let mut rng = rand::thread_rng();
    let rand_no: u64 = rng.gen();

    let root_path = match args.path {
        Some(path) => {
            let folder = PathBuf::from(path);
            tokio::fs::create_dir_all(&folder)
                .await
                .context("passed folder can't be accessed")?;
            folder
        }
        None => {
            info!("No path specified, using current working directory.");
            current_dir().context("Can't guess current working directory")?
        }
    };
    let zip_path = root_path.join(format!("{rand_no}.zip"));
    let file = tokio::fs::OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .truncate(true)
        .open(&zip_path)
        .await
        .context("Couldn't create temp zip file")?;
    info!("tmp zip: {zip_path:?}, extract folder: {root_path:?}");
    let mut writer = BufWriter::new(file);
    if let Err(e) = write_to_file(initial_response, &mut writer).await {
        tokio::fs::remove_file(&zip_path)
            .await
            .context("Couldn't remove temp file")?;
        bail!("Failed to download all byte contents: {e:?}");
    };
    writer.flush().await?;
    writer.rewind().await?;
    let file = writer.into_inner().into_std().await;
    unzip(file, &root_path)
        .inspect_err(|e| {
            error!("Failed to unzip file: {e:?}");
        })
        .ok();
    // Remove temp zip
    tokio::fs::remove_file(&zip_path).await.ok();
    info!("All done, have a nice day :)");
    Ok(())
}
