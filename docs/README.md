# `niceerr`

A **nicer** music discovery server that uses deeznuts as its backend.

## Features

- Ruste 🦀
- Backend written in epic rust for efficient resource usage (uses <10MB of ram when idle)
- Simple frontend written in tasty, sexy vue.js for extra pleasure
- Multi-user access
- Tiered privilege system: you can have as many admins as you want, but none of the admins can modify or otherwise delete you (or data) data from higher-ranked users. This means you can have multiple admins and still retain control over all of them.
- Ability to set per-user daily bandwidth quota
- Ability to enable on a per-user basis direct/streamed downloads (useful if you just want to quickly download something) and server-side downloads (if you want to build a collection).
- Ability to download stuff that was previously downloaded in the server.
- Ability to set per-user retention periods (i.e. if you don't want to keep someone else's music forever)
- Ability to set per-user download folders
- Ability to configure ARLs and set their priority (this means, the system will always have a valid, working ARL). You can choose which ARL is used first, and you can also have a list of backup ARLs, depending on their priority.
- Ability to retry or delete files from existing download sessions 
- Webhook support: allows you to update your library or perform any action whenever a song has been downloaded
- **EXTRA**: Navidrome refresh webhook, written in rust
- **EXTRA**: CLI download tool, also written in rust
- **EXTRA**: Standalone telegram userbot client for Niceerr


...and other features.

## Configuration

This server exclusively uses Postgres and it won't run with any other database implementation such as MariaDB or MySQL. Niceerr won't run without a valid database connection.

The server only requires you to configure a few environment variables. Note that you can set these variables through a `.env` file (a sample 
env file can be found under `extra/env-sample`.)


| Env variable                | Optional | Example                                                     | Description          | 
|:---------------------------:|:--------:|:-----------------------------------------------------------:|:--------------------:|
| `database_url`              |  **No**  | `postgres://username:password@192.168.0.1:5432/niceerr`     | Postgres database URL|
| `dangerous_cors_origin_allow_any` (see note 2) |    Yes    | `false`     | Completely disable CORS. Useful if you're gonna use niceerr through a `http://localhost` instance. |
| `cors_origins` (see note 2) |  **No (can be optional if `dangerous_cors_origin_allow_any` is set to true)**  | `https://mywebsite.com,https://myotherwebsite.com`          | List of allowed URL origins. If you're hosting a niceerr instance, set this to your domain URL.  |
| `jwt_secret`       |  **No**  | `asdasdasdasdasdasdadsad`                                   | JWT signing key (can be any random string)      |
| `default_admin_password`    |  **No**  | `ILikePizz69`                                               | Default admin password, only used when niceerr runs for the first time     |
| `downloads_folder`          |  **No**  | `.`                                                         | Root downloads folder     |
| `temp_folder`               |    Yes   | `/tmp`                                                      | Temp folder, only used when streaming downloads option is active. By default, uses the OS' default temp folder.     |
| `max_grants_per_user`       |    Yes   | `5`                                                         | Max number of ongoing/enqueued download requests per user     |
| `listen_address`            |    Yes   | `[::]:8000`                                                 | Inbound listening address and port, can be either Ipv4 or Ipv6 address with port     |
| `prometheus_basic_auth_credentials` |    Yes   | `username:password` (separated by a colon)          | Username and password used for the prometheus metrics endpoint. If not set, then the `/metrics` endpoint won't be available at all, and the feature will be disabled. Note that this is disabled by default, and regular user credentials won't work with this endpoint. These credentials are always static. |
| `jwt_expiration_sec`        |    Yes   | `43200`s (12 hours)                                         | JWT session duration (sessions will expire after 12h unless the user renews their token, which happens automatically in the UI)     |
| `user_agent `               |    Yes   | `Mozilla/5.0 (Windows NT 10...`                             | Default user agent to use for the HTTP client     |
| `arl_refresh_max_age_sec`   |    Yes    | `1200`s (20 min)                                           | When Niceerr is actively downloading stuff, refresh the tokens used every this many seconds     |
| `arl_refresher_inactivity_timeout_sec`   |    Yes    | `300`s (5 min)                                | Shut down the arl refresher after this many seconds of stream or download inactivity, and free up resources |
| `expired_token_prune_sec`   |    Yes    | `43200` (12 hours)                                         | Prune expired ARLs from database every this many seconds. Set to 0 to disable automatic pruning.         |
| `artist_name_schema`        |    Yes    | `$ARTIST_NAME`                                             | Artist naming schema (see below for more info)     |
| `album_name_schema`         |    Yes    | `$ALBUM_NAME`                                              | Album naming schema (see below for more info)     |
| `song_name_schema`          |    Yes    | `$TRACK_NUMBER. $TRACK_NAME $VERSION`                      | Song naming schema (see below for more info)     |
| `playlist_name_schema`      |    Yes    | `$TITLE. ($USER_ID)`                                       | Playlist naming schema (see below for more info)     |
| `playlist_folder`           |    Yes    | `Playlists`                                                | When download playlists, save the contents under this folder. This folder will always exist under the main `downloads_folder`. If a download folder was set for any given user, then the structure will be `downloads_folder`/`playlist_folder`/<USER folder>/`playlist_name_schema`.|
| `server_side_download_workers`     |    Yes    | `6`                                                 | Worker/Thread count for server-side downloads. A high count may cause network congestion/timeout issues.     |
| `streaming_download_workers`|    Yes    | `3`                                                        | Worker/Thread count for streaming downloads. A higher number means songs will be delivered faster, but it also means the server will waste bandwidth if the client cancels the download.     |
| `streaming_remote_timeout_sec`|    Yes    | `120`s                                                      | Streaming downloads only: Sets the maximum time (in seconds) a song can remain enqueued (waiting for download) before cancelling the entire download session. This prevents slow downloaders from causing resource starvation, **but might cause problems if 1. the remote client is extremely slow (<100kB/s), 2. the server has an extremely slow internet connection such that it is unable to download at least 1 song every `streaming_remote_timeout_sec` sec, and 3. if the server's network is unstable such that it is unable to download at least 1 song every `streaming_remote_timeout_sec` sec.** |
| `stream_download_buffer_byte_size` |    Yes    | `131072` (128KiB)                                   | When streaming (e.g. when delivering data as a stream to the browser/curl/etc), use this buffer size. Higher values yield faster download speeds, but might be wasteful if the downloading client is slow.  |
| `streaming_download_queue_size`|    Yes    | `1`                                                     | When streaming from the upstream provider (and not from disk), keep this many songs in the queue. A value of 0 means niceerr will wait until the client downloads the song completely to start another download. A higher number can allow clients with slower internet to download continuously, but will result in wasted bandwidth if the client cancels the download. |
| `album_fetcher_queue_size`|    Yes    | `2`                                                     | Fetch this many albums and put them in a 'wait queue' so that they're always ready for immediate consumption by the download workers. A high number will slightly increase memory usage and won't necessarily lead to improved download speeds. Set to `2` by default. |
| `enable_stream_downloads`   |    Yes    | `true`                                                     | Whether to globally enable downloads for both streaming (direct) and for existing, pre-downloaded content. |
| `enable_server_downloads`   |    Yes    | `true`                                                     | Whether to globally enable server-side downloads. If disabled, users won't be able to store any content in the server. |
| `connect_timeout_sec`       |    Yes    | `60`                                                       | HTTP client: connection timeout, in seconds      |
| `read_timeout_sec`          |    Yes    | `60`                                                       | HTTP client: read timeout, in seconds      |
| `tcp_keepalive_sec`         |    Yes    | `60`                                                       | HTTP client: Keep TCP sockets alive for this many seconds. Set to `0` to disable, default: `60` seconds.      |
| `http_accept_language`      |    Yes    | `en`                                                       | Use this language to make requests. Currently, this only changes the language used to burn tags (particularly, the genre). It must be a valid 2-letter ISO669-1 country code, e.g. `en`, `es`, `fr`, `it`, `pt`, etc. Only common languages are supported; please see `config/src/iso636_lang.rs` for a full list.  |
| `retry_backoff_ceil`        |    Yes    | `65`                                                       | When retrying failed downloads, and after at least >0 attempts, wait at most this many seconds after exponential backoff has kicked in        |
| `max_retries`               |    Yes    | `5`                                                        | Retry a failed download this many times. Note that exponential backoff will kick in after the 1st try, i.e. the first retry will wait 0 seconds, the next time 2^0, then 2^1, 2^2 and so on, but never more than `retry_backoff_ceil` seconds.  |
| `prune_service_interval_sec`|    Yes    | `1800` sec (30min)                                         | Run the download pruner every this many seconds (1/2 hour by default)  |
| `max_semaphore_lifetime_sec`|    Yes    | `60`                                                       | Max per-user semaphore lifetime before it's considered stale  |
| `shutdown_max_loops`        |    Yes    | `20`                                                       | When shutting down, check this many times if all downloads have been stopped successfully. There's a 500ms delay between each loop. Default is 20*500ms = 10s.  |
| `timezone`                  |    Yes    | `Utc`                                                      | Timezone to be used to calculate the daily bandwidth quota |
| `create_folder_for_each_disk`|    Yes   | `true`                                                      | Whether to create separate folders for each disk (only applicable to multi-disk albums) |
| `webhook_url`        |    Yes    | `http://example.com`                                         | Webhook to call (POST) to whenever a song has been either downloaded or cancelled.  (See note 2) |
| `housekeeping_interval_sec`|    Yes    | `3600`                                                      | Run housekeeping tasks every this many seconds. This includes: freeing up memory used by the internal queues, cleaning up the tmp directory, and so on. |
| `allow_info_access_level`|    Yes    | `0`                                                      | Allow admins whose level is equal to or less than this to access the info endpoint. The info only contains all-time harmless statistics, but in any case niceerr allows you to limit access to it. By default, only the super-superuser has access to it. |
| `db_max_connections`|    Yes    | `100`                                                      | Max number of DB connections at any given point. |
| `db_min_connections`|    Yes    | `1`                                                        | Min number of DB connections to keep at all times. |
| `db_idle_timeout_sec`|    Yes    | `60` (1min)                                             | Wait this many seconds before closing idle database connections. |
| `db_connect_timeout_sec`|    Yes    | `10` (10s)                                      | When niceerr runs for the first time, wait this many seconds when connecting to DB. |
| `sse_keep_alive_interval_sec`|    Yes    | `15` (15s)                                | For any ServerSideEvent functionality in Niceerr, send downstream clients a keep alive pings every this many seconds. Setting this value to more than 30 might lead to lost/dropped connections and wasted resources from the clients. |
| `sse_ongoing_downloads_poll_interval_msec`|    Yes    | `1000` (1000ms/1s)                  | How often to send clients updates when they're viewing the "Ongoing downloads" page. Set to 1000ms by default. This value must be greater than or equal to `500`ms. |
| `server_enable_relay`|    Yes    | `true`, `false`, set to `false` by default | Whether to globally enable relay access. This allows other Niceerr clients to use your instance to generate download links. Barely consumes any bandwidth, but might increase your ARL's download quota if overused. |
| `relay_url`|    Yes    | `https://username:password@example.com`  | Use an existing Niceerr server (with a valid ARL) to download stuff. Note that this doesn't use any bandwidth at all: the remote server is only used to generate the download links, and downloads are executed locally. **SEE NOTE 1** |
| `relay_client_refresh_sec`|    Yes    | `7200` (seconds) | When `relay_url` is set, refresh the relay token every this many seconds (set to 2h by default). If `relay_url` isn't set, this won't do anything. |

## Docker setup 

Please read `docker.md` in this folder

## **VERY** Important notes

**EXTREMELY IMPORTANT:** It's recommended to leave **OPTIONAL** settings unchanged, unless you know what you're doing. Changing advanced parameters (such as `housekeeping_interval_sec`, `retry_backoff_ceil`, `prune_service_interval_sec`, etc) might have negative side-effects.


### Note 1: RELAY setup

**WHAT THE HECK IS EVEN THAT?**

The relay is a nice and cool feature that allows you to:

- As a client: run Niceerr with an invalid or expired ARL and still download songs in lossless quality, using another Niceerr instance with a valid ARL
- As a server: allow other Niceerr instances to use your ARL to generate stream links, without sharing your ARL

The above features are **MUTUALLY EXCLUSIVE** and can't be enabled at the same time. 

**CLIENT SETUP**

Steps:

1. Set `relay_url` env variable. If you're using a superuser, no further setup should be needed. Otherwise, make sure your user has the `Relay access` permission.
2. Start your Niceerr instance.
3. Add an expired/invalid ARL to your Niceerr instance. The reason why you still need an ARL is because Niceerr needs one to fetch metadata.

Some sub-notes:

- When you use a relay, Niceerr will allow you to use expired/non-premium ARLs
- If you have a valid ARL and it can stream the requested contents at the requested quality, Niceerr won't use the relay (because there's no point in doing so)
- If the configured server uses an ARL that can't stream the requested contents (probably because of geo-blocking), these errors will be forwarded to your client instance.

**SERVER SETUP**

Set `server_enable_relay` to `true` and make sure your server has a valid ARL. 


### Note 2:  CORS Setup

**NOTE 1** You **SHOULD** always configure CORS (hopefully, although you can override this). CORS (short for Cross Ourigin Resource Sharing) prevents different people from accessing your server from a different domain, and it mitigates some kinds of attacks to a certain degree. 
HTTP is fucked up protocol but it's the way things are, so please configure CORS.

### Note 3: Webhook

Webhooks basically are esentially callback functions that are invoked whenever something happens on a server (which, in this case, is niceerr). When you configure the webhook URL, niceerr will always call it whenever a new song is either cancelled or downloaded with the following payload:

```json
{
    "id": "SOME-UUID",
    "sessionId": "ANOTHER-UUID",
    "itemID": "deezerItemId",
    "totalBytes": 12345,
    "downloadedBytes": 123456,
    "downloadPath": "/home/some/path",
    "errorMessage": "Some random error message"
}
```

HTTP method will always be `POST`. Webhook events will be permanently lost if the webhook endpoint is down or if it's too slow to process the webhook events.

**A navidrome refresh** webhook is provided under `/extra`. 

**The webhook won't be called when someone performs a direct streaming download**. These downloads don't save anything to the server, so it doesn't make any sense to call the webhook.

## Artist/Album/Song naming schemas

Note: All leading or trailing spaces will be automatically removed

### Keys available for `artist_name_schema`:

| Key | Description | 
|:---:|:-----------:|
|`$ARTIST_ID` | Artist's upstream (service) ID. It's usually useless. |
|`$ARTIST_NAME` | Artist's name. |
|`$LABEL` | Artist's label. |

Default: `$ARTIST_NAME`

### Keys available for `album_name_schema`:

| Key | Description | 
|:---:|:-----------:|
|`$ALBUM_ID` | Album's upstream (service) ID. It's usually useless, unless you use it to disambiguate albums with the same name.  |
|`$ALBUM_NAME` | Album's name. |
|`$ARTIST_ID` | Album artist's ID. |
|`$ARTIST_NAME` | Album's artist name |
|`$LABEL` | Album's label name |

Default: `$ALBUM_NAME`


### Keys available for `song_name_schema`:

| Key | Description | 
|:---:|:-----------:|
|`$ID` | song's Upstream (service) ID  |
|`$TRACK_NAME` | Song's name  |
|`$ARTIST_ID` | Song's artist upstream (service) ID  |
|`$ARTIST_NAME` | Song's artist name  |
|`$ALBUM_NAME` | Song's album name  |
|`$ALBUM_ID` | Song's album upstream (service) ID  |
|`$DISK_NUMBER` | Song's disk number  |
|`$TRACK_NUMBER` | Song's track number within its disk |
|`$ISRC` | Song's ISRC |
|`$VERSION` | Song version, e.g. 'Death **(Live at Wacken)**', 'Abyss **(BBC Recording Session)**', etc. |

Default: `$TRACK_NUMBER. $TRACK_NAME $VERSION`


### Keys available for `playlist_name_schema`:

| Key | Description | 
|:---:|:-----------:|
|`$ID` | Playlist's Upstream (service) ID  |
|`$CHECKSUM` | Playlist's checksum (only deezer know what this checksum means, lol)  |
|`$DATE_ADD` | Playlist's creation date  |
|`$DATE_MOD` | Playlist's last modification date  |
|`$USERNAME` | Playlist's creator username  |
|`$USER_ID` | Playlist's creator ID  |
|`$TITLE` | Playlist name  |

Default: `$TITLE ($ID)`

## Compile/Install/Run

To compile the server (written in rust), you'll need the rust compiler.

```bash
git clone <THIS REPOSITORY>
cd niceerr
cargo build --release
cp target/release/niceerr .
export RUST_LOG=niceerr=info
# Note that once niceerr is compiled, you can copy it to your /usr/local/bin folder.
./niceerr
```

To compile the Vue frontend, you'll need `npm`. This step assumes you already cloned the repository:

```bash
cd ui
npm install
npx quasar build
# Once this command is finished, there'll be a folder under `dist/spa`. This is the SPA folder and
# it must be served with Caddy or nginx.
```

**The default username is `niceerr`, and the password will be whatever `default_admin_password` is set to.**

You'll need to serve both the rust service and the vue frontend from the same domain to be able 
to use niceerr. 
You can find the relevant files in the `extra` folder. Note that `/PATH_TO_SPA` refers to the path where
the Vue frontend was compiled, which is usually under `ui/dist/spa`.

## Some treats!

You'll find under the `extra` directory the following tools:

- A Prometheus sample config .yaml, which you can use to monitor Niceerr statistics
- A Navidrome refresh hook (also written in rust) that works with niceerr's webhook feature. It'll trigger a library refresh whenever you download new stuff,
- A CLI tool that allows you to directly download stuff from your terminal, without opening the browser,
- A python telegram userbot, that allows you to use niceerr from telegram,
- A systemd service unit that allows you to run niceerr as a service, and
- A Caddyfile with a basically ready-to-use configuration that serves both niceerr and its frontend.
