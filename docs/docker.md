# Docker setup

containerized niceness
 containerized niceness
  containerized niceness

## WARNING NOTE

Make sure you read the main README first!


## Instructions

There are no pre-built images, but you can easily build your own image by running the commands below:

```bash
git clone <this repository>
cd <this repository>

# This is the real deal: this will build everything for you inside docker, without ever touching
# your environment.
docker build -t niceerr .

```

## Config & run

This docker image is configured through a .env file. 

Make sure you use a valid .env file before you run the resulting docker image!

When using the docker image, make sure you DO NOT CHANGE `downloads_folder` as the docker image
is hardcoded to internally use `/downloads`. You can still use whatever downloads folder you want
by changing the command used to run the image.

Quick and dirty minimal `.env` file:

```bash
jwt_secret=abcd
# This is very bad. Please read the main docs for instructions on how to configure this.
dangerous_cors_origin_allow_any=true
default_admin_password=admin
database_url=postgres://username:password@192.168.0.69:5432/niceerr
```

Once you've pasted that into a `.env` file, you should be ready to run Niceerr:

```bash
# This is where Niceerr will save server-side downloads
mkdir cool_downloads
# You can change this to whatever you want
export SUPER_COOL_PORT=9000
# Do not change the right side of the volume mapping, i.e.
# "/downloads" or "/etc/niceerr/.env"! 
docker run -v ./cool_downloads:/downloads -v $PWD/.env:/etc/niceerr/.env -p $SUPER_COOL_PORT:80 niceerr
```

