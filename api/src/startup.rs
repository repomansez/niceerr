use log::{error, info, warn};
use niceerr_config::{Config, DEFAULT_NICEERR_USERNAME};
use niceerr_entity::{
    download_session::Model as DownloadSession, traits::Countable, user::Model as UserModel,
};
use serde_json::json;

use crate::ty::ApiResult;

pub(crate) async fn exec_db_startup_tasks() {
    create_default_user_conditionally().await.ok();
    update_progress_to_error().await.ok();
}

async fn create_default_user_conditionally() -> ApiResult<()> {
    if UserModel::count().await? > 0 {
        info!("User table isn't empty, skipping default user creation");
        return Ok(());
    }

    let password = &Config::get().default_admin_password;

    // Create default superuser. serde will automatically populate all
    // the remaining fields.
    let user = json!({
        "username": DEFAULT_NICEERR_USERNAME,
        "password": password,
        "superuser": true,
    });
    let user_create = serde_json::from_value::<UserModel>(user).expect("unexpected failure");
    user_create
        .create(None)
        .await
        .inspect_err(|e| error!("Failed to create default user: {}", e))?;
    info!(
        "created default user with username '{}'",
        DEFAULT_NICEERR_USERNAME
    );
    Ok(())
}

// Sets progress with IN_PROGRESS status to ERROR. This can only happen if the server was abruptly stopped.
async fn update_progress_to_error() -> ApiResult<()> {
    let updated = DownloadSession::update_progress_to_error().await?;
    if updated > 0 {
        warn!("{} download sessions were updated to error status", updated);
    }
    Ok(())
}
