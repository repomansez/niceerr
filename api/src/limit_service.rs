use std::{ops::Deref, sync::LazyLock};

use dashmap::DashMap;
use log::{error, info};
use niceerr_config::Config;
use uuid::Uuid;

use crate::{error::ApiError, ty::ApiResult};
use niceerr_entity::user::Model as User;

#[derive(Clone, Debug)]
pub struct LimitService {
    inner: DashMap<Uuid, usize>,
}

#[derive(Debug, Clone)]
pub struct LimitServicePermit {
    pub user: Uuid,
}

// Wraps an user and associates it with an owned semaphore permit.
#[derive(Debug)]
pub struct WrappedUser {
    user: User,
    permit: LimitServicePermit,
}

impl WrappedUser {
    pub fn new(user: User) -> ApiResult<WrappedUser> {
        let permit = LimitService::get().try_new_permit(&user.id)?;
        Ok(WrappedUser { user, permit })
    }

    pub fn into_inner(self) -> (User, LimitServicePermit) {
        (self.user, self.permit)
    }
}

impl Deref for WrappedUser {
    type Target = User;

    fn deref(&self) -> &Self::Target {
        &self.user
    }
}

static LIMIT_SERVICE: LazyLock<LimitService> = LazyLock::new(|| LimitService {
    inner: DashMap::new(),
});

impl LimitService {
    pub fn try_new_permit(&self, key: &Uuid) -> ApiResult<LimitServicePermit> {
        let inner = &self.inner;
        let mut binding = inner.entry(*key).or_insert_with(|| 0);
        let limit = Config::get().max_grants_per_user;
        if *binding == limit as usize {
            info!("user/key {key} has reached the maximum number of permits ({limit})");
            return Err(ApiError::MaxResourceLimitExceeded);
        }
        *binding += 1;
        Ok(LimitServicePermit { user: *key })
    }

    /// Shrink the inner capacity as much as possible to free up unused resources
    pub fn shrink_to_fit(&self) {
        self.inner.shrink_to_fit();
    }

    fn release_permit(&self, key: &Uuid) {
        match self.inner.get_mut(key) {
            Some(mut v) => {
                if *v == 0 {
                    error!("[BUG] user/key {key} isn't supposed to be zero!");
                    drop(v);
                    self.inner.remove(key);
                    return;
                }
                *v -= 1;
                if *v == 0 {
                    info!("user/key {key} has no active permits, cleaning up...");
                    drop(v);
                    self.inner.remove(key);
                }
            }
            None => {
                error!(
                    "[BUG] user/key {key} not found in limit service but still got a removal request for it"
                );
            }
        }
    }

    /// Returns the stats of the limit service containing a two-item tuple:
    /// the number of active users and the number of granted permits.
    pub fn stats() -> (usize, usize) {
        let svc = Self::get();
        let users = svc.inner.len();
        let grants = svc.inner.iter().fold(0, |mut accum, value| {
            accum += *value;
            accum
        });
        (users, grants)
    }

    pub fn init() {
        info!(
            "Limit service started, max streams={}",
            Config::get().max_grants_per_user
        );
    }

    pub fn get() -> &'static LimitService {
        &LIMIT_SERVICE
    }
}

impl Drop for LimitServicePermit {
    fn drop(&mut self) {
        LimitService::get().release_permit(&self.user);
    }
}
