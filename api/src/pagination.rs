use axum::{http::HeaderMap, response::IntoResponse, Json};
use serde::Serialize;

pub struct PaginatedResponse<T>
where
    T: Serialize,
{
    pub items: Vec<T>,
    pub total_items: u64,
    pub total_pages: u64,
}

impl<T: Serialize> PaginatedResponse<T> {
    pub fn new(items: Vec<T>, total_items: u64, total_pages: u64) -> Self {
        Self {
            items,
            total_items,
            total_pages,
        }
    }
}

impl<T> IntoResponse for PaginatedResponse<T>
where
    T: Serialize,
{
    fn into_response(self) -> axum::response::Response {
        let mut headers = HeaderMap::new();
        headers.insert("x-total-items", self.total_items.into());
        headers.insert("x-total-pages", self.total_pages.into());
        let body = Json(self.items);
        (headers, body).into_response()
    }
}

/// (items, total_items, total_pages)
impl<T: Serialize> From<(Vec<T>, u64, u64)> for PaginatedResponse<T> {
    fn from(value: (Vec<T>, u64, u64)) -> Self {
        Self::new(value.0, value.1, value.2)
    }
}
