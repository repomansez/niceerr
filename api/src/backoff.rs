use std::{future::Future, time::Duration};

use log::{error, warn};
use niceerr_config::Config;
use tokio::select;
use tokio_util::sync::CancellationToken;

use crate::{error::ApiError, ty::ApiResult};

pub enum ExecutionResult<T> {
    Completed(T),
    Cancelled,
    TimedOut,
}

/// future runner with exponential backoff logic between calls
struct CancellableExponentialBackoff<'a, F, C, O>
where
    F: Future<Output = O> + Send,
    C: Fn() -> F,
{
    /// The current iteration we're at.
    pub iter_count: u32,
    // The future function factory, used to create brand-new futures that we
    // can call over and over again.
    max_retries: u32,
    future_factory: C,
    cancel_token: &'a CancellationToken,
}

impl<'a, F, C, O> CancellableExponentialBackoff<'a, F, C, O>
where
    F: Future<Output = O> + Send,
    C: Fn() -> F,
{
    pub fn new(future_factory: C, cancel_token: &'a CancellationToken) -> Self {
        Self {
            future_factory,
            max_retries: Config::get().max_retries as u32,
            iter_count: 0,
            cancel_token,
        }
    }

    // Invokes the factory and runs the returned future.
    async fn invoke(&self) -> O {
        (self.future_factory)().await
    }

    /// Call this backoff instance's inner function
    ///
    /// Returns None whenever we reach MAX_RETRIES as per the config,
    /// otherwise returns Some(O) where O is the output of the future.
    pub async fn call(&mut self) -> ExecutionResult<O> {
        if self.iter_count == 0 {
            // This is the first run, so we can just
            // return the result right away.
            self.iter_count += 1;
            return ExecutionResult::Completed(self.invoke().await);
        } else if self.iter_count == self.max_retries {
            return ExecutionResult::TimedOut;
        }
        let new_delay = 2u32
            .pow(self.iter_count)
            .min(Config::get().retry_backoff_ceil);
        select! {
            _ = self.cancel_token.cancelled() => {
                return ExecutionResult::Cancelled;
            }
            _ = tokio::time::sleep(Duration::from_secs(new_delay.into())) => {

            }
        }
        self.iter_count += 1;
        ExecutionResult::Completed(self.invoke().await)
    }
}

/// A simple wrapper around the bigger and more complex
/// ExponentialBackoff runner.      
pub struct EBCancellableRunner<'a, F, C, T>
where
    F: Future<Output = ApiResult<T>> + Send,
    C: Fn() -> F,
{
    backoff: CancellableExponentialBackoff<'a, F, C, ApiResult<T>>,
}

impl<'a, F, C, T> EBCancellableRunner<'a, F, C, T>
where
    F: Future<Output = ApiResult<T>> + Send,
    C: Fn() -> F,
{
    pub fn new(future_factory: C, cancel_token: &'a CancellationToken) -> Self {
        Self {
            backoff: CancellableExponentialBackoff::new(future_factory, cancel_token),
        }
    }

    /// Call this backoff instance.
    ///
    /// This function allows the caller to pass a check function that gets called
    /// whenever the underlying function returns an error. If the check function
    /// returns true, the error is immediately returned to the caller and the
    /// backoff is stopped.
    pub async fn call_with_err_cbk<M>(&mut self, return_error: M) -> ApiResult<T>
    where
        M: Fn(&ApiError) -> bool,
    {
        let mut last_error = None;
        loop {
            let result = self.backoff.call().await;
            match result {
                ExecutionResult::Completed(Ok(t)) => return Ok(t),
                ExecutionResult::Completed(Err(e)) => {
                    if return_error(&e) {
                        return Err(e);
                    }

                    warn!(
                        "[{}/{}] Backoff triggered, reason: {e:?}",
                        self.backoff.iter_count, self.backoff.max_retries
                    );
                    last_error = Some(e);
                }
                ExecutionResult::TimedOut => {
                    return Err(last_error.unwrap_or_else(|| {
                        error!("BUG: Reached timeout and there was no previous error!");
                        ApiError::InternalError
                    }))
                }
                ExecutionResult::Cancelled => return Err(ApiError::InternalError),
            };
        }
    }

    pub async fn call(&mut self) -> ApiResult<T> {
        let mut last_error = None;
        loop {
            let result = self.backoff.call().await;
            match result {
                ExecutionResult::Completed(Ok(t)) => return Ok(t),
                ExecutionResult::Completed(Err(e)) => {
                    warn!(
                        "[{}/{}] Backoff triggered, reason: {e:?}",
                        self.backoff.iter_count, self.backoff.max_retries
                    );
                    last_error = Some(e);
                }
                ExecutionResult::TimedOut => {
                    return Err(last_error.unwrap_or_else(|| {
                        error!("BUG: Reached timeout and there was no previous error!");
                        ApiError::InternalError
                    }))
                }
                ExecutionResult::Cancelled => return Err(ApiError::InternalError),
            };
        }
    }
}

/// A cancellable runner with exponential backoff.
pub struct ExponentialBackoffCancellableMut<'a, T> {
    inner: &'a mut T,
    pub iter_count: u32,
    max_retries: u32,
    cancel_token: CancellationToken,
}

impl<'a, T> ExponentialBackoffCancellableMut<'a, T> {
    pub fn new(inner: &'a mut T, cancel_token: CancellationToken) -> Self {
        Self {
            inner,
            iter_count: 0,
            max_retries: Config::get().max_retries as u32,
            cancel_token,
        }
    }

    /// Call the cancellable runner. If this call was cancelled while we were
    /// waiting for the future to complete, None will be returned.
    ///
    /// The driver function must take the underlying, inner data as a mutable
    /// parameter and return a future.
    pub async fn call<'b, F, C, O>(&'b mut self, driver: C) -> ExecutionResult<O>
    where
        F: Future<Output = O> + Send,
        C: Fn(&'b mut T) -> F,
    {
        if self.iter_count == self.max_retries {
            return ExecutionResult::TimedOut;
        }

        if self.iter_count > 0 {
            let new_delay = 2u32
                .pow(self.iter_count)
                .min(Config::get().retry_backoff_ceil);
            let sleep = tokio::time::sleep(Duration::from_secs(new_delay.into()));
            select! {
            _ = self.cancel_token.cancelled() => {
                return ExecutionResult::Cancelled;
            }
            _ = sleep => {},
            }
        }

        self.iter_count += 1;
        let runnable = (driver)(self.inner);

        select! {
            r = runnable => ExecutionResult::Completed(r),
            _ = self.cancel_token.cancelled() => {
                ExecutionResult::Cancelled
            }
        }
    }
}
