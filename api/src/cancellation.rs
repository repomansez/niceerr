use std::future::Future;

use tokio::select;
use tokio_util::sync::CancellationToken;

/// future runner that can be easily cancelled.
pub struct CancellableRunner<'a, F, C, O>
where
    F: Future<Output = O> + Send,
    C: Fn() -> F,
{
    future_factory: C,
    cancel_token: &'a CancellationToken,
}

impl<'a, F, C, O> CancellableRunner<'a, F, C, O>
where
    F: Future<Output = O> + Send,
    C: Fn() -> F,
{
    pub fn new(future_factory: C, cancel_token: &'a CancellationToken) -> Self {
        Self {
            future_factory,
            cancel_token,
        }
    }

    // Invokes the factory and runs the returned future.
    async fn invoke(&self) -> O {
        (self.future_factory)().await
    }

    /// Call the cancellable runner. If this call was cancelled while we were waiting for the future to complete,
    /// None will be returned.
    pub async fn call(&mut self) -> Option<O> {
        select! {
            _ = self.cancel_token.cancelled() => {
                None
            }
            r = self.invoke() => Some(r),
        }
    }
}
