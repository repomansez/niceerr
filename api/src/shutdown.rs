use std::{
    sync::{atomic::AtomicBool, LazyLock, OnceLock},
    time::Duration,
};

use flume::bounded;
use log::{info, warn};
use niceerr_config::Config;
use tokio::signal::unix::{signal, SignalKind};
use tokio::time::interval;

use crate::download::ty::DownloadService;

static RX_SHUTDOWN_RECEIVER: OnceLock<flume::Receiver<()>> = OnceLock::new();
static IS_SHUTTING_DOWN: LazyLock<AtomicBool> = LazyLock::new(|| AtomicBool::new(false));

async fn handle_sigterm() {
    let stream = signal(SignalKind::terminate());
    if let Ok(mut signal) = stream {
        let _ = signal.recv().await;
    }
    info!("SIGTERM received");
}

pub fn init() {
    let (tx, rx) = bounded(0);
    tokio::spawn(async move {
        tokio::select! {
            _ = tokio::signal::ctrl_c() => {}
            _ = handle_sigterm() => {}
        }
        tx.send_async(()).await.ok();
    });
    RX_SHUTDOWN_RECEIVER
        .set(rx)
        .expect("cannot set shutdown signal handler");
}

pub fn is_shutting_down() -> bool {
    IS_SHUTTING_DOWN.load(std::sync::atomic::Ordering::Relaxed)
}

pub async fn shutdown() {
    let rx = RX_SHUTDOWN_RECEIVER
        .get()
        .expect("cannot get shutdown signal handler");
    rx.recv_async().await.ok();
    info!("Shutdown signal received.");
    IS_SHUTTING_DOWN.store(true, std::sync::atomic::Ordering::Relaxed);
    DownloadService::ongoing_waker().wake(); // Wake attached SSE listeners and disconnect them.
    DownloadService::stop_all();
}

pub async fn wait_for_download_completion() {
    let mut tries = Config::get().shutdown_max_loops;
    let mut sleeper = interval(Duration::from_millis(500));
    while tries > 0 {
        sleeper.tick().await;
        let ongoing = DownloadService::ongoing_download_count();
        if ongoing == 0 {
            break;
        }
        info!(
            "waiting for {} downloads to complete... ({} retries left) ",
            ongoing, tries
        );
        tries -= 1;
    }
    let ongoing = DownloadService::ongoing_download_count();
    if ongoing > 0 {
        warn!("{ongoing} downloads are still ongoing but we're out of time. This might lead to DB inconsistencies.");
    } else {
        info!("Clean shutdown: state has been flushed successfully. How will you live your life today?");
    }
}
