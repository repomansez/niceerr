use std::ops::Deref;
use std::task::Poll;

use async_zip::base::write::ZipFileWriter;
use async_zip::ZipEntryBuilder;
use camino::Utf8PathBuf;
use flume::{Receiver, Sender};
use futures_util::{AsyncWriteExt, FutureExt, Stream, StreamExt};
use log::{error, info, warn};
use niceerr_config::Config;
use niceerr_entity::download_session::{Model as DownloadSession, OutcomeKind};
use niceerr_entity::song::Model as Song;
use niceerr_entity::user::Model as User;
use tokio::io::{BufReader, DuplexStream};
use tokio::pin;
use tokio_util::compat::FuturesAsyncWriteCompatExt;
use tokio_util::io::read_buf;
use tracing::{Instrument, Span};

use crate::download::ty::{OngoingDownload, PrunnableSong};
use crate::slow_stream::SlowConsumerStreamWrapper;
use crate::{error::ApiError, limit_service::LimitService, ty::ApiResult};

// TODO: Implement some logic to detect slow readers and cut them off
pin_project_lite::pin_project! {
    pub(crate) struct ZipStreamer {
        // IMPORTANT: Don't change the DuplexStream to SimplexStream
        // SimplexStream is buggy and won't error out when downstream clients
        // disconnect. This will lead to resource starvation.
        #[pin]
        stream: DuplexStream,
        buff: Vec<u8>,
    }
}

impl Stream for ZipStreamer {
    type Item = ApiResult<Vec<u8>>;
    fn poll_next(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Option<Self::Item>> {
        let mut this = self.project();

        let mut buf = this.buff;
        let ret = {
            let polled = read_buf(&mut this.stream, &mut buf);
            pin!(polled);
            polled.poll_unpin(cx)
        };
        match ret {
            Poll::Pending => Poll::Pending,
            Poll::Ready(Ok(0)) => Poll::Ready(None),
            Poll::Ready(Ok(_)) => {
                // cloning is faster because u8 is copy, so it's just a plain memcpy vs allocating and
                // zeroing a new buffer
                let ret = Poll::Ready(Some(Ok(buf.clone())));
                buf.clear();
                ret
            }
            // User cancelled download before it was even finished
            Poll::Ready(Err(_)) => Poll::Ready(Some(Err(ApiError::InternalError))),
        }
    }
}

impl ZipStreamer {
    pub fn new(stream: DuplexStream) -> Self {
        let buff_size = Config::get().stream_download_buffer_byte_size;
        Self {
            stream,
            buff: Vec::with_capacity(buff_size),
        }
    }
}

impl PrunnableSong {
    pub fn without_prune(song: Song) -> Self {
        Self {
            song,
            song_pruner_tx: None,
        }
    }

    pub fn with_prune(song: Song, song_pruner_tx: Sender<Utf8PathBuf>) -> Self {
        Self {
            song,
            song_pruner_tx: Some(song_pruner_tx),
        }
    }
}

impl Deref for PrunnableSong {
    type Target = Song;

    fn deref(&self) -> &Self::Target {
        &self.song
    }
}

// Stream an existing download session as zip, and compress it in real time.
pub struct ZipDownload {
    download: DownloadSession,
    ignore_errors: bool,
}

impl ZipDownload {
    async fn check_missing_files(&self) -> ApiResult<()> {
        if self.ignore_errors {
            return Ok(());
        }
        let mut stream = self.download.songs_as_stream().await?;
        while let Some(song) = stream.next().await.transpose()? {
            if !song.is_completed() {
                continue;
            }
            match tokio::fs::try_exists(song.download_path).await {
                Ok(true) => (),
                _ => {
                    return Err(ApiError::BypassableError(
                        "one or more songs are missing".into(),
                    ))
                }
            }
        }
        Ok(())
    }

    pub fn new(download: DownloadSession, ignore_errors: bool) -> ApiResult<Self> {
        if download.outcome == OutcomeKind::InProgress {
            return Err(ApiError::InvalidRequest);
        }
        Ok(Self {
            download,
            ignore_errors,
        })
    }

    /// Zipping task. Songs are received from a channel and then their contents are
    /// read and sent to the client through a duplex "File" instance.
    async fn zipper_task(
        mut pipe: DuplexStream,
        receiver: Receiver<PrunnableSong>,
        prefix: String,
    ) -> ApiResult<()> {
        info!("Stripping prefix: {prefix}");

        let mut writer = ZipFileWriter::with_tokio(&mut pipe);

        while let Ok(song) = receiver.recv_async().await {
            if !song.is_completed() {
                continue;
            }
            let file = match tokio::fs::File::open(&song.download_path).await {
                Ok(file) => file,
                Err(e) => {
                    warn!("failed to open file at {}: {e:?}", song.download_path);
                    continue;
                }
            };
            let mut buf_reader = BufReader::new(file);
            let filename = match song.download_path.strip_prefix(&prefix) {
                Some(path) => path,
                None => &song.download_path,
            };
            let builder =
                ZipEntryBuilder::new(filename.into(), async_zip::Compression::Stored).build();
            let mut entry_writer = writer.write_entry_stream(builder).await?.compat_write();
            tokio::io::copy_buf(&mut buf_reader, &mut entry_writer)
                .await
                .map_err(|_| ApiError::InternalError)?;
            entry_writer.into_inner().close().await?;
        }
        writer.inner_mut().flush().await?;
        writer.close().await?;
        Ok(())
    }

    /// Song sender task. This task exclusively caches up entries from database,
    /// and then sends them to the zipper task.
    async fn song_sender_task(
        download: DownloadSession,
        sender: Sender<PrunnableSong>,
    ) -> ApiResult<()> {
        let mut stream = download.songs_as_stream().await?;
        while let Some(song) = stream.next().await.transpose()? {
            sender
                .send_async(PrunnableSong::without_prune(song))
                .await
                .map_err(|_| ApiError::InternalError)?;
        }
        Ok(())
    }

    /// Create a new stream of albums from a live download session.
    ///
    /// This method is used to perform a direct stream from the upstream
    /// service to the client. If the downstream client cancels the download,
    /// the
    pub fn new_stream_live(
        session: &OngoingDownload,
        receiver: Receiver<PrunnableSong>,
    ) -> SlowConsumerStreamWrapper<ZipStreamer> {
        let prefix = session.root_path.to_string();
        let buff_size = Config::get().stream_download_buffer_byte_size;
        let (readhalf, writehalf) = tokio::io::duplex(buff_size);
        let cancel_token = session.cancel_token.clone();

        let fut = async move {
            match Self::zipper_task(writehalf, receiver, prefix).await {
                Ok(_) => info!("zipper (stream) task finished successffully"),
                Err(e) => {
                    error!("zipper (stream) task failed: {e:?}");
                    cancel_token.cancel();
                }
            };
        }
        .instrument(Span::current());
        tokio::spawn(fut);
        SlowConsumerStreamWrapper::new(ZipStreamer::new(readhalf))
    }

    /// Create a new zip stream from files that are already downloaded.
    ///
    /// This basically just streams existing files from storage to the user.
    pub async fn stream_from_storage(
        self,
        user: &User,
    ) -> ApiResult<SlowConsumerStreamWrapper<ZipStreamer>> {
        let permit = LimitService::get().try_new_permit(&user.id)?;
        self.check_missing_files().await?;
        let buff_size = Config::get().stream_download_buffer_byte_size;
        let prefix = OngoingDownload::build_root_prefix(user, &self.download, false).to_string();
        info!("zip stream: {} buff size={}", self.download.id, buff_size);
        let (readhalf, writehalf) = tokio::io::duplex(buff_size);
        let (tx, rx) = flume::bounded(0);
        tokio::spawn(Self::song_sender_task(self.download, tx));
        let fut = async move {
            let _permit = permit;
            match Self::zipper_task(writehalf, rx, prefix).await {
                Ok(_) => info!("zipper (storage) task finished successfully"),
                Err(e) => error!("zipper (storage) task failed: {e:?}"),
            };
        }
        .instrument(Span::current());
        tokio::spawn(fut);
        Ok(SlowConsumerStreamWrapper::new(ZipStreamer::new(readhalf)))
    }
}

impl Drop for PrunnableSong {
    fn drop(&mut self) {
        let Some(tx) = &self.song_pruner_tx else {
            return;
        };
        if !self.is_completed() {
            return;
        }
        tx.try_send(Utf8PathBuf::from(self.download_path.clone()))
            .inspect_err(|e| {
                error!(
                    "Delete channel: couldn't send song: {e:?} ({})",
                    self.download_path
                );
            })
            .ok();
    }
}
