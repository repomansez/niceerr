use axum::{
    extract::Path,
    routing::{get, post},
    Extension, Router,
};
use log::info;
use niceerr_entity::{arl::Model as Arl, user::Model as UserModel};
use reqwest::StatusCode;
use serde::Deserialize;

use crate::{
    admin_console::{self, arl::ArlPriorityUpdate},
    middleware::require_admin_console_perm,
    scheduled_tracker::{ScheduledTaskName, ScheduledTaskTracker, SerializedScheduleState},
    ty::{Json, JsonResult},
};

async fn reset_arl_indexes(
    Extension(_caller): Extension<UserModel>,
) -> JsonResult<ArlPriorityUpdate> {
    ScheduledTaskTracker::check_arl_running()?;
    let resp = admin_console::arl::reset_arl_priorities().await?;
    Ok((StatusCode::OK, axum::Json(resp)))
}

#[derive(Debug, Deserialize)]
pub struct CountryHolder {
    countries: Vec<String>,
}

async fn existing_arl_countries(
    Extension(_caller): Extension<UserModel>,
) -> JsonResult<Vec<String>> {
    let countries = Arl::unique_countries().await?;
    Ok((StatusCode::OK, axum::Json(countries)))
}

async fn reprioritize_by_countries(
    Extension(_caller): Extension<UserModel>,
    Json(payload): Json<CountryHolder>,
) -> JsonResult<ArlPriorityUpdate> {
    ScheduledTaskTracker::check_arl_running()?;
    let resp = admin_console::arl::reprioritize_by_countries(payload.countries).await?;
    Ok((StatusCode::OK, axum::Json(resp)))
}

async fn scheduler_state(
    Extension(_caller): Extension<UserModel>,
) -> JsonResult<SerializedScheduleState> {
    let state = ScheduledTaskTracker::serialize_state();
    Ok((StatusCode::OK, axum::Json(state)))
}

async fn scheduler_run(
    Extension(_caller): Extension<UserModel>,
    Path(task): Path<ScheduledTaskName>,
) -> JsonResult<SerializedScheduleState> {
    info!("Manual scheduler run: executing {task:?}");
    ScheduledTaskTracker::run_task(task).await?;
    let state = ScheduledTaskTracker::serialize_state();
    Ok((StatusCode::OK, axum::Json(state)))
}

pub fn admin_console_router() -> Router {
    Router::new()
        .route("/console/schedule/state", get(scheduler_state))
        .route("/console/schedule/:task", post(scheduler_run))
        .route("/console/arl/reset-priorities", post(reset_arl_indexes))
        .route(
            "/console/arl/reprioritize-by-country",
            post(reprioritize_by_countries),
        )
        .route(
            "/console/arl/available-countries",
            get(existing_arl_countries),
        )
        .route_layer(axum::middleware::from_fn(require_admin_console_perm))
}
