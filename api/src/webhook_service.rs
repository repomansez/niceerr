use std::sync::OnceLock;

use flume::{Receiver, Sender};
use log::{info, warn};
use niceerr_config::Config;
use niceerr_entity::song::Model as DbSong;
use reqwest::Client;

use crate::arl_service::ArlService;

static SERVICE: OnceLock<WebhookService> = OnceLock::new();

#[derive(Debug)]
pub struct WebhookService {
    song_tx: Sender<DbSong>,
}

impl WebhookService {
    pub fn init() {
        if Config::get().webhook_url.is_none() {
            info!("Webhook: disabled");
            return;
        }
        let client = ArlService::builder()
            .user_agent("niceerr")
            .build()
            .expect("Cannot build webhook HTTP client");
        let (song_tx, rx) = flume::bounded(1);
        let this = WebhookService { song_tx };
        SERVICE.set(this).expect("Cannot set webhook service");
        tokio::spawn(Self::send_song_task(client, rx));
        info!("Webhook service started");
    }

    async fn send_song_task(client: Client, rx: Receiver<DbSong>) {
        let url = Config::get()
            .webhook_url
            .as_ref()
            .expect("Webhook URL isn't set?");

        while let Ok(song) = rx.recv_async().await {
            if let Err(e) = client
                .post(url.to_string())
                .json(&song)
                .send()
                .await
                .map(reqwest::Response::error_for_status)
            {
                warn!("Webhook: downstream consumer returned an error: {e:?}");
            }
        }
    }

    pub fn send(song: DbSong) {
        if let Some(s) = SERVICE.get() {
            s.song_tx
                .try_send(song)
                .inspect_err(|e| {
                    warn!("Webhook: consumer is too slow, discarding event: {e:?}");
                })
                .ok();
        }
    }
}
