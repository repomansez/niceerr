use std::{fmt::Display, sync::LazyLock};

use axum::{response::IntoResponse, routing::get, Extension, Router};
use chrono::{DateTime, Utc};
use log::error;

use niceerr_config::Config;
use niceerr_db::DBConfig;
use niceerr_entity::{
    assigned_permission::Permission, song::Model as Song, song_impl::SongStats,
    user::Model as UserModel,
};
use reqwest::StatusCode;
use serde::Serialize;
use tokio::{
    fs::File,
    io::{self, AsyncReadExt},
    runtime::Handle,
};

use crate::{
    arl_service::ArlService,
    clean_tmp::TmpFolder,
    client_relay_service::RelayClientService,
    download::ty::DownloadService,
    limit_service::LimitService,
    middleware,
    ongoing_sse::sse_ongoing_download_subscribers,
    ty::{ApiResult, JsonResult},
};

pub static VERSION: &str = "1.66.1";

/// The date this server was started at
static STARTED_AT: LazyLock<chrono::DateTime<Utc>> = LazyLock::new(Utc::now);

#[derive(Serialize, Default, Debug)]
#[serde(rename_all = "camelCase")]
pub struct SystemInfo {
    request_queue_size: usize,
    request_queue_capacity: usize,
    update_dl_queue_size: usize,
    update_dl_queue_capacity: usize,
    song_to_db_queue_size: usize,
    song_to_db_queue_capacity: usize,
    db_pool_connections: u32,
    db_idle_connections: usize,
    max_grants_per_user: u8,
    streaming_download_workers: u8,
    server_side_download_workers: u8,
    limit_service_user_count: usize,
    limit_service_granted_permits: usize,
    song_stats: SongStats,
    rss_memory_usage_bytes: usize,
    streaming_download_queue_size: u8,
    stream_download_buffer_byte_size: usize,
    hot_arl_cache_size: usize,
    active_worker_count: usize,
    server_version: String,
    tmp_folder_size: Option<u64>,
    alive_task_count: usize,
    thread_count: usize,
    sse_ongoing_download_subscribers: usize,
    started_at: DateTime<Utc>,
    relay_configured: bool,
    has_valid_relay_token: bool,
    rust_version: String,
}

// Dereference STARTED_AT to invoke the lazy constructor
pub fn init_uptime_tracker() {
    let _startup = *STARTED_AT;
}

async fn get_memory_usage_res() -> io::Result<usize> {
    // Open the /proc/self/statm file
    let mut file = File::open("/proc/self/statm").await?;

    // Read the content of the file into a string
    let mut contents = String::new();
    file.read_to_string(&mut contents).await?;

    // Split the content by whitespace and parse the second field as usize
    let parts: Vec<&str> = contents.split_whitespace().collect();
    if parts.len() > 1 {
        // The second field is the resident set size in pages
        let rss_pages: usize = parts[1].parse().unwrap_or(0);

        // Convert pages to bytes
        let rss_bytes = rss_pages * page_size::get();

        Ok(rss_bytes)
    } else {
        Err(io::Error::new(
            io::ErrorKind::Other,
            "Failed to read memory usage",
        ))
    }
}

async fn generate_info() -> ApiResult<SystemInfo> {
    let mut info = SystemInfo {
        song_stats: Song::all_time_stats().await?,
        ..Default::default()
    };
    let download_service = DownloadService::get_service();
    let token_service = ArlService::get();
    let (user_count, granted_permits) = LimitService::stats();
    let config = Config::get();
    let metrics = Handle::current().metrics();

    info.request_queue_size = download_service.request_tx.len();
    info.request_queue_capacity = download_service.request_tx.queue_capacity();
    info.update_dl_queue_size = download_service.update_dl_session_tx.len();
    info.update_dl_queue_capacity = download_service.update_dl_session_tx.queue_capacity();
    info.song_to_db_queue_size = download_service.song_to_db_tx.len();
    info.song_to_db_queue_capacity = download_service.song_to_db_tx.queue_capacity();

    let db = DBConfig::get_connection();
    let pool = db.get_postgres_connection_pool();
    info.db_pool_connections = pool.size();
    info.db_idle_connections = pool.num_idle();

    info.server_side_download_workers = config.server_side_download_workers;
    info.max_grants_per_user = config.max_grants_per_user;
    info.streaming_download_workers = config.streaming_download_workers;
    info.streaming_download_queue_size = config.streaming_download_queue_size;
    info.stream_download_buffer_byte_size = config.stream_download_buffer_byte_size;
    info.limit_service_user_count = user_count;
    info.limit_service_granted_permits = granted_permits;
    info.active_worker_count = DownloadService::active_worker_count();
    info.hot_arl_cache_size = token_service.get_cache_size();
    info.server_version = VERSION.to_string();
    info.tmp_folder_size = TmpFolder::get_size().await.ok();
    info.sse_ongoing_download_subscribers = sse_ongoing_download_subscribers();
    info.rss_memory_usage_bytes = get_memory_usage_res().await.unwrap_or_else(|e| {
        error!("Failed to get memory usage: {e:?}");
        0
    });
    info.alive_task_count = metrics.num_alive_tasks();
    info.thread_count = metrics.num_workers();
    info.started_at = *STARTED_AT;
    info.relay_configured = Config::get().relay_url.is_some();
    info.has_valid_relay_token = RelayClientService::is_available();
    let rust_info = rustc_version_runtime::version_meta();
    info.rust_version = format!(
        "rustc={}, llvm={}",
        rust_info.semver,
        rust_info
            .llvm_version
            .map(|v| v.to_string())
            .unwrap_or_default()
    );
    Ok(info)
}

struct PrometheusResponse {
    buff: String,
}

enum PrometheusMetricType {
    Counter,
    Gauge,
}

impl Display for PrometheusMetricType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            PrometheusMetricType::Counter => write!(f, "counter"),
            PrometheusMetricType::Gauge => write!(f, "gauge"),
        }
    }
}

impl PrometheusResponse {
    fn new() -> Self {
        Self {
            buff: String::new(),
        }
    }

    fn add_gauge<D: Display, D2: Display, D3: Display>(self, name: D, value: D2, help: D3) -> Self {
        self.add_field(PrometheusMetricType::Gauge, name, value, help)
    }

    fn add_counter<D: Display, D2: Display, D3: Display>(
        self,
        name: D,
        value: D2,
        help: D3,
    ) -> Self {
        self.add_field(PrometheusMetricType::Counter, name, value, help)
    }

    fn add_field<D: Display, D2: Display, D3: Display>(
        mut self,
        mtype: PrometheusMetricType,
        name: D,
        value: D2,
        help: D3,
    ) -> Self {
        let line = format!(
            r#"# HELP {name}: {help}
# TYPE {name} {mtype}
{name} {value}"#
        );
        self.buff.push_str(&line);
        self.buff.push('\n');
        self
    }

    fn into_inner(self) -> String {
        self.buff
    }
}

async fn get_info_prometheus() -> ApiResult<impl IntoResponse> {
    let info = generate_info().await?;
    let stats = PrometheusResponse::new()
        .add_gauge(
            "active_worker_count",
            info.active_worker_count,
            "number of active workers",
        )
        .add_gauge(
            "alive_task_count",
            info.alive_task_count,
            "number of alive tasks",
        )
        .add_gauge(
            "rss_memory_usage_bytes",
            info.rss_memory_usage_bytes,
            "rss memory used by niceerr",
        )
        .add_gauge(
            "tmp_folder_size",
            info.tmp_folder_size.unwrap_or(0),
            "tmp folder size in bytes",
        )
        .add_gauge(
            "request_queue_size",
            info.request_queue_size,
            "size of the request/downloads queue",
        )
        .add_gauge(
            "db_pool_connections",
            info.db_pool_connections,
            "DB total connections, including idle",
        )
        .add_gauge(
            "db_idle_connections",
            info.db_idle_connections,
            "DB idle connections",
        )
        .add_gauge(
            "limit_service_granted_permits",
            info.limit_service_granted_permits,
            "granted permits/active ongoing downloads",
        )
        .add_gauge(
            "limit_service_user_count",
            info.limit_service_user_count,
            "number of users with at least 1 active download",
        )
        .add_gauge(
            "hot_arl_cache_size",
            info.hot_arl_cache_size,
            "size of the hot arl cache",
        )
        .add_gauge(
            "sse_ongoing_download_subscribers",
            info.sse_ongoing_download_subscribers,
            "active SSE subscribers (ongoing downloads)",
        )
        .add_counter(
            "song_stats_downloaded_bytes",
            info.song_stats.downloaded_bytes,
            "effective downloaded bytes",
        )
        .add_counter(
            "song_stats_total_bytes",
            info.song_stats.total_bytes,
            "total discovered bytes",
        )
        .add_counter(
            "song_stats_total_count",
            info.song_stats.total_count,
            "total songs downloaded, including failed downloads",
        )
        .add_counter(
            "song_stats_error_count",
            info.song_stats.error_count,
            "errored downloads song count",
        )
        .add_counter(
            "song_stats_session_count",
            info.song_stats.session_count,
            "download session count",
        );

    Ok(stats.into_inner().into_response())
}

async fn get_info(Extension(caller): Extension<UserModel>) -> JsonResult<SystemInfo> {
    caller.verify_has_permission(&Permission::ReadServerStats)?;
    let info = generate_info().await?;
    Ok((StatusCode::OK, axum::Json(info)))
}

pub fn prometheus_router() -> Router {
    Router::new()
        .route("/api/metrics", get(get_info_prometheus))
        .layer(axum::middleware::from_fn(middleware::prometheus_basic_auth))
}

pub fn info_router() -> Router {
    Router::new().route("/info", get(get_info))
}
