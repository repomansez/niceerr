use std::{
    sync::{Arc, OnceLock, RwLock},
    time::Duration,
};

use anyhow::bail;
use axum::body::Bytes;
use futures_util::{Stream, TryFutureExt};
use log::{debug, error, warn};
use niceerr_config::Config;
use niceerr_dz::SongStream;
use niceerr_entity::assigned_permission::{self, Permission};
use serde_json::json;
use tokio::time::interval;
use tokio_util::sync::CancellationToken;

use crate::{
    arl_service::ArlService,
    backoff::EBCancellableRunner,
    download::ty::OngoingDownload,
    error::{ApiError, TokenErrorReason, WrappedDownloadError},
    info,
    scheduled_tracker::{ScheduledTaskName, ScheduledTaskTracker},
    server_relay_service::{FallibleRelayResponse, RelayRequest, RelayResponse},
    ty::ApiResult,
};

#[derive(Debug)]
pub struct RelayClientService {
    client: reqwest::Client,
    username: String,
    password: String,
    url: String,
    token: RwLock<Option<Arc<String>>>,
}

static CLIENT_RELAY: OnceLock<RelayClientService> = OnceLock::new();

impl RelayClientService {
    pub fn init() {
        if CLIENT_RELAY.get().is_some() {
            return;
        }
        let Some(url) = Config::get().relay_url.as_ref() else {
            return;
        };
        let username = url.username().trim().into();
        let password = url
            .password()
            .expect("missing check for password?")
            .trim()
            .to_string();

        let client = ArlService::builder();
        let mut url = url
            .clone()
            .to_string()
            .replace(&format!("{username}:{password}@"), "")
            .to_string();
        if url.ends_with("/") {
            url.pop();
        }

        let svc = RelayClientService {
            client: client.build().expect("Can't create http client"),
            username,
            password,
            url,
            token: RwLock::new(None),
        };
        CLIENT_RELAY
            .set(svc)
            .expect("can't set client relay service");
        Self::run_long_task();
        info!("Relay client service initialized.");
    }

    pub fn is_available() -> bool {
        CLIENT_RELAY.get().is_some_and(|s| s.get_token().is_ok())
    }

    pub fn get() -> &'static RelayClientService {
        CLIENT_RELAY
            .get()
            .expect("client relay service not initialized")
    }

    fn run_long_task() {
        tokio::spawn(async move {
            let mut sleep = interval(Duration::from_secs(
                Config::get().relay_client_refresh_sec as u64,
            ));
            loop {
                sleep.tick().await;
                let sself = Self::get();
                match sself.update_token().await {
                    Ok(_) => info!("successfully updated token"),
                    Err(e) => error!("can't update token: {e:?}"),
                }
            }
        });
    }

    pub async fn get_stream_with_data(
        id: u64,
        session: &OngoingDownload,
    ) -> Result<
        (
            SongStream<impl Stream<Item = Result<Bytes, reqwest::Error>>>,
            RelayResponse,
        ),
        WrappedDownloadError,
    > {
        let this = Self::get();
        let token = this.get_token()?;
        let client = this.client.clone();
        let request = RelayRequest::from_ongoing_download(id, session);
        let url = format!("{}/api/download/relayed", this.url);
        debug!("requesting relayed stream at {url}");
        let response = client
            .post(&url)
            .json(&request)
            .bearer_auth(token)
            .send()
            .await
            .map_err(|e| {
                warn!("can't send request: {e:?}");
                WrappedDownloadError::RetriableRelay(e.to_string())
            })?
            .json::<FallibleRelayResponse>()
            .await
            .map_err(|e| {
                warn!("can't parse response: {e:?}");
                WrappedDownloadError::RetriableRelay(e.to_string())
            })?;
        let response = match response {
            FallibleRelayResponse::Failure(r) => {
                return Err(WrappedDownloadError::UnretriableRelay(r.error))
            }
            FallibleRelayResponse::Success(r) => r,
        };
        let stream = client
            .get(&response.download_url)
            .send()
            .await
            .map_err(|e| {
                warn!("can't send CDN request: {e:?}");
                WrappedDownloadError::RetriableRelay(e.to_string())
            })?
            .bytes_stream();

        let stream =
            niceerr_dz::types::Song::decrypt_reqwest_stream(stream, response.key_hex.clone())?;
        Ok((stream, response))
    }

    fn get_token(&self) -> Result<Arc<String>, WrappedDownloadError> {
        let lock = self.token.read().expect("Can't unlock mutex");
        let Some(t) = lock.as_ref() else {
            return Err(WrappedDownloadError::UnretriableRelay(
                "Relay client service doesn't have a valid token".into(),
            ));
        };
        Ok(t.clone())
    }

    fn set_token(&self, token: Option<Arc<String>>) {
        *self.token.write().expect("can't unlock mutex for write") = token;
    }

    pub async fn update_token(&self) -> ApiResult<()> {
        let _guard = ScheduledTaskTracker::try_guard(ScheduledTaskName::RelayTokenRefresh)?;
        let factory = || {
            self.try_get_auth().map_err(|e| {
                error!("Can't get auth payload: {e}");
                self.set_token(None);
                ApiError::TokenError(TokenErrorReason::InvalidRelayCredentials)
            })
        };
        let cancel_token = CancellationToken::new();
        let mut runner = EBCancellableRunner::new(factory, &cancel_token);
        let token = Some(Arc::new(runner.call().await?));
        self.set_token(token);
        Ok(())
    }

    async fn try_get_auth(&self) -> anyhow::Result<String> {
        match self.get_refresh_auth_payload().await {
            Ok(t) => Ok(t),
            Err(e) => {
                warn!(
                    "Couldn't invoke quick refresh endpoint: {e:?}. Falling back to normal auth."
                );
                self.get_auth_payload().await
            }
        }
    }

    async fn get_refresh_auth_payload(&self) -> anyhow::Result<String> {
        let resp = self
            .client
            .post(format!("{}/api/user/refresh", self.url))
            .bearer_auth(self.get_token()?)
            .send()
            .await?
            .error_for_status()?
            .json::<serde_json::Value>()
            .await?;
        if let Some(s) = resp["token"].as_str() {
            Ok(s.to_string())
        } else {
            bail!("Invalid response from relay")
        }
    }

    async fn get_auth_payload(&self) -> anyhow::Result<String> {
        let auth_payload = json!({
            "username": self.username,
            "password": self.password,
        });
        let resp = self
            .client
            .post(format!("{}/api/user/login", self.url))
            .json(&auth_payload)
            .send()
            .await?
            .error_for_status()?
            .json::<serde_json::Value>()
            .await?;
        let superuser = resp["user"]["superuser"].as_bool().unwrap_or(false);
        let perms = resp["user"]["permissions"]
            .as_array()
            .unwrap_or(&vec![])
            .iter()
            .map(|v| serde_json::from_value::<assigned_permission::Model>(v.to_owned()))
            .collect::<Result<Vec<_>, _>>()?;

        if !superuser && !perms.iter().any(|p| p.name == Permission::RelayAccess) {
            bail!("User doesn't have permission to access relay");
        }

        if let Some(s) = resp["token"].as_str() {
            Ok(s.to_string())
        } else {
            bail!("Invalid response from relay")
        }
    }
}
