use std::{
    ops::Deref,
    pin::Pin,
    sync::{atomic::Ordering, Arc},
};

use axum::body::Bytes;
use camino::Utf8PathBuf;
use futures_util::{Stream, StreamExt};
use log::{error, info, warn};
use niceerr_config::Config;
use niceerr_dz::{
    client::DzClient,
    error::{DzError, DzResult},
    types::{Album, IncompleteSong, Song, StreamFormat},
};
use niceerr_entity::song::Model as DbSong;
use sanitize_filename::sanitize;
use tokio::io::{AsyncWriteExt, BufWriter};
use tokio_util::sync::CancellationToken;

use crate::{
    arl_service::ArlService,
    backoff::{EBCancellableRunner, ExecutionResult, ExponentialBackoffCancellableMut},
    client_relay_service::RelayClientService,
    download::ty::OngoingDownload,
    error::WrappedDownloadError,
    server_relay_service::RelayResponse,
    ty::ApiResult,
};

use super::album::WrappedAlbum;

/// Wrapper for both dz_song (a dz song object)
/// and a database song object. This makes things easier when
/// it comes to saving results to database and handling the entire
/// download process.
pub struct SongDownloader<'a> {
    db_model: &'a mut DbSong,
    pub(crate) song: niceerr_dz::types::Song,
    pub(crate) bytes_downloaded: usize,
    pub(crate) album: Arc<WrappedAlbum>,
    pub(crate) session: OngoingDownload,
    fallback_stream_format: Option<StreamFormat>,
    client: DzClient,
    use_local_client: bool,
    relay_data_set: bool,
}

impl Deref for SongDownloader<'_> {
    type Target = IncompleteSong;

    fn deref(&self) -> &Self::Target {
        &self.song
    }
}

impl<'a> SongDownloader<'a> {
    pub async fn new(
        album: Arc<WrappedAlbum>,
        session: &OngoingDownload,
        db_model: &'a mut DbSong,
    ) -> ApiResult<Self> {
        let (song, client) =
            Self::fetch_song_with_client(db_model.item_id as u64, &session.cancel_token)
                .await
                .inspect_err(|e| {
                    db_model.error_message = Some(e.to_string());
                    session.increase_error_count();
                    warn!("Failed to fetch full song: {}", e);
                })?;
        let use_local_client = client
            .can_stream_at_format(&song, &session.stream_format())
            .is_ok()
            || !RelayClientService::is_available();
        Ok(Self {
            bytes_downloaded: 0,
            fallback_stream_format: None,
            db_model,
            session: session.clone(),
            album,
            song,
            client,
            use_local_client,
            relay_data_set: false,
        })
    }

    pub fn set_error_message<S: AsRef<str>>(&mut self, message: S) {
        self.db_model.error_message = Some(message.as_ref().to_string());
    }

    async fn try_remove(&self) {
        let path = Utf8PathBuf::from(&self.db_model.download_path);
        tokio::fs::remove_file(path).await.ok();
    }

    /// Whether this song needs a download fallback to be applied. This is always
    /// skipped if:
    ///
    /// - Quality fallback wasn't enabled by the user
    /// - We have a premium ARL that can download at the requested stream format
    /// - The server is configured to use a "relay" to stream stuff
    fn check_needs_fallback(&mut self) {
        if !self.session.download_session.allow_fallback || !self.use_local_client {
            return;
        }

        let stream_format = self.session.stream_format();

        if !self.is_stream_format_available(&stream_format) {
            if let Some(f) = self.fallback_format(&stream_format) {
                warn!("Target stream format unavailable; falling back to {f:?}");
                self.fallback_stream_format = Some(f);
                self.session
                    .total_bytes
                    .fetch_add(self.byte_size(&f), Ordering::Relaxed);
            }
        }
    }

    fn stream_format(&self) -> StreamFormat {
        self.fallback_stream_format
            .unwrap_or(self.session.stream_format())
    }

    /// Fetch the song, but make the whole thing async-cancellable
    async fn fetch_song_with_client(
        item_id: u64,
        cancel_token: &CancellationToken,
    ) -> ApiResult<(Song, DzClient)> {
        let arlsv = ArlService::get();
        EBCancellableRunner::new(|| arlsv.fetch_song_with_client_by_id(item_id), cancel_token)
            .call()
            .await
    }

    /// Infallible download function. If there are any errors, we set the model's error status
    /// and return early.
    pub async fn download(&mut self) {
        self.check_needs_fallback();
        let path = self
            .session
            .root_path
            .join(self.album.song_path(self, self.stream_format()));
        self.db_model.download_path = path.to_string();
        if self.use_local_client {
            self.db_model.total_bytes = self.byte_size(&self.stream_format()) as i32;
        }

        // Create a runner to drive the download. If this download is cancelled,
        // the whole thing (network requests, I/O, etc) will be cancelled too.
        let mut runner =
            ExponentialBackoffCancellableMut::new(self, self.session.cancel_token.clone());
        let mut last_error = String::new();
        loop {
            let result = match runner.call(|s| s.try_download()).await {
                ExecutionResult::Cancelled => {
                    self.try_remove().await;
                    break;
                }
                ExecutionResult::TimedOut => {
                    last_error = format!(
                        "Couldn't download after {} retries: {}",
                        runner.iter_count, last_error
                    );
                    self.handle_download_error(last_error);
                    self.try_remove().await;
                    break;
                }
                ExecutionResult::Completed(result) => result,
            };

            match result {
                Err(WrappedDownloadError::Dz(DzError::Unavailable(msg))) => {
                    self.handle_download_error(format!("Unavailable: {msg}"));
                    self.try_remove().await;
                    break;
                }
                Err(WrappedDownloadError::IO(msg)) => {
                    error!("I/O error: {msg}");
                    self.handle_download_error(msg);
                    self.try_remove().await;
                    break;
                }
                Err(WrappedDownloadError::UnretriableRelay(msg)) => {
                    warn!("Relay failure: {msg}");
                    self.handle_download_error(msg);
                    self.try_remove().await;
                    break;
                }
                Err(e) => {
                    last_error = e.to_string();
                    warn!(
                        "try {}/{} {:?}: retrying failed download: {e:?}",
                        runner.iter_count,
                        Config::get().max_retries,
                        path
                    );
                }
                Ok(_) => {
                    break;
                }
            };
        }
        self.db_model.downloaded_bytes = self.bytes_downloaded as i32;
    }

    fn handle_download_error<S: AsRef<str>>(&mut self, message: S) {
        let message = message.as_ref();
        self.set_error_message(message);
        warn!("{}", message);
        self.session.increase_error_count();
    }

    fn set_relay_data(&mut self, data: RelayResponse) {
        if self.relay_data_set {
            return;
        }
        self.db_model.total_bytes = data.size as i32;
        self.session
            .total_bytes
            .fetch_add(data.size, Ordering::Relaxed);
        self.relay_data_set = true;
        if data.stream_format != self.stream_format() {
            self.fallback_stream_format = Some(data.stream_format);
            let mut path = Utf8PathBuf::from(&self.db_model.download_path);
            path.set_extension(self.stream_format().extension());
            self.db_model.download_path = path.to_string();
        }

        match self.stream_format() {
            StreamFormat::Flac => self.song.filesize_flac = data.size,
            StreamFormat::Mp3_320 => self.song.filesize_mp3_320 = data.size,
            StreamFormat::Mp3_128 => self.song.filesize_mp3_128 = data.size,
        }
    }

    // dyn dispatch the downloader for local or relayed sources
    async fn get_stream(
        &mut self,
    ) -> Result<Pin<Box<dyn Stream<Item = DzResult<Bytes>> + Send>>, WrappedDownloadError> {
        if self.use_local_client {
            let stream = self
                .song
                .stream(&self.client, &self.stream_format())
                .await?;
            Ok(Box::pin(stream))
        } else {
            let (stream, response) =
                RelayClientService::get_stream_with_data(self.song.id, &self.session).await?;
            self.set_relay_data(response);
            Ok(Box::pin(stream))
        }
    }

    async fn try_download(&mut self) -> Result<(), WrappedDownloadError> {
        // If this download had previously failed, then we'll simply reset its counter to 0, because we've
        // downloaded 0 "valid" bytes.
        self.bytes_downloaded = 0;
        let stream = self.get_stream().await?;
        let path = Utf8PathBuf::from(&self.db_model.download_path);
        if let Some(parent) = path.parent() {
            tokio::fs::create_dir_all(parent)
                .await
                .map_err(|e| WrappedDownloadError::IO(e.to_string()))?;
        } else {
            return Err(WrappedDownloadError::IO(
                "Couldn't get parent directory".into(),
            ));
        }

        let handle = tokio::fs::OpenOptions::new()
            .write(true)
            .read(true)
            .create(true)
            .truncate(true)
            .open(&path)
            .await
            .map_err(|e| WrappedDownloadError::IO(e.to_string()))?;
        tokio::pin!(stream);
        info!("Saving to {path:?}");

        let mut buf_writer = BufWriter::new(handle);
        while let Some(bytes) = stream.next().await {
            let bytes = bytes.inspect_err(|_| {
                // If we failed to get more bytes from the stream, then this means whatever we
                // got is incomplete garbage. In this case, we'll set the byte counters for this
                // individual song (as these are saved to database) and substract whatever
                // we added to the global counter (dl_bytes_so_far).
                self.reset_bytes_downloaded();
            })?;

            let written = buf_writer
                .write(&bytes)
                .await
                .map_err(|e| WrappedDownloadError::IO(e.to_string()))?;
            self.bytes_downloaded += written;
            self.session
                .dl_bytes_so_far
                .fetch_add(written, Ordering::Relaxed);
        }
        let expected_file_size = self.byte_size(&self.stream_format());
        if self.bytes_downloaded != expected_file_size {
            warn!(
                "filesize mismatch: expected {} but got {} bytes",
                expected_file_size, self.bytes_downloaded
            );
        }
        // flush buffered writer to prevent tags from corrupting
        buf_writer
            .flush()
            .await
            .map_err(|e| WrappedDownloadError::IO(e.to_string()))?;

        let file = buf_writer.into_inner().into_std().await;
        self.song
            .file_apply_tags(
                file,
                &self.album,
                self.album.cover.clone(),
                self.album.extra_tags.clone(),
            )
            .await?;
        info!(
            "FINISHED ({}/{})",
            expected_file_size, self.bytes_downloaded
        );
        Ok(())
    }

    fn reset_bytes_downloaded(&mut self) {
        self.session
            .dl_bytes_so_far
            .fetch_sub(self.bytes_downloaded, Ordering::Relaxed);
        self.bytes_downloaded = 0;
    }
}

pub trait NameSchema {
    fn artist_name(&self) -> String;
    fn name(&self) -> String;
}

impl NameSchema for Album {
    fn artist_name(&self) -> String {
        let mut artist_name_schema = niceerr_config::Config::get()
            .artist_name_schema
            .clone()
            .contains_n_replace("$ARTIST_ID", || self.artist_id.to_string())
            .contains_n_replace("$ARTIST_NAME", || self.artist_name.to_owned())
            .contains_n_replace("$LABEL", || self.label.to_owned().unwrap_or_default());

        if artist_name_schema.trim().is_empty() {
            artist_name_schema = "INVALID_ARTIST_NAME_SCHEMA".into()
        }
        sanitize(artist_name_schema.trim())
    }

    fn name(&self) -> String {
        let album_name_schema = niceerr_config::Config::get()
            .album_name_schema
            .clone()
            .contains_n_replace("$ALBUM_ID", || self.id.to_string())
            .contains_n_replace("$ALBUM_NAME", || self.name.to_owned())
            .contains_n_replace("$ARTIST_ID", || self.artist_id.to_string())
            .contains_n_replace("$ARTIST_NAME", || self.artist_name.to_string())
            .contains_n_replace("$LABEL", || self.label.to_owned().unwrap_or_default())
            .contains_n_replace("$RELEASE_DATE", || self.release_date().unwrap_or_default());
        if album_name_schema.trim().is_empty() {
            return "INVALID_ALBUM_NAME_SCHEMA".into();
        }

        sanitize(album_name_schema.trim())
    }
}

impl NameSchema for IncompleteSong {
    fn artist_name(&self) -> String {
        unimplemented!("get_artist_name() not implemented for Song");
    }
    fn name(&self) -> String {
        let song_name_schema = niceerr_config::Config::get()
            .song_name_schema
            .clone()
            .contains_n_replace("$ID", || self.id.to_string())
            .contains_n_replace("$TRACK_NAME", || self.title.clone())
            .contains_n_replace("$ARTIST_ID", || self.artist_id.to_string())
            .contains_n_replace("$ARTIST_NAME", || self.artist_name.to_owned())
            .contains_n_replace("$ALBUM_NAME", || self.album_name.to_owned())
            .contains_n_replace("$ALBUM_ID", || self.album_id.to_string())
            .contains_n_replace("$DISK_NUMBER", || self.disk_number.to_string())
            .contains_n_replace("$TRACK_NUMBER", || self.track_number.to_string())
            .contains_n_replace("$ISRC", || self.isrc.to_owned())
            .contains_n_replace("$VERSION", || self.version.to_owned().unwrap_or_default());
        sanitize(song_name_schema.trim())
    }
}

trait ContainsAndReplace {
    /// Find if a song contains any given string, and if it does, invoke the callback and replace it
    fn contains_n_replace<T: Fn() -> String>(self, pattern: &str, replacement: T) -> String;
}

impl ContainsAndReplace for String {
    fn contains_n_replace<T: Fn() -> String>(mut self, pattern: &str, replacement: T) -> String {
        if self.contains(pattern) {
            let r = replacement();
            self = self.replace(pattern, &r);
        }
        self
    }
}
