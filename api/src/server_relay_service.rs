use axum::Extension;
use niceerr_dz::types::StreamFormat;
use niceerr_entity::{assigned_permission::Permission, user::Model as UserModel};
use reqwest::StatusCode;
use serde::{Deserialize, Serialize};

use crate::{
    arl_service::ArlService,
    download::ty::OngoingDownload,
    error::ApiErrorResponse,
    info,
    ty::{Json, JsonResult},
};

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct RelayRequest {
    pub id: u64,
    #[serde(default)]
    pub stream_format: StreamFormat,
    #[serde(default)]
    pub allow_fallback: bool,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct RelayResponse {
    pub download_url: String,
    pub stream_format: StreamFormat,
    #[serde(with = "hex::serde")]
    pub key_hex: Vec<u8>,
    pub size: usize,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(untagged)]
pub enum FallibleRelayResponse {
    Success(RelayResponse),
    Failure(ApiErrorResponse),
}

impl RelayRequest {
    pub fn from_ongoing_download(id: u64, session: &OngoingDownload) -> Self {
        Self {
            id,
            stream_format: session.stream_format(),
            allow_fallback: session.download_session.allow_fallback,
        }
    }
}

pub async fn get_relayed_song(
    Extension(caller): Extension<UserModel>,
    Json(mut req): Json<RelayRequest>,
) -> JsonResult<RelayResponse> {
    caller.verify_has_permission(&Permission::RelayAccess)?;
    let arlsv = ArlService::get();
    let client = arlsv.get_primary_client().await?;
    let song = client.song(req.id).await?;
    let (song, client) = arlsv.fetch_song_with_client(song).await?;
    if !song.is_stream_format_available(&req.stream_format) && req.allow_fallback {
        if let Some(f) = song.fallback_format(&req.stream_format) {
            req.stream_format = f;
        }
    }
    let response = RelayResponse {
        download_url: song.stream_url(&client, &req.stream_format).await?,
        stream_format: req.stream_format,
        key_hex: song.stream_key(),
        size: song.byte_size(&req.stream_format),
    };
    info!(
        "relaying download link for song id={}, format={}, title='{}'",
        req.id, req.stream_format, song.title
    );
    Ok((StatusCode::OK, axum::Json(response)))
}
