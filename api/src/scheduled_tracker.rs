use std::{collections::HashMap, sync::LazyLock, time::Duration};

use chrono::Utc;
use dashmap::DashMap;
use log::warn;
use niceerr_config::Config;
use serde::{Deserialize, Serialize};
use tokio::{spawn, time::sleep};
use tracing::{info, Instrument, Span};

use crate::{
    arl_service::ArlService, client_relay_service::RelayClientService,
    download::ty::DownloadService, error::ApiError, prune_service::PruneService, ty::ApiResult,
};

#[derive(Debug, Deserialize, Serialize, PartialEq, Eq, Hash, Clone, Copy)]
#[serde(rename_all = "camelCase")]
pub enum ScheduledTaskName {
    UserDownloadsPrune,
    Housekeeping,
    PrimaryArlRefresh,
    PruneExpiredArl,
    RelayTokenRefresh,
}

#[derive(Debug, Deserialize, Serialize, PartialEq, Eq, Hash, Default, Clone)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum TaskState {
    #[default]
    NeverExecuted,
    Running,
    Completed,
}

#[derive(Debug, Deserialize, Serialize, PartialEq, Eq, Hash, Default, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ScheduledTask {
    state: TaskState,
    last_run: Option<chrono::DateTime<Utc>>,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ScheduledTaskTracker {
    state: DashMap<ScheduledTaskName, ScheduledTask>,
}

pub struct TaskTrackerGuard {
    task: ScheduledTaskName,
}

pub type SerializedScheduleState = HashMap<ScheduledTaskName, ScheduledTask>;

static TRACKED_TASKS: LazyLock<ScheduledTaskTracker> = LazyLock::new(|| {
    let map = DashMap::new();
    map.insert(ScheduledTaskName::UserDownloadsPrune, Default::default());
    map.insert(ScheduledTaskName::Housekeeping, Default::default());
    map.insert(ScheduledTaskName::PrimaryArlRefresh, Default::default());
    map.insert(ScheduledTaskName::PruneExpiredArl, Default::default());
    map.insert(ScheduledTaskName::RelayTokenRefresh, Default::default());
    ScheduledTaskTracker { state: map }
});

impl ScheduledTaskTracker {
    pub fn serialize_state() -> SerializedScheduleState {
        TRACKED_TASKS
            .state
            .iter()
            .map(|it| (*it.key(), it.value().to_owned()))
            .collect::<HashMap<_, _>>()
    }

    fn task_set_run(task: &ScheduledTaskName) {
        let mut task = TRACKED_TASKS
            .state
            .get_mut(task)
            .expect("Couldn't find task in hashmap, this is a bug.");
        task.state = TaskState::Running;
    }

    fn set_completed(task: &ScheduledTaskName) {
        let mut task = TRACKED_TASKS
            .state
            .get_mut(task)
            .expect("Couldn't find task in hashmap, this is a bug.");
        task.state = TaskState::Completed;
        task.last_run = Some(chrono::Utc::now());
    }

    fn is_running(task: ScheduledTaskName) -> bool {
        let task = TRACKED_TASKS
            .state
            .get(&task)
            .expect("Couldn't find task in hashmap, this is a bug.");
        task.state == TaskState::Running
    }

    fn check_multiple_running(tasks: &[ScheduledTaskName]) -> ApiResult<()> {
        for task in tasks {
            if Self::is_running(*task) {
                warn!("Attempted to run more than 1 instance of {task:?}");
                return Err(crate::error::ApiError::LockedResource);
            }
        }
        Ok(())
    }

    /// Checks whether any of the tasks that modify ARLs is running. This ensures we don't cause
    /// problems with the database when potentially "blocking" tasks run concurrently alongside
    /// CRUD operations.    
    pub fn check_arl_running() -> ApiResult<()> {
        Self::check_multiple_running(&[
            ScheduledTaskName::PrimaryArlRefresh,
            ScheduledTaskName::PruneExpiredArl,
        ])?;
        Ok(())
    }

    pub fn check_running(task: &ScheduledTaskName) -> ApiResult<()> {
        if Self::is_running(*task) {
            warn!("Attempted to run more than 1 instance of {task:?}");
            return Err(crate::error::ApiError::LockedResource);
        }
        Ok(())
    }

    /// Try to generate a guard for this task.
    pub fn try_guard(task: ScheduledTaskName) -> ApiResult<TaskTrackerGuard> {
        Self::check_running(&task)?;
        Self::task_set_run(&task);
        Ok(TaskTrackerGuard { task })
    }

    pub async fn run_task(task: ScheduledTaskName) -> ApiResult<()> {
        match task {
            ScheduledTaskName::UserDownloadsPrune => PruneService::scheduled_task().await?,
            ScheduledTaskName::Housekeeping => DownloadService::scheduled_task().await?,
            ScheduledTaskName::PruneExpiredArl => {
                let span = Span::current();
                let background = spawn(async {
                    ArlService::prune_expired_arls_inner()
                        .instrument(span)
                        .await?;
                    Ok::<_, ApiError>(())
                });
                let sleep = sleep(Duration::from_secs(5));
                tokio::select! {
                    r = background => r??,
                    _ = sleep => {
                        info!("Prune expired ARLs is taking longer than 5s to complete, request will return early.");
                    },
                }
            }
            ScheduledTaskName::RelayTokenRefresh => {
                if Config::get().relay_url.is_none() {
                    return Err(ApiError::DisabledFeature);
                }
                RelayClientService::get().update_token().await?;
            }
            // Other tasks aren't meant to be manually executed or already have endpoints to do so
            // that don't require super-superuser permissions.
            _ => return Err(crate::error::ApiError::NotFound),
        };
        Ok(())
    }
}

impl Drop for TaskTrackerGuard {
    fn drop(&mut self) {
        ScheduledTaskTracker::set_completed(&self.task);
    }
}
