use std::{
    sync::{atomic::AtomicUsize, Arc, LazyLock},
    task::Poll,
    time::Duration,
};

use axum::response::sse::Event;
use futures_util::{ready, task::AtomicWaker, Stream};
use log::{error, info};
use niceerr_config::Config;
use niceerr_entity::user::Model as UserModel;
use serde_json::json;
use tokio::time::{interval, MissedTickBehavior};

use crate::{
    download::ty::DownloadService, error::ApiError, shutdown::is_shutting_down, ty::ApiResult,
};

static SSE_SUBSCRIBER_COUNT: LazyLock<AtomicUsize> = LazyLock::new(|| AtomicUsize::new(0));

pub fn sse_ongoing_download_subscribers() -> usize {
    SSE_SUBSCRIBER_COUNT.load(std::sync::atomic::Ordering::Relaxed)
}

struct SubscriberCountGuard {}

impl SubscriberCountGuard {
    fn new() -> Self {
        SSE_SUBSCRIBER_COUNT.fetch_add(1, std::sync::atomic::Ordering::Relaxed);
        Self {}
    }
}

impl Drop for SubscriberCountGuard {
    fn drop(&mut self) {
        SSE_SUBSCRIBER_COUNT.fetch_sub(1, std::sync::atomic::Ordering::Relaxed);
    }
}

/// SSE state sender for ongoing downloads.
pub(crate) struct StateSender {
    /// Whether we already sent the user an empty list.
    sent_empty: bool,
    /// Whether this state sender needs to enter the hot loop, busy
    /// polling state.
    needs_busy_poll: bool,
    caller: UserModel,
    sleep: tokio::time::Interval,
    waker: Arc<AtomicWaker>,
    _count_guard: SubscriberCountGuard,
}

impl StateSender {
    pub fn new(caller: UserModel) -> Self {
        let waker = DownloadService::ongoing_waker();
        let poll_interval = Config::get().sse_ongoing_downloads_poll_interval_msec as u64;
        let mut sleep = interval(Duration::from_millis(poll_interval));
        sleep.set_missed_tick_behavior(MissedTickBehavior::Skip);
        info!("New SSE listener connected, poll interval={poll_interval} ms");
        Self {
            _count_guard: SubscriberCountGuard::new(),
            needs_busy_poll: false,
            sent_empty: false,
            caller,
            sleep,
            waker,
        }
    }

    /// Whether we should send the client an empty list.
    ///
    /// This is usually needed ONLY after we were sending some data and
    /// then suddenly there's no more data. It helps downstream customers
    /// know there are no more ongoing downloads.
    fn empty_event(sent_empty: &mut bool) -> Option<Event> {
        if !*sent_empty {
            let ev = Event::default()
                .json_data(json!([]))
                .expect("Cannot serialize blank data");
            *sent_empty = true;
            return Some(ev);
        }
        None
    }

    /// Fast/best-case polling:
    fn cold_poll(mut self: std::pin::Pin<&mut Self>) -> std::task::Poll<Option<ApiResult<Event>>> {
        if DownloadService::active_downloads_count() == 0 {
            if let Some(ev) = StateSender::empty_event(&mut self.sent_empty) {
                return Poll::Ready(Some(Ok(ev)));
            }
            return Poll::Pending;
        }

        let state = DownloadService::get_serializable_instance(&self.caller);

        // This can happen if this user doesn't have permissions to see someone else's
        // downloads. We don't want to send them anything in that case.
        if state.is_empty() {
            if let Some(ev) = StateSender::empty_event(&mut self.sent_empty) {
                return Poll::Ready(Some(Ok(ev)));
            }
            return Poll::Pending;
        }

        self.sent_empty = false;
        let ev = Event::default().json_data(state).map_err(|e| {
            error!("SSE: couldn't serialize status: {e:?}");
            ApiError::InternalError
        })?;

        // There are ongoing downloads for this user: switch to busy polling
        self.needs_busy_poll = true;
        Poll::Ready(Some(Ok(ev)))
    }

    /// Busy poll for new events. This will consume a little bit more CPU (because
    /// the task has to be woken up periodically). When there are no new ongoing
    /// downloads visible for this user, we switch back to cold/efficient polling.
    ///
    /// The cold poll handler will be woken up far less often to check if we should
    /// switch back again to busy polling.
    fn busy_poll(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Option<ApiResult<Event>>> {
        ready!(self.sleep.poll_tick(cx));
        // Don't even clone the internal state if it's empty
        if DownloadService::active_downloads_count() == 0 {
            if let Some(ev) = StateSender::empty_event(&mut self.sent_empty) {
                return Poll::Ready(Some(Ok(ev)));
            }
            self.needs_busy_poll = false;
            return Poll::Pending;
        }

        let state = DownloadService::get_serializable_instance(&self.caller);

        if state.is_empty() {
            if let Some(ev) = StateSender::empty_event(&mut self.sent_empty) {
                return Poll::Ready(Some(Ok(ev)));
            }
            self.needs_busy_poll = false;
            return Poll::Pending;
        }

        self.sent_empty = false;
        let ev = Event::default().json_data(state).map_err(|e| {
            error!("SSE: couldn't serialize status: {e:?}");
            ApiError::InternalError
        })?;
        Poll::Ready(Some(Ok(ev)))
    }
}

impl Stream for StateSender {
    type Item = ApiResult<Event>;

    fn poll_next(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Option<Self::Item>> {
        self.waker.register(cx.waker());

        if is_shutting_down() {
            info!("forcefully disconnecting SSE listener due to shutdown");
            return Poll::Ready(None);
        }

        match self.needs_busy_poll {
            true => self.busy_poll(cx),
            false => self.cold_poll(),
        }
    }
}
