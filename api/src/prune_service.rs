use std::time::Duration;

use futures_util::StreamExt;
use log::info;
use niceerr_config::Config;
use niceerr_entity::{song::Model as Song, user::Model as User};
use tokio::time::interval;
use tracing::error;

use crate::{
    scheduled_tracker::{ScheduledTaskName, ScheduledTaskTracker},
    ty::ApiResult,
};

pub struct PruneService {}

impl PruneService {
    pub fn init() {
        tokio::spawn(Self::run_scheduled_task());
        info!(
            "Prune service started: interval={}",
            Config::get().prune_service_interval_sec,
        );
    }

    async fn song_prunner(receiver: flume::Receiver<Song>) -> ApiResult<()> {
        while let Ok(song) = receiver.recv_async().await {
            match song.prune().await {
                Ok(()) => {}
                Err(e) => {
                    error!(
                        "Couldn't prune song {} @ {:?}: {e:?}",
                        song.id, song.download_path
                    );
                }
            };
        }
        Ok(())
    }

    async fn process_user(user: User, sender: flume::Sender<Song>) -> ApiResult<(u32, u32)> {
        let (mut song_count, mut session_count) = (0, 0);
        let mut sessions = user.pruneable_download_sessions(None, false, false).await?;
        while let Some(session) = sessions.next().await.transpose()? {
            let mut songs = session.songs_as_stream().await?;
            session_count += 1;
            while let Some(song) = songs.next().await.transpose()? {
                sender.send_async(song).await.ok();
                song_count += 1;
            }
            session.mark_pruned().await?;
        }
        Ok((song_count, session_count))
    }

    pub async fn scheduled_task() -> ApiResult<()> {
        let _tracker_guard =
            ScheduledTaskTracker::try_guard(ScheduledTaskName::UserDownloadsPrune)?;
        let (tx, rx) = flume::bounded(0);
        let handle = tokio::spawn(Self::song_prunner(rx.clone()));
        drop(rx);
        let mut users = User::with_pruneable_dl_sessions().await?;
        let mut user_count = 0;
        let (mut song_count, mut session_count) = (0, 0);
        while let Some(user) = users.next().await.transpose()? {
            let (s_user_count, s_session_count) = Self::process_user(user, tx.clone()).await?;
            song_count += s_user_count;
            session_count += s_session_count;
            user_count += 1;
        }
        drop(tx);
        handle.await.ok();
        info!(
            "Successfully ran pruner for: {} users, {} sessions, {} songs",
            user_count, session_count, song_count
        );

        Ok(())
    }

    async fn run_scheduled_task() {
        let mut interval = interval(Duration::from_secs(
            Config::get().prune_service_interval_sec.into(),
        ));
        loop {
            interval.tick().await;
            if let Err(e) = Self::scheduled_task().await {
                error!("Couldn't prune: {e:?}");
            }
        }
    }
}
