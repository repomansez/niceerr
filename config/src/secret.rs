use jsonwebtoken::{DecodingKey, EncodingKey};

// Parse ed25519 private key and generate encoding and decoding keys from it.
pub(crate) fn get_key_pair(priv_key: &str) -> (EncodingKey, DecodingKey) {
    let encoding_key = EncodingKey::from_secret(priv_key.as_bytes());
    let decoding_key = DecodingKey::from_secret(priv_key.as_bytes());
    (encoding_key, decoding_key)
}
