use anyhow::{bail, Context};
use better_debug::BetterDebug;
use chrono_tz::Tz;
use dotenvy::dotenv;
use iso639_lang::Iso669CountryCode;
use jsonwebtoken::{DecodingKey, EncodingKey};
use log::{info, warn};
use serde::Deserialize;
use serde_inline_default::serde_inline_default;
use std::{net::SocketAddr, ops::Deref, path::Path, sync::OnceLock};
use url::Url;
pub mod iso639_lang;
pub(crate) mod secret;

pub use sanitize_filename::sanitize;
pub static DEFAULT_NICEERR_USERNAME: &str = "niceerr";

#[serde_inline_default]
#[derive(BetterDebug, Deserialize)]
pub struct EnvConfig {
    #[better_debug(secret)]
    jwt_secret: String,
    // Max number of queued download requests per user. If any given user has more than
    // `max_grants_per_user`, their download will be rejected.
    // Default: 5
    #[serde_inline_default(5)]
    pub max_grants_per_user: u8,

    // Dangerous stuff
    #[serde_inline_default(false)]
    pub dangerous_cors_origin_allow_any: bool,

    // List of allowed CORS origins.
    pub cors_origins: Option<Box<[Url]>>,

    // Default: ::0
    #[serde_inline_default("[::]:8000".parse::<SocketAddr>().expect("Can't parse default SocketAddr"))]
    pub listen_address: SocketAddr,

    // Whether to enable prometheus metrics endpoint
    #[better_debug(secret)]
    pub prometheus_basic_auth_credentials: Option<String>,

    /// JWT default token duration (1 day)
    #[serde_inline_default(43200)]
    pub jwt_expiration_sec: u32,

    #[better_debug(secret)]
    pub default_admin_password: String,

    // User agent used to make requests
    #[serde_inline_default(String::from("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36"))]
    pub user_agent: String,

    pub downloads_folder: String,

    // When stuff is being actively downloaded, refresh tokens every this many seconds
    #[serde_inline_default(1200)]
    pub arl_refresh_max_age_sec: u32,

    // After this many seconds of download or stream inactivity, kill the token refresher
    #[serde_inline_default(300)]
    pub arl_refresher_inactivity_timeout_sec: u32,

    // Run the expired ARL pruner every this many seconds
    // Set by default to 12 hours (43200 seconds)
    #[serde_inline_default(43200)]
    pub expired_token_prune_sec: u32,

    // artist_name_schema
    #[serde_inline_default(String::from("$ARTIST_NAME"))]
    pub artist_name_schema: String,

    // album name schema
    #[serde_inline_default(String::from("$ALBUM_NAME"))]
    pub album_name_schema: String,

    // song name schema
    #[serde_inline_default(String::from("$TRACK_NUMBER. $TRACK_NAME $VERSION"))]
    pub song_name_schema: String,

    // playlist name schema
    #[serde_inline_default(String::from("$TITLE ($ID)"))]
    pub playlist_name_schema: String,

    // playlist folder
    #[serde_inline_default(Some(String::from("Playlists")))]
    pub playlist_folder: Option<String>,

    // Request this language in all our requests
    #[serde_inline_default(Iso669CountryCode::English)]
    pub http_accept_language: Iso669CountryCode,

    // Allow server-side downloads
    #[serde_inline_default(true)]
    pub enable_server_downloads: bool,

    // Whether to allow downloads or not
    #[serde_inline_default(true)]
    pub enable_stream_downloads: bool,

    // Default: 128KiB
    #[serde_inline_default(131_072)]
    pub stream_download_buffer_byte_size: usize,

    // Size of the streaming song queue
    #[serde_inline_default(0u8)]
    pub streaming_download_queue_size: u8,

    // Queue size for the album fetcher
    #[serde_inline_default(5u8)]
    pub album_fetcher_queue_size: u8,

    #[serde_inline_default(120u16)]
    pub streaming_remote_timeout_sec: u16,

    // When streaming stuff from upstream, use this many download
    // workers.
    #[serde_inline_default(3u8)]
    pub streaming_download_workers: u8,

    // Temp dir for downloads
    #[serde_inline_default(std::env::temp_dir().display().to_string())]
    pub temp_folder: String,

    // Max download workers
    // Default: 5
    #[serde_inline_default(6)]
    pub server_side_download_workers: u8,

    // Connection timeout in seconds
    #[serde_inline_default(60)]
    pub connect_timeout_sec: u16,

    #[serde_inline_default(60u16)]
    pub tcp_keepalive_sec: u16,

    // Timeout from when the request starts connecting until the response body has finished.
    #[serde_inline_default(60)]
    pub read_timeout_sec: u16,

    // When exponential backoff is enabled, use this as the upper ceil (in secconds) to prevent
    // the backoff from making us wait way too many seconds.
    #[serde_inline_default(65)]
    pub retry_backoff_ceil: u32,

    // Retry at most this many times before giving up on requests, downloads, etc.
    #[serde_inline_default(5)]
    pub max_retries: u8,

    #[better_debug(secret)]
    pub database_url: String,

    // How often should we run the prune service?
    // The prune service runs in background every now and then and deletes
    // downloads from users that have a non-zero persistence setting.
    #[serde_inline_default(1800)]
    pub prune_service_interval_sec: u16,

    // Period in seconds after which a semaphore will be considered stale.
    #[serde_inline_default(60)]
    pub max_semaphore_lifetime_sec: u16,

    // Before niceerr exits, we always try to gracefully stop
    // all pending downloads and save status to database. However, we can't stop
    // everything right away and we have to wait for the OS to close files, songs
    // to be saved to DB and so on. niceerr will check every 500ms whether we're
    // done with everything this many times.
    //
    // 0 means niceerr will exit right away (this is absolutely NOT recommended).
    //
    // Default: 20 (20 * 500ms = 10s of total wait time before niceerr exits abruptly).
    #[serde_inline_default(20)]
    pub shutdown_max_loops: u16,

    // Timezone we should use when calculating downloaded bytes per day. Those are
    // reset every 24 hours, so it makes sense to reset 'em whenever
    // it's a new day at the user's local timezone (instead of UTC).
    #[serde_inline_default(chrono_tz::UTC)]
    pub timezone: Tz,

    // Whether we should create a folder for each separate disk, if applicable, i.e.
    // SOME ARTIST/COOL ALBUM/1/<songs>
    // SOME ARTIST/COOL ALBUM/2/<songs>
    //
    // Set to true by default. Note that albums with less than 2 disks will not cause
    // niceerr to create separate folders, as that would be redundant.
    #[serde_inline_default(true)]
    pub create_folder_for_each_disk: bool,

    // Run cleanup and housekeeping tasks every this many seconds.
    //
    // This will free up memory used by the internal queues and
    // delete potentially temporary files that might've been created by niceerr.
    // Default: 3600 (1 hour)
    #[serde_inline_default(3600u16)]
    pub housekeeping_interval_sec: u16,

    #[serde_inline_default(false)]
    pub server_enable_relay: bool,

    /// Use this relay to stream songs
    #[better_debug(secret)]
    pub relay_url: Option<Url>,

    /// When using a relay, refresh the token every this many seconds
    #[serde_inline_default(7200)]
    pub relay_client_refresh_sec: u32,

    /// Database options
    /// Max database connections at any given time
    #[serde_inline_default(100u32)]
    pub db_max_connections: u32,

    /// Minimum number of database connections to keep open at all times
    #[serde_inline_default(1u32)]
    pub db_min_connections: u32,

    /// How long a connection can be idle before it is closed (in seconds)
    #[serde_inline_default(60u64)]
    pub db_idle_timeout_sec: u64,

    /// How long to wait for a new connection to be established
    #[serde_inline_default(10u64)]
    pub db_connect_timeout_sec: u64,

    /// How long to wait for acquiring a new connection from the pool
    /// (in seconds)
    #[serde_inline_default(60u64)]
    pub db_acquire_timeout_sec: u64,

    /// How often should we send keep-alives when clients are connected to
    /// SSE streams?
    #[serde_inline_default(15u8)]
    pub sse_keep_alive_interval_sec: u8,

    /// How often should we send the client updates when there are ongoing downloads?
    #[serde_inline_default(1000u16)]
    pub sse_ongoing_downloads_poll_interval_msec: u16,

    // Webhook URL to call to whenever a new song has been downloaded.
    pub webhook_url: Option<Url>,
}

#[derive(BetterDebug)]
pub struct Config {
    pub inner: EnvConfig,
    #[better_debug(secret)]
    pub encoding_key: EncodingKey,
    #[better_debug(secret)]
    pub decoding_key: DecodingKey,
}

impl Deref for Config {
    type Target = EnvConfig;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

// Global config singleton
static CONFIG: OnceLock<Config> = OnceLock::new();

impl Config {
    pub fn get() -> &'static Config {
        CONFIG.get().expect("config isn't initialized")
    }

    async fn verify_writable(path: &str) -> anyhow::Result<()> {
        let test_file_path = Path::new(path).join("test.txt");
        tokio::fs::create_dir_all(&path).await?;
        tokio::fs::write(&test_file_path, "test").await?;
        tokio::fs::remove_file(&test_file_path).await?;
        Ok(())
    }

    pub async fn init() -> anyhow::Result<()> {
        if CONFIG.get().is_some() {
            warn!("config is already initialized");
            return Ok(());
        }
        dotenv().ok();
        let env_conf = Self::parse_from_env()?;
        info!("verifying config from env vars: {:#?}", env_conf);

        // Downloads folder
        // We also verify here whether the folder exists, and if we have permissions to write in it
        info!(
            "Verifying whether root download folder ('{}') is writable",
            env_conf.downloads_folder
        );
        Self::verify_writable(&env_conf.downloads_folder)
            .await
            .context("Downloads folder path isn't writable")?;

        // Temp folder
        info!(
            "Verifying whether tmp folder ('{}') is writable",
            env_conf.temp_folder
        );
        Self::verify_writable(&env_conf.temp_folder)
            .await
            .context("Downloads folder path isn't writable")?;

        let (encoding_key, decoding_key) = secret::get_key_pair(&env_conf.jwt_secret);

        if (env_conf.streaming_remote_timeout_sec as u32) < env_conf.retry_backoff_ceil {
            warn!(
                r#"`streaming_remote_timeout_sec` should preferably be greater than
`retry_backoff_ceil` to prevent the server from disconnecting clients when there's a download failure."#
            );
        }

        if env_conf.streaming_remote_timeout_sec < env_conf.read_timeout_sec {
            warn!(
                r#"`streaming_remote_timeout_sec` should preferably be greater than
`read_timeout_sec` to prevent the server from disconnecting clients when there are server-side network issues."#
            );
        }

        if env_conf.server_enable_relay {
            warn!("`server_enable_relay` is enabled. This will probably increase your ARL's CDN usage quota.")
        }

        let config = Self {
            inner: env_conf,
            encoding_key,
            decoding_key,
        };

        CONFIG
            .set(config)
            .map_err(|_| anyhow::Error::msg("Cannot set config singleton"))?;
        Ok(())
    }

    // Parse config from env.
    fn parse_from_env() -> anyhow::Result<EnvConfig> {
        let env_conf = envy::from_env::<EnvConfig>().context(
            "couldn't parse env conf: one or more settings are either invalid or missing",
        )?;

        if env_conf.jwt_secret.trim().is_empty() {
            bail!("jwt_secret cannot be an empty string");
        }

        // Max global downloads
        if env_conf.max_grants_per_user == 0 {
            bail!("max_grants_per_user must be greater than 0")
        }

        // JWT expiration
        if env_conf.jwt_expiration_sec == 0 {
            bail!("jwt_expiration_sec must be greater than 0");
        }

        // Default admin password
        if env_conf.default_admin_password.trim().is_empty() {
            bail!("default_admin_password cannot be an empty string");
        }

        // Refresh service
        if env_conf.arl_refresh_max_age_sec == 0 {
            bail!("`arl_refresh_max_age_sec` must be greater than 0");
        }

        if env_conf.arl_refresher_inactivity_timeout_sec == 0 {
            bail!("`arl_refresher_inactivity_timeout_sec` must be greater than 0");
        }

        if env_conf.expired_token_prune_sec == 0 {
            warn!("`expired_token_prune_sec` is set to 0. This will disable automatatic pruning of expired ARLs.");
        }

        // max download workers
        if env_conf.server_side_download_workers == 0 {
            bail!("server_side_download_workers must be greater than 0");
        }

        if env_conf.album_fetcher_queue_size == 0 {
            bail!("album_fetcher_queue_size must be greater than 0");
        }

        // request timeouts
        if env_conf.connect_timeout_sec == 0 {
            bail!("connect_timeout_sec must be greater than 0");
        }

        if env_conf.read_timeout_sec == 0 {
            bail!("read_timeout_sec must be greater than 0");
        }

        // retry ceil limit
        if env_conf.retry_backoff_ceil == 0 {
            bail!("retry_backoff_ceil must be greater than 0");
        }

        // max retries
        if env_conf.max_retries == 0 {
            bail!("max_retries must be greater than 0");
        }

        if env_conf.max_semaphore_lifetime_sec == 0 {
            bail!("max_semaphore_lifetime_sec must be greater than 0");
        }

        if env_conf.streaming_remote_timeout_sec == 0 {
            bail!("streaming_remote_timeout_sec must be greater than 0");
        }

        if env_conf.cors_origins.is_none() && !env_conf.dangerous_cors_origin_allow_any {
            bail!(
                "'cors_origins' is empty and 'dangerous_cors_origin_allow_any' is false! \
Please set at least one URL or set `dangerous_cors_origin_allow_any` \
to true."
            )
        }

        if env_conf.song_name_schema.trim().is_empty()
            || env_conf.album_name_schema.trim().is_empty()
            || env_conf.artist_name_schema.trim().is_empty()
        {
            bail!("Name schemas must not be empty.");
        }

        if let Some(playlist_folder) = &env_conf.playlist_folder {
            if sanitize(playlist_folder.trim()).is_empty() {
                bail!("Playlist folder must not be empty.");
            }
        }

        if env_conf.playlist_name_schema.trim().is_empty() {
            warn!("Playlist name schema is empty. Playlists will be downloaded to the root folder/user folder.");
        }

        if env_conf.dangerous_cors_origin_allow_any && env_conf.cors_origins.is_some() {
            bail!(
                "'dangerous_cors_origin_allow_any' is set to true but at least \
one origin URL ('cors_origins') was set! You cannot use both at the same time."
            );
        }

        if env_conf.stream_download_buffer_byte_size < 8192
            || env_conf.stream_download_buffer_byte_size % 256 != 0
        {
            bail!("`stream_download_buffer_byte_size` must be greater than 8192 (8KiB) and a multiple of 256");
        }

        if !env_conf.enable_server_downloads && !env_conf.enable_stream_downloads {
            bail!("`enable_server_downloads` and `enable_zip_downloads` are disabled! At least one of these features must be enabled.");
        }

        if env_conf.streaming_download_workers == 0 {
            bail!("`streaming_download_workers` must be greater than 0");
        }

        if env_conf.db_min_connections > env_conf.db_max_connections {
            bail!("`db_min_connections` must be less than or equal to `db_max_connections`");
        }

        if env_conf.housekeeping_interval_sec == 0 {
            bail!("`housekeeping_interval_sec` must be greater than 0");
        }

        if env_conf.sse_keep_alive_interval_sec == 0 {
            bail!("`sse_keep_alive_interval_sec` must be greater than 0");
        }

        if env_conf.sse_ongoing_downloads_poll_interval_msec < 500 {
            bail!(
                "`sse_ongoing_downloads_poll_interval_msec` must be greater than or equal to 500"
            );
        }

        if env_conf.server_enable_relay && env_conf.relay_url.is_some() {
            bail!("`server_enable_relay` and `relay_url` are mutually exclusive features. Use only one at a time.");
        }

        if env_conf.relay_client_refresh_sec == 0 {
            bail!("`relay_client_refresh_sec` must be greater than 0")
        }

        if let Some(relay_url) = env_conf.relay_url.as_ref() {
            if relay_url.username().trim().is_empty()
                || relay_url.password().is_none()
                || relay_url.password().is_some_and(|s| s.trim().is_empty())
            {
                bail!("`relay_url` must contain a valid username and password.");
            }
        }

        if env_conf.db_max_connections == 0
            || env_conf.db_min_connections == 0
            || env_conf.db_idle_timeout_sec == 0
            || env_conf.db_connect_timeout_sec == 0
            || env_conf.db_acquire_timeout_sec == 0
        {
            bail!("All db_* options must be greater than 0");
        }

        if env_conf.db_min_connections >= env_conf.db_max_connections {
            bail!("`db_min_connections` must be less than `db_max_connections`");
        }

        if env_conf
            .prometheus_basic_auth_credentials
            .as_ref()
            .is_some_and(|v| !v.contains(":"))
        {
            bail!("`prometheus_basic_auth_credentials` must be in the format `username:password`");
        }

        Ok(env_conf)
    }
}
